package run;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import classes.Area;
import classes.Customer;
import classes.DeliveryDocket;
import classes.Driver;
import classes.Order;
import classes.OrdersToPublication;
import classes.Publication;

public class MySQL {

	private static Connection connect = null;
	private Statement statement = null;
	private static PreparedStatement preparedStatement = null;
	private static ResultSet resultSet = null;
	private List<Integer> list = new ArrayList<Integer>();

	public MySQL() throws SQLException {
		try {
			String url = "jdbc:mysql://localhost:3306/mydb?useSSL=false";
			connect = DriverManager.getConnection(url, "root", "admin" + "");
			statement = connect.createStatement();
			System.out.println("Server Runing");
		} catch (SQLException e) {
			System.out.println("Error: Failed to initialise DB Connection");
			throw new SQLException("Error: Failed to initialise DB Connection");
		}
	}

	public static String ExportStats() throws IOException, SQLException {
		ArrayList<Publication> data = ReadPublication();

		BufferedWriter writer = new BufferedWriter(new FileWriter(new File("Publication.csv")));

		if (data == null) {
			return "There is no data to generate csv file";
		} else {
			for (Publication statsData : data) {
				writer.write("Book Name: " + statsData.getBook_name() + "\nBook Price: " + statsData.getBook_price()
						+ "\nBook Amount: " + statsData.getBook_amount() + "\n\n");
			}
			writer.close();
			return "Exported Successfully";
		}
	}

	public static ArrayList<Publication> ReadPublication() throws SQLException {
		ArrayList<Publication> arrayList = new ArrayList<>();
		try {
			preparedStatement = connect.prepareStatement("SELECT book_name, book_price, book_amount FROM publications");
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				arrayList.add(new Publication(resultSet.getString(1), resultSet.getInt(2), resultSet.getInt(3)));
			}
			return arrayList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

//+------------------------------CREATE------------------------------+
	public boolean insertArea(Area c) throws SQLException {
		// create Area
		boolean insertSucessfull = true;
		try {
			preparedStatement = connect.prepareStatement("INSERT INTO delivery_area values (default, ?, ?);");
			preparedStatement.setInt(1, c.getId_customer());
			preparedStatement.setInt(2, c.getId_driver());
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			insertSucessfull = false;
		}
		return insertSucessfull;
	}

	public boolean insertOrders(Order c) throws SQLException {
		// create Order
		boolean insertSucessfull = true;
		try {
			preparedStatement = connect.prepareStatement("INSERT INTO orders values (default, ?, ?);");
			preparedStatement.setString(1, c.getOrder_type());
			preparedStatement.setInt(2, c.getId_customer());
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			insertSucessfull = false;
		}
		return insertSucessfull;
	}

	public boolean insertOrders1(Order c, OrdersToPublication b) throws SQLException {
		// create Order
		boolean insertSucessfull = true;
		try {
			preparedStatement = connect.prepareStatement("INSERT INTO orders values (default, ?, ?);");
			preparedStatement.setString(1, c.getOrder_type());
			preparedStatement.setInt(2, c.getId_customer());
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);

			preparedStatement = connect
					.prepareStatement(" INSERT INTO order_publication VALUES (default, LAST_INSERT_ID(), ?);");
			preparedStatement.setInt(1, b.getId_publication());
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);

		} catch (SQLException e) {
			insertSucessfull = false;
		}
		return insertSucessfull;
	}

	public boolean insertdelivery_driver(Driver c) throws SQLException {
		// create Driver
		boolean insertSucessfull = true;
		try {
			preparedStatement = connect.prepareStatement("INSERT INTO delivery_driver values (default, ?, ?);");
			preparedStatement.setString(1, c.getName());
			preparedStatement.setString(2, c.getPassword());
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			insertSucessfull = false;
		}
		return insertSucessfull;
	}

	public boolean insertOrdPub(OrdersToPublication c) throws SQLException {
		// create Order to Publication
		boolean insertSucessfull;
		try {
			preparedStatement = connect.prepareStatement("INSERT INTO order_publication values (default, ?, ?);");
			preparedStatement.setInt(1, c.getId_order());
			preparedStatement.setInt(2, c.getId_publication());
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
			insertSucessfull = true;
		} catch (SQLException e) {
			insertSucessfull = false;
			// throw new SQLException("fail insert");
		}
		return insertSucessfull;
	}

	public boolean insertCustomer(Customer c) throws SQLException {
		// Create Customer
		boolean insertSucessfull;
		try {
			preparedStatement = connect.prepareStatement("INSERT INTO customer values (default, ?, ?, ?, ?, ?);");
			preparedStatement.setString(1, c.getFirstName());
			preparedStatement.setString(2, c.getLastName());
			preparedStatement.setString(3, c.getAddress());
			preparedStatement.setString(4, c.getDeliveryArea());
			preparedStatement.setString(5, c.getPhone());
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
			insertSucessfull = true;
		} catch (SQLException e) {
			insertSucessfull = false;
			// throw new SQLException("fail insert");
		}
		return insertSucessfull;
	}

	public boolean insertPublication(Publication c) throws SQLException {
		// create Publication
		boolean insertSucessfull;
		try {
			preparedStatement = connect.prepareStatement("INSERT INTO publications VALUES (default, ?, ?, ?);");
			preparedStatement.setString(1, c.getBook_name());
			preparedStatement.setInt(2, c.getBook_price());
			preparedStatement.setInt(3, c.getBook_amount());
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
			insertSucessfull = true;
		} catch (SQLException e) {
			insertSucessfull = false;
		}
		return insertSucessfull;
	}

//+------------------------------READ------------------------------+
	public ResultSet printArea() throws SQLException {
		// read Delivery Area
		try {
			resultSet = statement.executeQuery("SELECT * FROM delivery_area;");
			System.out.println("a");
		} catch (SQLException e) {
			resultSet = null;
		}
		return resultSet;
	}

	public ResultSet printAllOrders() throws SQLException {
		// read Order
		try {
			resultSet = statement.executeQuery("SELECT * FROM orders;");
		} catch (SQLException e) {
			resultSet = null;
		}
		return resultSet;
	}

	public ResultSet printAllOrdersToPublication() throws SQLException {
		// read Order to Publication
		try {
			resultSet = statement.executeQuery("SELECT * FROM order_publication;");
		} catch (SQLException e) {
			resultSet = null;
		}
		return resultSet;
	}

	public ResultSet printAllCustomers() throws SQLException {
		// read Customers
		try {
			resultSet = statement.executeQuery("SELECT * FROM customer;");
		} catch (SQLException e) {
			resultSet = null;
		}
		return resultSet;
	}

	public ResultSet printAllPublications() throws SQLException {
		// read Publication
		try {
			resultSet = statement.executeQuery("SELECT * FROM publications;");
		} catch (SQLException e) {
			resultSet = null;
		}
		return resultSet;
	}

	public ResultSet printAlldelivery_drivers() throws SQLException {
		// read delivery_driver to Order
		try {
			resultSet = statement.executeQuery("SELECT * FROM delivery_driver;");
		} catch (SQLException e) {
			resultSet = null;
		}
		return resultSet;
	}

// +------------------------------UPDATE------------------------------+
	public boolean updateOrders(Order c) throws SQLException {
		// update Order Type
		boolean insertSucessfull = true;
		try {
			preparedStatement = connect.prepareStatement("UPDATE orders SET order_type = ? WHERE id_order = ?;");
			preparedStatement.setString(1, c.getOrder_type());
			preparedStatement.setInt(2, c.getId_order());
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			insertSucessfull = false;
			// throw new SQLException("fail update");
		}
		return insertSucessfull;
	}

	public boolean updatePublication(Publication c) throws SQLException {
		// update Publication
		boolean insertSucessfull = true;
		try {
			preparedStatement = connect.prepareStatement(
					"UPDATE publications SET book_name = ?, book_price = ?, book_amount = ? WHERE id_publication = ?;");
			preparedStatement.setString(1, c.getBook_name());
			preparedStatement.setInt(2, c.getBook_price());
			preparedStatement.setInt(3, c.getBook_amount());
			preparedStatement.setInt(4, c.getId_publication());
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			insertSucessfull = false;
			// throw new SQLException("fail update");
		}
		return insertSucessfull;
	}

	public boolean updateCustomer(Customer c) throws SQLException {
		// update Customer
		boolean insertSucessfull = true;
		try {
			preparedStatement = connect.prepareStatement(
					"UPDATE customer SET first_name = ?, last_name = ?, address = ?, delivey_area = ?, phone = ? WHERE id_cust = ?;");
			preparedStatement.setString(1, c.getFirstName());
			preparedStatement.setString(2, c.getLastName());
			preparedStatement.setString(3, c.getAddress());
			preparedStatement.setString(4, c.getDeliveryArea());
			preparedStatement.setString(5, c.getPhone());
			preparedStatement.setInt(6, c.getId());
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			insertSucessfull = false;
			System.out.println("fail");
			// throw new SQLException("fail update");
		}
		return insertSucessfull;
	}

	public boolean updateDriver(Driver c) throws SQLException {
		// update Delivery driver
		boolean insertSucessfull = true;
		try {
			preparedStatement = connect.prepareStatement(
					"UPDATE delivery_driver SET name = ?, password = ? where id_delivery_driver = ?;");
			preparedStatement.setString(1, c.getName());
			preparedStatement.setString(2, c.getPassword());
			preparedStatement.setInt(3, c.getId_driver());
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			insertSucessfull = false;
			System.out.println("fail");
			// throw new SQLException("fail update");
		}
		return insertSucessfull;
	}

//+------------------------------DELETE------------------------------+
	public boolean deleteArea(int id_area) throws SQLException {
		// delete Area
		boolean deleteSucessfull = true;
		try {
			preparedStatement = connect.prepareStatement("DELETE from delivery_area WHERE id_delivery_area = ?;");
			preparedStatement.setInt(1, id_area);
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			deleteSucessfull = false;
		}
		return deleteSucessfull;
	}

	public boolean deleteOrders(int id) throws SQLException {
		// delete Order
		boolean insertSucessfull = true;
		try {
			preparedStatement = connect.prepareStatement("DELETE FROM orders WHERE id_order = ?;");
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			insertSucessfull = false;
			// throw new SQLException("fail update");
		}
		return insertSucessfull;
	}

	public boolean deleteOrderToPublication(int id) throws SQLException {
		// delete Order to Publication
		boolean insertSucessfull = true;
		try {
			preparedStatement = connect
					.prepareStatement("DELETE FROM order_publication WHERE id_orderpublication = ?;");
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			insertSucessfull = false;
		}
		return insertSucessfull;
	}

	public boolean deleteCustomer(int id) throws SQLException {
		// delete Customer
		boolean insertSucessfull = true;
		try {
			preparedStatement = connect.prepareStatement("DELETE FROM customer WHERE id_cust = ?;");
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			insertSucessfull = false;
		}
		return insertSucessfull;
	}

	public boolean deleteDriver(Driver driverObj) throws SQLException {
		// delete delivery driver
		boolean insertSucessfull = true;
		try {
			preparedStatement = connect.prepareStatement("DELETE FROM delivery_driver WHERE id_delivery_driver = ?;");
			preparedStatement.setInt(1, driverObj.getId_driver());
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			insertSucessfull = false;
		}
		return insertSucessfull;
	}

	public boolean deletePublications(int id) throws SQLException {
		// delete Publication
		boolean insertSucessfull = true;
		try {
			preparedStatement = connect.prepareStatement("DELETE FROM publications WHERE id_publication = ?;");
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			insertSucessfull = false;
		}
		return insertSucessfull;
	}

	public boolean deleteInvoice(int id) throws SQLException {
		// delete Invoice
		boolean insertSucessfull = true;
		try {
			preparedStatement = connect.prepareStatement("DELETE FROM inv WHERE idinv = ?;");
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			insertSucessfull = false;
		}
		return insertSucessfull;
	}

//+------------------------------INCREMENT------------------------------+
	public boolean incrementPublication(Publication c) throws SQLException {
		boolean insertSuccessfull = true;
		try {
			preparedStatement = connect.prepareStatement(
					"UPDATE publications set book_amount = book_amount + 1 WHERE id_publication = ?;");
			preparedStatement.setInt(1, c.getId_publication());
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			insertSuccessfull = false;
		}
		return insertSuccessfull;
	}

//+------------------------------DECREMENT------------------------------+
	public boolean decrementPublication(Publication c) throws SQLException {
		boolean insertSuccessfull = true;
		try {
			preparedStatement = connect.prepareStatement(
					"UPDATE publications set book_amount = book_amount - 1 WHERE id_publication = ?;");
			preparedStatement.setInt(1, c.getId_publication());
			preparedStatement.executeUpdate();
			System.out.println(preparedStatement);
		} catch (SQLException e) {
			insertSuccessfull = false;
		}
		return insertSuccessfull;
	}

	public ResultSet generateDeliveryAreaReport(Order c) throws SQLException {
		try {
			String order_type = c.getOrder_type();
			System.out.println(order_type);
			resultSet = statement.executeQuery("select *  from delivery_area\r\n"
					+ "inner join customer on delivery_area.area_customer_id = customer.id_cust  \r\n"
					+ "inner join orders on customer.id_cust = orders.customer_id_cust where order_type = '"
					+ order_type + "' group by id_cust;");
		} catch (SQLException e) {
			resultSet = null;
		}
		return resultSet;
	}

	// +------------------------------DRIVER LOGIN
	// DETAILS------------------------------+
	public ResultSet driverlogin() throws SQLException {
		try {
			resultSet = statement.executeQuery("SELECT * FROM delivery_driver;");
		} catch (SQLException e) {
			resultSet = null;
		}
		return resultSet;
	}

	public boolean DriverLogin(Driver c) throws SQLException {
		boolean logIn = true;
		ResultSet result = driverlogin();
		System.out.println(c.getName());
		System.out.println(c.getPassword());
		if (result == null) {
			System.out.println("Error: SQLException wrong driverlogin sql command");
		} else {
			while (result.next()) {
				try {
					if (c.getName().equals(result.getString(2)) && c.getPassword().equals(result.getString(3))) {
						logIn = true;
						return logIn;
					} else {
						logIn = false;
					}
				} catch (SQLException e) {
					logIn = false;
				}
			}
		}
		return logIn;
	}
	// +------------------------------DELIVERY DOCKET------------------------------+

	public ResultSet printAlldeliveryDocket(DeliveryDocket docketObj) throws SQLException {
		try {
			resultSet = statement.executeQuery("select * from delivery_docket\r\n"
					+ "inner join delivery_area on delivery_docket.delivery_area_id_delivery_area = delivery_area.id_delivery_area\r\n"
					+ "inner join customer on delivery_area.area_customer_id = customer.id_cust\r\n"
					+ "inner join orders on customer.id_cust = orders.customer_id_cust\r\n"
					+ "inner join order_publication on orders.customer_id_cust = order_publication.orders_id_order\r\n"
					+ "inner join publications on order_publication.publications_id_publication = publications.id_publication\r\n"
					+ "inner join delivery_driver on delivery_docket.delivery_driver_id_delivery_driver = delivery_driver.id_delivery_driver\r\n"
					+ "where deliver_area = " + docketObj.getArea() + " and DATE_FORMAT(added_date,\"%Y-%m-%d\") = '"
					+ docketObj.getDate() + "' ORDER BY id_delivery_docket ASC;");
		} catch (SQLException e) {
			resultSet = null;
		}
		return resultSet;
	}

	public ResultSet printAllDeliveryDockets() throws SQLException {
		try {
			resultSet = statement.executeQuery("select * from delivery_docket\r\n"
					+ "inner join delivery_area on delivery_docket.delivery_area_id_delivery_area = delivery_area.id_delivery_area\r\n"
					+ "inner join customer on delivery_area.area_customer_id = customer.id_cust\r\n"
					+ "inner join orders on customer.id_cust = orders.customer_id_cust\r\n"
					+ "inner join order_publication on orders.customer_id_cust = order_publication.orders_id_order\r\n"
					+ "inner join publications on order_publication.publications_id_publication = publications.id_publication\r\n"
					+ "inner join delivery_driver on delivery_docket.delivery_driver_id_delivery_driver = delivery_driver.id_delivery_driver;");
		} catch (SQLException e) {
			resultSet = null;
		}
		return resultSet;
	}

	public ResultSet returndelivery_area(int area, String type) throws SQLException {
		try {
			resultSet = statement.executeQuery(
					"select * from delivery_area inner join customer on delivery_area.area_customer_id = customer.id_cust inner join orders on customer.id_cust = orders.customer_id_cust where deliver_area = "
							+ area + " and order_type = '" + type + "';");
			System.out.println(resultSet);
		} catch (SQLException e) {
			resultSet = null;
		}
		return resultSet;
	}

	public boolean createDeliveryDocket(DeliveryDocket docketObj) throws SQLException {
		boolean docket = true;
		ResultSet result = returndelivery_area(docketObj.getArea(), docketObj.getType());
		if (result == null) {
			System.out.println("Error: SQLException wrong driverlogin sql command");
		} else {
			while (result.next()) {
				try {
					preparedStatement = connect
							.prepareStatement("INSERT INTO delivery_docket VALUES (default, ?,?,FALSE,CURDATE());");
					preparedStatement.setInt(1, docketObj.getDriverID());
					preparedStatement.setInt(2, result.getInt(1));
					preparedStatement.executeUpdate();
					System.out.println(preparedStatement);
				} catch (SQLException e) {
					docket = false;
				}
			}
		}
		return docket;
	}

	// +------------------------------INVOICE------------------------------+
	public boolean invoiceMonthly(String date) throws SQLException {
		boolean insertSucessfull = true;
		ResultSet result = invoiceReadDate(date);
		if (result == null) {
			System.out.println("Error: SQLException wrong Invoice sql command");
		} else {
			while (result.next()) {
				try {
					preparedStatement = connect
							.prepareStatement("INSERT INTO inv VALUES (default,	CURDATE(), 36, ?);");
					preparedStatement.setInt(1, result.getInt(1));
					preparedStatement.executeUpdate();
					System.out.println(preparedStatement);
				} catch (SQLException e) {
					insertSucessfull = false;
				}
			}
		}
		return insertSucessfull;
	}

	public ResultSet invoiceReadDate(String date) throws SQLException {
		// read all invoice based on month
		try {
			resultSet = statement.executeQuery("select *,sum(book_price) from delivery_docket\r\n"
					+ "inner join delivery_area on delivery_docket.delivery_area_id_delivery_area = delivery_area.id_delivery_area\r\n"
					+ "inner join customer on delivery_area.area_customer_id = customer.id_cust\r\n"
					+ "inner join orders on customer.id_cust = orders.customer_id_cust\r\n"
					+ "inner join order_publication on orders.customer_id_cust = order_publication.orders_id_order\r\n"
					+ "inner join publications on order_publication.publications_id_publication = publications.id_publication\r\n"
					+ "inner join delivery_driver on delivery_docket.delivery_driver_id_delivery_driver = delivery_driver.id_delivery_driver\r\n"
					+ "where DATE_FORMAT(added_date,\"%Y-%m\") = '" + date + "'\r\n" + "group by id_cust;");
		} catch (SQLException e) {
			resultSet = null;
		}
		return resultSet;
	}

	public void closeSQL() throws SQLException {
		try {
			connect.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
