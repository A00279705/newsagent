package run;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import classes.Area;
import classes.Customer;
import classes.DeliveryDocket;
import classes.Driver;
import classes.Invoice;
import classes.Order;
import classes.OrdersToPublication;
import classes.Publication;
import exceptionHandler.AreaExceptionHandler;
import exceptionHandler.CustomerExceptionHandler;
import exceptionHandler.DeliveryDocketException;
import exceptionHandler.DriversExceptionHandler;
import exceptionHandler.InvoiceExceptionHandler;
import exceptionHandler.OrderExceptionHandler;
import exceptionHandler.PublicationExceptionHandler;

public class MyRun {
	public static void main(String[] args) {
		try {
			MySQL sqlObject = new MySQL();
			Scanner myScanner = new Scanner(System.in);
			boolean runProgram = true;
			while (runProgram == true) {
				System.out.println("\n----options----");
				System.out.println("--Docket-- \n0 Create, 00 Read");
				System.out.println("--Order-- \n1 Create, 11 Read, 111 Update, 1111 Delete, 11111 Order 1 publication");
				System.out.println("--Order to Publication-- \n2 Create, 22 Read, 222 Delete");
				System.out
						.println("--Publication-- \n3 Create, 33 Read, 333 Update, 3333 Delete, 33333 Inc, 333333 Dec");
				System.out.println("--Customer-- \n4 Create, 44 Read, 444 Update, 4444 Delete");
				System.out.println("--Delivery Area-- \n5 Create, 55 Read, 555 Delete, 5555 Area Report");
				System.out.println("--Delivery Driver-- \n6 Create, 66 Read, 666 Update, 6666 Delete, 66666 Log In");
				System.out.println("--Invoice-- \n7 Create, 77 Delete");
				System.out.print("\nEnter: ");
				String optionEntered = myScanner.nextLine();
				switch (optionEntered) {
				case "q":
					runProgram = false;
					break;
//+------------------------------DELIVERY DOCKET------------------------------+		
				case "0":
					try {
						System.out.print("Enter Driver: ");
						int driver11 = Integer.parseInt(myScanner.nextLine().trim());
						System.out.print("Enter Order Type: ");
						String orderType = myScanner.nextLine().trim();
						System.out.print("Enter Area: ");
						int area11 = Integer.parseInt(myScanner.nextLine().trim());
						DeliveryDocket docket = new DeliveryDocket(driver11, area11, orderType);
						boolean insertRes = sqlObject.createDeliveryDocket(docket);
						if (insertRes == true) {
							System.out.println("Success: Order createDeliveryDocket Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Docket");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (DeliveryDocketException e1) {

						System.out.println("Error: DeliveryDocketException\n");
					}
					break;

				case "00":
					try {
						// 2021-03-18
						System.out.print("Enter Area: ");
						int area = Integer.parseInt(myScanner.nextLine().trim());
						System.out.print("Enter Date: ");
						String date = myScanner.nextLine().trim();
						DeliveryDocket docket = new DeliveryDocket();
						docket.dvalidateDate(date);
						docket.validate_area(area);
						ResultSet rs = sqlObject.printAlldeliveryDocket(docket);
						if (rs == null) {
							System.out.println("Error: SQLException wrong Order read sql command");
						} else {
							while (rs.next()) {
								System.out.println("ID: " + rs.getString(1) + "\t: " + rs.getString(2) + "\t: "
										+ rs.getString(3) + "\t: " + rs.getString(4) + "\t: " + rs.getString(5) + "\t: "
										+ rs.getString(6));
							}

						}
					} catch (SQLException e1) {
						System.out.println("Error: SQLException\n");
					} catch (DeliveryDocketException e1) {
						System.out.println("Error: DeliveryDocketException\n");
					}
					break;

// +------------------------------ORDER------------------------------+
				case "1":
					// create Order
					try {
						System.out.print("Enter Order Type: ");
						String orderType = myScanner.nextLine().trim();
						System.out.print("Enter Customer ID: ");
						int customerID = Integer.parseInt(myScanner.nextLine().trim());
						Order custObj = new Order(orderType, customerID);
						boolean insertResult = sqlObject.insertOrders(custObj);
						if (insertResult == true) {
							System.out.println("Success: Order insert Attempted\n");
						} else {
							System.out.println(
									"Error: SQLException wrong Order insert sql command or Customer id doesnt exist");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (OrderExceptionHandler e) {
						System.out.println("Error: OrderExceptionHandler\n");
					}
					break;

				case "11":
					// read Order
					ResultSet result = sqlObject.printAllOrders();
					if (result == null) {
						System.out.println("Error: SQLException wrong Order read sql command");
					} else {
						while (result.next()) {
							System.out.println("ID: " + result.getString(1) + "\tType: " + result.getString(2)
									+ "\tCustomer ID: " + result.getString(3));
						}
					}
					break;

				case "111":
					// update Order
					try {
						System.out.print("Enter Order ID: ");
						int orderid = Integer.parseInt(myScanner.nextLine().trim());
						System.out.print("Enter Order Type: ");
						String orderType = myScanner.nextLine().trim();
						Order ordObj = new Order();
						ordObj.validateID_order(orderid);
						ordObj.validateOrder_UpdateType(orderType);
						boolean insertResult = sqlObject.updateOrders(ordObj);
						if (insertResult == true) {
							System.out.println("Success: Order update Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Order update sql command");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (OrderExceptionHandler e) {
						System.out.println("Error: OrderExceptionHandler\n");
					}
					break;

				case "1111":
					// delete Order
					try {
						System.out.print("Enter Order ID: ");
						int orderID = Integer.parseInt(myScanner.nextLine().trim());
						Order ordObj = new Order();
						ordObj.validateID_order(orderID);
						boolean updateOrder = sqlObject.deleteOrders(ordObj.getId_order());
						if (updateOrder == true) {
							System.out.println("Success: Order delete Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Order delete sql command");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (OrderExceptionHandler e) {
						System.out.println("Error: OrderExceptionHandler\n");
					}
					break;

				case "11111":
					// create Order and ord to publication same time
					try {
						System.out.print("Enter Order Type: ");
						String orderType = myScanner.nextLine().trim();
						System.out.print("Enter Customer ID: ");
						int customerID = Integer.parseInt(myScanner.nextLine().trim());
						System.out.print("Enter Publication ID: ");
						int publicationID = Integer.parseInt(myScanner.nextLine().trim());
						Order custObja = new Order(orderType, customerID);
						OrdersToPublication custObjb = new OrdersToPublication(1, publicationID);
						boolean insertResult = sqlObject.insertOrders1(custObja, custObjb);
						if (insertResult == true) {
							System.out.println("Success: Order insert Attempted\n");
						} else {
							System.out.println(
									"Error: SQLException wrong Order insert sql command or Customer id doesnt exist");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (OrderExceptionHandler e) {
						System.out.println("Error: OrderExceptionHandler\n");
					}
					break;

//+------------------------------ORDER TO PUBLICATION------------------------------+	
				case "2":
					// create Order to Publication
					try {
						System.out.print("Enter Order ID: ");
						int orderID = Integer.parseInt(myScanner.nextLine().trim());
						System.out.print("Enter Publication ID: ");
						int publicationID = Integer.parseInt(myScanner.nextLine().trim());
						OrdersToPublication custObj = new OrdersToPublication(orderID, publicationID);
						boolean insertResult = sqlObject.insertOrdPub(custObj);
						if (insertResult == true) {
							System.out.println("Success: Ord To Pub insert Attempted\n");
						} else {
							System.out.println(
									"Error: SQLException wrong Ord To Pub insert sql command or order,publication id doesnt exist");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (OrderExceptionHandler e) {
						System.out.println("Error: OrderExceptionHandlerr\n");
					}
					break;

				case "22":
					// read Order to Publication
					ResultSet resultOrdPub = sqlObject.printAllOrdersToPublication();
					if (resultOrdPub == null) {
						System.out.println("Error: SQLException wrong Ord To Pub read sql command");
					} else {
						while (resultOrdPub.next()) {
							System.out.println("ID: " + resultOrdPub.getString(1) + "\tID Order: "
									+ resultOrdPub.getString(2) + "\tID Customer: " + resultOrdPub.getString(3));
						}
					}
					break;

				case "222":
					// delete Order to Publication
					try {
						System.out.print("Enter Order ID: ");
						int ordToPubID = Integer.parseInt(myScanner.nextLine().trim());
						OrdersToPublication ordtopubObj = new OrdersToPublication();
						ordtopubObj.validateOrdProdID_ordprod(ordToPubID);
						boolean updateOrder = sqlObject.deleteOrderToPublication(ordtopubObj.getId_ordprod());
						if (updateOrder == true) {
							System.out.println("Success: Ord To Pub delete Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Ord To Pub delete sql command");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (OrderExceptionHandler e) {
						System.out.println("Error: OrderExceptionHandler\n");
					}
					break;

//+------------------------------PUBLICATION------------------------------+
				case "3":
					// create Publication
					try {
						System.out.print("Enter Book Name: ");
						String book_name = myScanner.nextLine().trim();
						System.out.print("Enter Book Price: ");
						int book_price = Integer.parseInt(myScanner.nextLine().trim());
						System.out.print("Enter Book Amount: ");
						int book_amount = Integer.parseInt(myScanner.nextLine().trim());
						Publication custObj = new Publication(book_name, book_price, book_amount);
						boolean insertResult = sqlObject.insertPublication(custObj);
						if (insertResult == true) {
							System.out.println("Success: Publication insert Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Publication insert sql command");
						}
					} catch (NumberFormatException e) {
						System.out.println("Error: NumberFormatException\\n");
					} catch (PublicationExceptionHandler e) {
						System.out.println("Error: PublicationExceptionHandler\n");
					}
					break;

				case "33":
					// read Publication
					ResultSet resultPub = sqlObject.printAllPublications();
					if (resultPub == null) {
						System.out.println("Error: SQLException wrong Publication read sql command");
					} else {
						while (resultPub.next()) {
							System.out.println("Publication ID: " + resultPub.getString(1) + "\tBook Name: "
									+ resultPub.getString(2) + "\t\tBook Price: " + resultPub.getString(3)
									+ "\tBook Amount:" + resultPub.getString(4));
						}
					}
					break;

				case "333":
					// update Publication
					try {
						System.out.print("Enter Publication ID: ");
						int id_publication = Integer.parseInt(myScanner.nextLine().trim());
						System.out.print("Enter Book Name: ");
						String book_name = myScanner.nextLine().trim();
						System.out.print("Enter Book Price: ");
						int book_price = Integer.parseInt(myScanner.nextLine().trim());
						System.out.print("Enter Book Amount: ");
						int book_amount = Integer.parseInt(myScanner.nextLine().trim());
						Publication pubObj = new Publication();
						pubObj.validateId_publication(id_publication);
						pubObj.validateBook_name(book_name);
						pubObj.validateBook_price(book_price);
						pubObj.validateBook_amount(book_amount);
						boolean insertResult = sqlObject.updatePublication(pubObj);
						if (insertResult == true) {
							System.out.println("Success: Publication update Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Publication update sql command");
						}
					} catch (NumberFormatException e) {
						System.out.println("Error: NumberFormatException\n");
					} catch (PublicationExceptionHandler e) {
						System.out.println("Error: PublicationExceptionHandler\n");
					}
					break;

				case "3333":
					// delete Publication
					try {
						System.out.print("Enter Publication ID: ");
						int id_publication = Integer.parseInt(myScanner.nextLine().trim());
						Publication pubObj = new Publication();
						pubObj.validateId_publication(id_publication);
						boolean updateOrder = sqlObject.deletePublications(pubObj.getId_publication());
						if (updateOrder == true) {
							System.out.println("Success: Publication Delete Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Publication delete sql command");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (PublicationExceptionHandler e) {
						System.out.println("Error: PublicationExceptionHandler\n");
					}
					break;

				case "33333":
					// increment publication
					try {
						System.out.println("Enter Publication ID to increment:");
						int id_publication = Integer.parseInt(myScanner.nextLine().trim());
						Publication pubObj = new Publication();
						pubObj.validateId_publication(id_publication);
						boolean insertResult = sqlObject.incrementPublication(pubObj);
						if (insertResult == true) {
							System.out.println("Success: Publication Stock increment Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Publication increment sql command");
						}
					} catch (NumberFormatException e) {
						System.out.println("Error: NumberFormatException\n");
					} catch (PublicationExceptionHandler e) {
						System.out.println("Error: PublicationExceptionHandler\n");
					}
					break;

				case "333333":
					// decrement publication
					try {
						System.out.println("Enter Publication ID to decrement:");
						int id_publication = Integer.parseInt(myScanner.nextLine().trim());
						Publication pubObj = new Publication();
						pubObj.validateId_publication(id_publication);
						boolean insertResult = sqlObject.decrementPublication(pubObj);
						if (insertResult == true) {
							System.out.println("Success: Publication Stock decremented Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Publication decrement sql command");
						}
					} catch (NumberFormatException e) {
						System.out.println("Error: NumberFormatException\n");
					} catch (PublicationExceptionHandler e) {
						System.out.println("Error: PublicationExceptionHandler\n");
					}
					break;
//+------------------------------CUSTOMER ACTIONS------------------------------+					
				case "4":
					// create Customer
					try {
						System.out.print("Enter Customer First Name: ");
						String Fname = myScanner.nextLine().trim();
						System.out.print("Enter Customer Last Name: ");
						String Lname = myScanner.nextLine().trim();
						System.out.print("Enter Customer Address (street name and house/apt number): ");
						String Address = myScanner.nextLine().trim();
						System.out.print("Enter Customer Delivery area (1-24): ");
						String DA = myScanner.nextLine().trim();
						System.out.print("Enter Customer Phone Number: ");
						String phone = myScanner.nextLine().trim();
						// Create the customer object & validate details
						Customer newCust = new Customer(Fname, Lname, Address, DA, phone);
						newCust.validateFirstName(Fname);
						newCust.validateLastName(Lname);
						newCust.validateAddress(Address);
						newCust.validateCustomer_UpdateDeliveryArea(DA);
						newCust.validateCustomer_UpdatePhone(phone);
						boolean insertResult = sqlObject.insertCustomer(newCust);
						if (insertResult == true) {
							System.out.println("Success: Customer Insert Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Customer insert sql command\\n");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (CustomerExceptionHandler e) {
						System.out.println("Error: CustomerExceptionHandler\n");
					}
					break;

				case "44":
					// read Customers
					ResultSet resultCust = sqlObject.printAllCustomers();
					if (resultCust == null) {
						System.out.println("Error: SQLException wrong read sql command\n");
					} else {
						while (resultCust.next()) {
							System.out.println("Customer ID: " + resultCust.getString(1) + "\tFirst Name: "
									+ resultCust.getString(2) + "\tLast Name: " + resultCust.getString(3)
									+ "\tAddress: " + resultCust.getString(4) + "\tDelivery Area: "
									+ resultCust.getString(5) + "\tPhone: " + resultCust.getString(6));
						}
					}
					break;

				case "444":
					// Update Customer
					try {
						System.out.print("Enter Customer ID: ");
						int ID = Integer.parseInt(myScanner.nextLine().trim());
						System.out.print("Enter Customer First Name: ");
						String Fname = myScanner.nextLine().trim();
						System.out.print("Enter Customer Last Name: ");
						String Lname = myScanner.nextLine().trim();
						System.out.print("Enter Customer Address (street name and house/apt number): ");
						String Address = myScanner.nextLine().trim();
						System.out.print("Enter Customer Delivery area (1-24): ");
						String DA = myScanner.nextLine().trim();
						System.out.print("Enter Customer Phone Number: ");
						String phone = myScanner.nextLine().trim();
						Customer updateCust = new Customer();
						updateCust.validateCustomer_UpdateFirstName(Fname);
						updateCust.validateCustomer_UpdateLastName(Lname);
						updateCust.validateCustomer_UpdateAddress(Address);
						updateCust.validateCustomer_UpdateDeliveryArea(DA);
						updateCust.validateCustomer_UpdatePhone(phone);
						updateCust.validateID_customer(ID);
						boolean insertResult = sqlObject.updateCustomer(updateCust);
						if (insertResult == true) {
							System.out.println("Success: Customer Update Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Customer update sql command\n");
						}
					} catch (NumberFormatException e) {
						System.out.println("Error: NumberFormatException\n");
					} catch (CustomerExceptionHandler e) {
						System.out.println("Error: CustomerExceptionHandler\n");
					}
					break;

				case "4444":
					// delete Customer
					try {
						System.out.print("Enter Customer ID: ");
						int custID = Integer.parseInt(myScanner.nextLine().trim());
						System.out.println(custID);
						Customer custObj = new Customer();
						custObj.validateID_customer(custID);
						boolean updateCustomer = sqlObject.deleteCustomer(custObj.getId());
						if (updateCustomer == true) {
							System.out.println("Success: Customer Delete Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Customer delete sql command");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (CustomerExceptionHandler e) {
						System.out.println("Error: CustomerExceptionHandler\n");
					}
					break;
//+------------------------------DELIVERY AREA------------------------------+	
				case "5":
					// create delivery area
					try {
						System.out.print("Enter Customer ID: ");
						int id_customer = Integer.parseInt(myScanner.nextLine().trim());
						System.out.print("Enter Driver ID: ");
						int id_driver = Integer.parseInt(myScanner.nextLine().trim());
						Area areaObj = new Area(id_customer, id_driver);
						boolean insertResult = sqlObject.insertArea(areaObj);
						if (insertResult == true) {
							System.out.println("Success: Area Insert Attempted\n");
						} else {
							System.out
									.println("Error: SQLException wrong Area insert sql command or customer,driver id");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (AreaExceptionHandler e) {
						System.out.println("Error: AreaExceptionHandler\n");
					}
					break;

				case "55":
					// read delivery area
					ResultSet result1 = sqlObject.printArea();
					if (result1 == null) {
						System.out.println("Error: SQLException wrong Area sql command");
					} else {
						while (result1.next()) {
							System.out.println("ID_Area: " + result1.getString(1) + "\tID_Customer: "
									+ result1.getString(2) + "\tID_Driver: " + result1.getString(3));
						}
					}
					break;

				case "555":
					// delete Delivery Area
					try {
						System.out.print("Enter Area ID: ");
						int id_area = Integer.parseInt(myScanner.nextLine().trim());
						Area areaObj = new Area();
						areaObj.validateId_area(id_area);
						boolean updateArea = sqlObject.deleteArea(areaObj.getId_area());
						if (updateArea == true) {
							System.out.println("Success: Area Delete Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Area delete sql command");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (AreaExceptionHandler e) {
						System.out.println("Error: AreaExceptionHandler\n");
					}
					break;

				case "5555": // Generate delivery area
					try {
						System.out.print("Enter Order Type: ");
						String orderTypea = myScanner.nextLine().trim();
						Order ordObj1 = new Order();
						ordObj1.validateOrder_UpdateType(orderTypea);
						ResultSet generate = sqlObject.generateDeliveryAreaReport(ordObj1);
						if (generate == null) {
							System.out.println("sql invalid");
						} else {
							while (generate.next()) {
								System.out.println(
										"ID_Area: " + generate.getString(1) + ",\tID_Driver: " + generate.getString(4)
												+ ",\tDriver Name: " + generate.getString(5) + ",\tCustomer ID: "
												+ generate.getString(7) + ",\tCustomer Name: " + generate.getString(8)
												+ ",\tCustomer Surname: " + generate.getString(9) + ",\tDeliver Area: "
												+ generate.getString(11) + ",\tOrder Type: " + generate.getString(14));
							}
						}
					} catch (NumberFormatException e) {
						System.out.println("Error: NumberFormatException\n");
					} catch (OrderExceptionHandler e) {
						System.out.println("Error: OrderExceptionHandler\n");
					}
					break;

				case "6":
					// insert Driver
					try {
						System.out.print("Enter Driver name: ");
						String driver_name = myScanner.nextLine().trim();
						System.out.print("Enter Driver Password: ");
						String driver_password = myScanner.nextLine().trim();
						Driver driverObj = new Driver(driver_name, driver_password);
						boolean insertResult = sqlObject.insertdelivery_driver(driverObj);
						if (insertResult == true) {
							System.out.println("Success: Order insert Attempted\n");
						} else {
							System.out.println(
									"Error: SQLException wrong Order insert sql command or Customer id doesnt exist");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (DriversExceptionHandler e) {
						System.out.println("Error: OrderExceptionHandler\n");
					}
					break;

				case "66":
					// read delivery driver
					ResultSet resultdriver = sqlObject.printAlldelivery_drivers();
					if (resultdriver == null) {
						System.out.println("Error: SQLException wrong Area sql command");
					} else {
						while (resultdriver.next()) {
							System.out.println("ID_Driver: " + resultdriver.getString(1) + "\tName: "
									+ resultdriver.getString(2) + "\tPassword: " + resultdriver.getString(3));
						}
					}
					break;

				case "666":
					// Update delivery driver
					try {
						System.out.print("Enter Driver ID: ");
						int ID = Integer.parseInt(myScanner.nextLine().trim());
						System.out.print("Enter Driver Name: ");
						String Fname = myScanner.nextLine().trim();
						System.out.print("Enter Driver Password : ");
						String password12 = myScanner.nextLine().trim();
						Driver updatedDriver = new Driver();
						updatedDriver.ValidateUpdateName_driver(Fname);
						updatedDriver.ValidateUpdatePassword_driver(password12);
						updatedDriver.validateID_driver(ID);
						boolean insertResult = sqlObject.updateDriver(updatedDriver);
						if (insertResult == true) {
							System.out.println("Success: Driver Update Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Driver update sql command\n");
						}
					} catch (NumberFormatException e) {
						System.out.println("Error: NumberFormatException\n");
					} catch (DriversExceptionHandler e) {
						System.out.println("Error: DriverExceptionHandler\n");
					}
					break;

				case "6666":
					// delete Delivery Driver
					try {
						System.out.print("Enter Driver ID: ");
						int DriverID = Integer.parseInt(myScanner.nextLine().trim());
						System.out.println(DriverID);
						Driver driverObj = new Driver();
						driverObj.validateID_driver(DriverID);
						boolean updateDriver = sqlObject.deleteDriver(driverObj);
						if (updateDriver == true) {
							System.out.println("Success: Driver Delete Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Driver delete sql command");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (DriversExceptionHandler e) {
						System.out.println("Error: DriverExceptionHandler\n");
					}

					break;

				case "66666":
					try {
						System.out.print("Enter Name: ");
						String driver_name1 = myScanner.nextLine().trim();
						System.out.print("Enter Password: ");
						String driver_password1 = myScanner.nextLine().trim();

						Driver objDriver1 = new Driver(driver_name1, driver_password1);

						boolean insertResult11 = sqlObject.DriverLogin(objDriver1);
						if (insertResult11 == true) {
							System.out.println("Logged In");
						} else
							System.out.println("Failed to login");

					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (DriversExceptionHandler e) {
						System.out.println("Error: DriversExceptionHandler\n");
					}
					break;

				case "7":
					try {
						// "2021-02"
						System.out.print("Enter Year and month: ");
						String date1 = myScanner.nextLine().trim();
						boolean createInvoice = sqlObject.invoiceMonthly(date1);
						if (createInvoice == true) {
							System.out.println("Success: Invoice Delete Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Invoice delete sql command");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					}
					break;

			
				case "77":
					// delete Invoice
					try {
						System.out.print("Enter Invoice ID: ");
						int id_invoice = Integer.parseInt(myScanner.nextLine().trim());
						Invoice invoiceObj = new Invoice();
						invoiceObj.validateId_invoice(id_invoice);
						boolean deleteInvoice = sqlObject.deleteInvoice(invoiceObj.getID());
						if (deleteInvoice == true) {
							System.out.println("Success: Invoice Delete Attempted\n");
						} else {
							System.out.println("Error: SQLException wrong Invoice delete sql command");
						}
					} catch (NumberFormatException E) {
						System.out.println("Error: NumberFormatException\n");
					} catch (InvoiceExceptionHandler e) {
						System.out.println("Error: InvoiceExceptionHandler\n");
					}
					break;
				}
			}
			myScanner.close();
		} catch (SQLException e) {
			System.out.println("Error: SQLException wrong Server sql command\n");
		}
	}
}