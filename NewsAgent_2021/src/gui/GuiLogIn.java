package gui;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import classes.Driver;
import exceptionHandler.DriversExceptionHandler;
import run.MySQL;

public class GuiLogIn {

	private JFrame frame;
	private JTextField name1;
	private JTextField password1;
	private JLabel message1;

	public GuiLogIn() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1110, 670);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton loginButton1 = new JButton("Log In");
		loginButton1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String name = name1.getText();
					String password = password1.getText();
					message1.setText("Warning");
					MySQL sqlObject = new MySQL();
					Driver objDriver1 = new Driver(name, password);
					boolean insertResult11 = sqlObject.DriverLogin(objDriver1);
					if (insertResult11 == true) {
						name1.setText("");
						password1.setText("");
						GuiDeliveryDocket docket = new GuiDeliveryDocket();
						docket.frameOn();
						frameOff();
						System.out.println("Logged In");
					} else {
						message1.setText("Failed Log In");
						System.out.println("Failed to login");
					}
					sqlObject.closeSQL();
				} catch (DriversExceptionHandler e1) {
					message1.setText("DriversExceptionHandler");
					System.out.println("Error: DriversExceptionHandler\n");
				} catch (SQLException e1) {
					message1.setText("SQLException");
					System.out.println("Error: SQLException");
				}
			}
		});
		loginButton1.setBounds(447, 260, 200, 30);
		frame.getContentPane().add(loginButton1);

		name1 = new JTextField();
		name1.setBounds(447, 160, 200, 30);
		frame.getContentPane().add(name1);
		name1.setColumns(10);

		password1 = new JTextField();
		password1.setColumns(10);
		password1.setBounds(447, 210, 200, 30);
		frame.getContentPane().add(password1);

		JLabel lblNewLabel = new JLabel("Enter Password:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(317, 210, 130, 30);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblEnterName = new JLabel("Enter Name:");
		lblEnterName.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblEnterName.setBounds(317, 160, 130, 30);
		frame.getContentPane().add(lblEnterName);

		message1 = new JLabel("Warning");
		message1.setBackground(new Color(0, 0, 0));
		message1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		message1.setBounds(317, 260, 130, 30);
		frame.getContentPane().add(message1);

		JLabel lblLogIn = new JLabel("Log In");
		lblLogIn.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblLogIn.setBounds(497, 78, 100, 40);
		frame.getContentPane().add(lblLogIn);

		JButton btnNewButton = new JButton("Return Main Menu");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiMain frameMain = new GuiMain();
				frameMain.frameOn();
				frameOff();
			}
		});
		btnNewButton.setBounds(10, 11, 150, 30);
		frame.getContentPane().add(btnNewButton);
		frame.setVisible(true);
	}

	public void frameOn() {
		frame.setVisible(true);
	}

	public void frameOff() {
		frame.setVisible(false);
	}
}
