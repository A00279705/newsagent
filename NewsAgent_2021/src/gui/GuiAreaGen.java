package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import classes.Order;
import exceptionHandler.OrderExceptionHandler;
import run.MySQL;

public class GuiAreaGen {

	private static final Order ordObj1 = null;
	private JFrame frame;
	private JTable table;
	private JTextField textOrderType;
	private DefaultTableModel tableModel;

	public GuiAreaGen() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1110, 670);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(277, 150, 540, 267);
		frame.getContentPane().add(scrollPane);

		tableModel = new DefaultTableModel(new String[] { "Area ID", " Deli Area ID", "Cust ID", "Name", "Surname",
				"Door Number", "Area Name", "Order Type" }, 0);
		table = new JTable(tableModel);
		scrollPane.setViewportView(table);

		JLabel lblDeliveryArea_1 = new JLabel("Delivery Area");
		lblDeliveryArea_1.setForeground(Color.RED);
		lblDeliveryArea_1.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblDeliveryArea_1.setBounds(440, 11, 213, 40);
		frame.getContentPane().add(lblDeliveryArea_1);

		textOrderType = new JTextField();
		textOrderType.setColumns(10);
		textOrderType.setBounds(100, 180, 150, 30);
		frame.getContentPane().add(textOrderType);

		JLabel lblNewLabel_12 = new JLabel("> Generate Del Area Report <");
		lblNewLabel_12.setForeground(Color.BLUE);
		lblNewLabel_12.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_12.setBounds(10, 155, 275, 20);
		frame.getContentPane().add(lblNewLabel_12);

		JLabel lblNewLabel_13 = new JLabel("Order Type");
		lblNewLabel_13.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_13.setBounds(20, 180, 80, 30);
		frame.getContentPane().add(lblNewLabel_13);

//**************************************************************************    generate delivery area

		JButton btnGenDelArea = new JButton("Area Report");
		btnGenDelArea.setBackground(Color.GREEN);
		btnGenDelArea.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnGenDelArea.setBounds(100, 230, 150, 30);
		frame.getContentPane().add(btnGenDelArea);

		// JButton btnread = new JButton("Read Docket");
		btnGenDelArea.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					tableModel.setRowCount(0);
					MySQL sqlObject = new MySQL();
					String order_type = textOrderType.getText();
					Order ordObj1 = new Order();
					ordObj1.validateOrder_UpdateType(order_type);
					ResultSet rs = sqlObject.generateDeliveryAreaReport(ordObj1);
					textOrderType.setText("");
					if (rs == null) {
						System.out.println("Error: SQLException wrong Order read sql command");
					} else {
						while (rs.next()) {
							Object rowData[] = new Object[8];
							rowData[0] = rs.getString(1);
							rowData[1] = rs.getString(2);
							rowData[2] = rs.getString(4);
							rowData[3] = rs.getString(5);
							rowData[4] = rs.getString(6);
							rowData[5] = rs.getString(7);
							rowData[6] = rs.getString(8);
							rowData[7] = rs.getString(11);
							tableModel.addRow(rowData);
						}
					}
				} catch (SQLException e1) {
					System.out.println("Error: SQLException\n");
				} catch (OrderExceptionHandler e1) {
					System.out.println("Error: OrderExceptionHandler\n");
				}
			}
		});

		JLabel lblNewLabel_1_2_2_1 = new JLabel(">Generate Delivery Area<");
		lblNewLabel_1_2_2_1.setForeground(Color.BLUE);
		lblNewLabel_1_2_2_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_2_2_1.setBounds(412, 109, 270, 30);
		frame.getContentPane().add(lblNewLabel_1_2_2_1);
		
		JButton btnNewButton = new JButton("Return Main Menu");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiMain frameMain = new GuiMain();
				frameMain.frameOn();
				frameOff();
			}
		});
		btnNewButton.setBounds(20, 11, 150, 30);
		frame.getContentPane().add(btnNewButton);
		// *********************************************************** Area Table
	}

	public void areaTable() throws SQLException {
		tableModel.setRowCount(0);
		try {

			MySQL sqlObject = new MySQL();

			// int area = Integer.parseInt(area2.getText());
			String order_type = textOrderType.getText();
			Order ordObj1 = new Order();
			ordObj1.validateOrder_UpdateType(order_type);
			ResultSet rs = sqlObject.generateDeliveryAreaReport(ordObj1);
			if (rs == null) {
				System.out.println("Error: SQLException wrong Order read sql command");
			} else {
				while (rs.next()) {
					Object rowData[] = new Object[8];
					rowData[0] = rs.getString(1);
					rowData[1] = rs.getString(11);
					rowData[2] = rs.getString(4);
					rowData[3] = rs.getString(5);
					rowData[4] = rs.getString(7);
					rowData[5] = rs.getString(8);
					rowData[6] = rs.getString(9);
					rowData[7] = rs.getString(14);
					// rowData[8] = rs.getString(14);
					tableModel.addRow(rowData);
				}

			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (OrderExceptionHandler e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

///////
	/*
	 * MySQL sqlObject = new MySQL(); ResultSet rs = sqlObject.printArea(); //on
	 * MyRun.java case 55
	 * 
	 * while (rs.next()) {
	 * 
	 * Object rowData[] = new Object[3]; rowData[0] = rs.getString(1); rowData[1] =
	 * rs.getString(2); rowData[2] = rs.getString(3);
	 * 
	 * tableModel.addRow(rowData);
	 */

////////
	/*
	 * Order ordObj1 = new Order(); ordObj1.validateOrder_UpdateType(orderType);
	 * MySQL sqlObject = new MySQL(); ResultSet generate =
	 * sqlObject.generateDeliveryAreaReport(ordObj1); //on MyRun.java case 5555
	 * 
	 * while (generate.next()) { Object rowData[] = new Object[8];
	 * 
	 * rowData[0] = generate.getString(1); rowData[1] = generate.getString(11);
	 * rowData[2] = generate.getString(4); rowData[3] = generate.getString(5);
	 * rowData[4] = generate.getString(7); rowData[5] = generate.getString(8);
	 * rowData[6] = generate.getString(9); rowData[7] = generate.getString(14);
	 * 
	 * tableModel.addRow(rowData);
	 * 
	 * } } catch (SQLException e1) { System.err.println("Error SQLException\n" +
	 * e1.toString()); }
	 */
//////////////
	/*
	 * //String orderType = myScanner.nextLine().trim(); //int id_customer =
	 * Integer.parseInt(txtCustomer_ID.getText());
	 * 
	 * String order_type = textOrderType.getText();
	 * 
	 * MySQL sqlObject = new MySQL(); //ResultSet rs = sqlObject.printArea();
	 * 
	 * System.out.print("Enter Order Type: ");
	 * 
	 * Order ordObj = new Order(); ordObj.validateOrder_UpdateType(order_type);
	 * 
	 * ResultSet generate = sqlObject.generateDeliveryAreaReport(ordObj); if
	 * (generate == null) { System.out.println("sql invalid"); } else { while
	 * (generate.next()) { System.out.println( "ID_Area: " + generate.getString(1) +
	 * ",\tID_Driver: " + generate.getString(4) + ",\tDriver Name: " +
	 * generate.getString(5) + ",\tCustomer ID: " + generate.getString(7) +
	 * ",\tCustomer Name: " + generate.getString(8) + ",\tCustomer Surname: " +
	 * generate.getString(9) + ",\tDeliver Area: " + generate.getString(11) +
	 * ",\tOrder Type: " + generate.getString(14)); } } } catch
	 * (NumberFormatException e) {
	 * System.out.println("Error: NumberFormatException\n"); } catch
	 * (OrderExceptionHandler e) {
	 * System.out.println("Error: OrderExceptionHandler\n"); }
	 * 
	 * 
	 * 
	 * }
	 */
	public void frameOn() {
		frame.setVisible(true);
	}

	public void frameOff() {
		frame.setVisible(false);
	}
}

//*************************************************************************
