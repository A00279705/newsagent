package gui;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import classes.Customer;
import exceptionHandler.CustomerExceptionHandler;
import run.MySQL;

public class Gui_Customer {
	private JFrame frame;
	private JTable table;
	private JTextField deleteCustTxt;
	private DefaultTableModel tableModel;
	private JTextField fnameTxt;
	private JTextField lnameTxt;
	private JTextField addressTxt;
	private JTextField deliveryTxt;
	private JTextField phoneTxt;
	private JTextField updateIDTxt;
	private JTextField updateFnameTxt;
	private JTextField updateLnameTxt;
	private JTextField updateAddressTxt;
	private JTextField updateDeliveryTxt;
	private JTextField updatePhoneTxt;

	public Gui_Customer() {

		frame = new JFrame();
		frame.setBounds(100, 100, 1110, 670);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblCustomerMenu = new JLabel("Customer Menu");
		lblCustomerMenu.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblCustomerMenu.setBounds(389, 10, 330, 40);
		frame.getContentPane().add(lblCustomerMenu);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(284, 116, 525, 273);
		frame.getContentPane().add(scrollPane);

		tableModel = new DefaultTableModel(new String[] { "ID", "First Name", "Last Name", "Address", "Delivery Area", "Phone" }, 0);
		table = new JTable(tableModel);
		scrollPane.setViewportView(table);

		JLabel LabelFirstName = new JLabel("First Name:");
		LabelFirstName.setFont(new Font("Tahoma", Font.PLAIN, 10));
		LabelFirstName.setBounds(10, 120, 80, 30);
		frame.getContentPane().add(LabelFirstName);

	

		JLabel LabelCreateCust = new JLabel(">Create Customer<");
		LabelCreateCust.setFont(new Font("Tahoma", Font.BOLD, 18));
		LabelCreateCust.setBounds(20, 80, 270, 30);
		frame.getContentPane().add(LabelCreateCust);

		JButton btnDeleteCust = new JButton("Delete Customer");
		btnDeleteCust.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					MySQL sqlObject = new MySQL();
					int id = Integer.parseInt(deleteCustTxt.getText());
					Customer custObj = new Customer();
					custObj.validateID_customer(id);
					boolean deleteCustomer = sqlObject.deleteCustomer(id);
					if (deleteCustomer == true) {
						System.out.println("Success: Customer Delete Attempted\n");
					} else {
						System.out.println("Error: SQLException wrong Driver delete sql command");
					}
					customerTable();
				}catch (NumberFormatException eee) {
					System.out.println("Error: NumberFormatException\n");
				} catch (CustomerExceptionHandler ee) {
					System.out.println("Error: CustomerExceptionHandler\n");
				} catch (SQLException e1) {
					System.out.println("Error: SQLException");
				}
			}
				
		});
		btnDeleteCust.setBounds(33, 444, 150, 30);
		frame.getContentPane().add(btnDeleteCust);

		deleteCustTxt = new JTextField();
		deleteCustTxt.setColumns(10);
		deleteCustTxt.setBounds(113, 415, 96, 19);
		frame.getContentPane().add(deleteCustTxt);

		JLabel LabelDeleteCust = new JLabel(">Delete Customer<");
		LabelDeleteCust.setFont(new Font("Tahoma", Font.BOLD, 18));
		LabelDeleteCust.setBounds(10, 375, 270, 30);
		frame.getContentPane().add(LabelDeleteCust);

		JLabel lblNewLabel_1_3 = new JLabel("Enter Customer ID:");
		lblNewLabel_1_3.setBounds(10, 415, 96, 19);
		frame.getContentPane().add(lblNewLabel_1_3);

		JLabel LabelUpdateCustomer = new JLabel(">Update Customer<");
		LabelUpdateCustomer.setFont(new Font("Tahoma", Font.BOLD, 18));
		LabelUpdateCustomer.setBounds(879, 80, 207, 30);
		frame.getContentPane().add(LabelUpdateCustomer);

		JLabel labelUpdateFName = new JLabel("First Name:");
		labelUpdateFName.setBounds(844, 256, 112, 30);
		frame.getContentPane().add(labelUpdateFName);

		JLabel labelUpdateLName = new JLabel("Last Name:");
		labelUpdateLName.setFont(new Font("Tahoma", Font.PLAIN, 10));
		labelUpdateLName.setBounds(844, 282, 112, 30);
		frame.getContentPane().add(labelUpdateLName);

	

		JButton btnUpdateCustomer = new JButton("Update Customer");
		btnUpdateCustomer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					MySQL sqlObject = new MySQL();
					int id = Integer.parseInt(updateIDTxt.getText());
					String Fname = updateFnameTxt.getText();
					String Lname = updateLnameTxt.getText();
					String Address = updateAddressTxt.getText();
					String DA = updateDeliveryTxt.getText();
					String Phone = updatePhoneTxt.getText();
					Customer updateCust = new Customer();
					updateCust.validateCustomer_UpdateFirstName(Fname);
					updateCust.validateCustomer_UpdateLastName(Lname);
					updateCust.validateCustomer_UpdateAddress(Address);
					updateCust.validateCustomer_UpdateDeliveryArea(DA);
					updateCust.validateCustomer_UpdatePhone(Phone);
					updateCust.validateID_customer(id);
					
					boolean updateCustomer = sqlObject.updateCustomer(updateCust);
					if(updateCustomer == true) {
						System.out.println("Success: Customer Update Attempted\n");
					} else {
						System.out.println("Error: SQLException wrong Customer update sql command\n");
					}
					customerTable();
				} catch (NumberFormatException e1) {
					System.out.println("Error: NumberFormatException\n");
				} catch (CustomerExceptionHandler e1) {
					System.out.println("Error: CustomerExceptionHandler\n");
				} catch (SQLException e1) {
					System.out.println("Error: SQLException");
				}
				}
			
		});
		btnUpdateCustomer.setBounds(844, 428, 150, 30);
		frame.getContentPane().add(btnUpdateCustomer);

		JLabel LabelUpdateID = new JLabel("Enter Customer ID:");
		LabelUpdateID.setBounds(844, 223, 112, 30);
		frame.getContentPane().add(LabelUpdateID);

		
		
		JLabel LabelCustomerRead = new JLabel(">Read Customers<");
		LabelCustomerRead.setFont(new Font("Tahoma", Font.BOLD, 18));
		LabelCustomerRead.setBounds(456, 80, 189, 30);
		frame.getContentPane().add(LabelCustomerRead);
		
		JLabel LabelLastName = new JLabel("Last Name:");
		LabelLastName.setFont(new Font("Tahoma", Font.PLAIN, 10));
		LabelLastName.setBounds(10, 151, 80, 30);
		frame.getContentPane().add(LabelLastName);
		
		JLabel LabelAddress = new JLabel("Address:");
		LabelAddress.setFont(new Font("Tahoma", Font.PLAIN, 10));
		LabelAddress.setBounds(10, 188, 80, 30);
		frame.getContentPane().add(LabelAddress);
		
		JLabel LabelDelivery = new JLabel("Delivery Area:");
		LabelDelivery.setFont(new Font("Tahoma", Font.PLAIN, 10));
		LabelDelivery.setBounds(10, 230, 80, 16);
		frame.getContentPane().add(LabelDelivery);
		
		JLabel LabelPhone = new JLabel("Phone:");
		LabelPhone.setFont(new Font("Tahoma", Font.PLAIN, 10));
		LabelPhone.setBounds(10, 256, 80, 30);
		frame.getContentPane().add(LabelPhone);
		
		fnameTxt = new JTextField();
		fnameTxt.setBounds(100, 130, 96, 19);
		frame.getContentPane().add(fnameTxt);
		fnameTxt.setColumns(10);
		
		lnameTxt = new JTextField();
		lnameTxt.setColumns(10);
		lnameTxt.setBounds(100, 157, 96, 19);
		frame.getContentPane().add(lnameTxt);
		
		addressTxt = new JTextField();
		addressTxt.setColumns(10);
		addressTxt.setBounds(100, 191, 96, 19);
		frame.getContentPane().add(addressTxt);
		
		deliveryTxt = new JTextField();
		deliveryTxt.setColumns(10);
		deliveryTxt.setBounds(100, 230, 96, 19);
		frame.getContentPane().add(deliveryTxt);
		
		phoneTxt = new JTextField();
		phoneTxt.setColumns(10);
		phoneTxt.setBounds(100, 262, 96, 19);
		frame.getContentPane().add(phoneTxt);
		
		JButton btnCreateCust = new JButton("Create Customer");
		btnCreateCust.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try{
				String fname = fnameTxt.getText();
				String lname = lnameTxt.getText();
				String address = addressTxt.getText();
				String delivery = deliveryTxt.getText();
				String phone = phoneTxt.getText();
				MySQL sqlObject = new MySQL();
				Customer custObj = new Customer();
				custObj.validateFirstName(fname);
				custObj.validateLastName(lname);
				custObj.validateAddress(address);
				custObj.validateCustomer_UpdateDeliveryArea(delivery);
				custObj.validatePhone(phone);
				boolean insertResult = sqlObject.insertCustomer(custObj);
				if (insertResult == true) {
					fnameTxt.setText("");
					lnameTxt.setText("");
					addressTxt.setText("");
					deliveryTxt.setText("");
					phoneTxt.setText("");
					System.out.println("Success: Customer insert Attempted\n");
				}
				else {
					System.out.println(
							"Error: SQLException wrong Customer insert sql command or Customer id doesnt exist");
				}
				customerTable();
				sqlObject.closeSQL();
				} 
				catch (SQLException e1) {
					System.out.println("Error: SQLException");
				} catch (CustomerExceptionHandler e1) {
					System.out.println("Error: CustomerExceptionHandler");
				}

			}
		});
		btnCreateCust.setBounds(20, 296, 150, 30);
		frame.getContentPane().add(btnCreateCust);
		
		JLabel LabelUpdateAddress = new JLabel("Address");
		LabelUpdateAddress.setFont(new Font("Tahoma", Font.PLAIN, 10));
		LabelUpdateAddress.setBounds(844, 309, 112, 30);
		frame.getContentPane().add(LabelUpdateAddress);
		
		JLabel LabelUpdateDeliveryArea = new JLabel("Delivery Area:");
		LabelUpdateDeliveryArea.setFont(new Font("Tahoma", Font.PLAIN, 10));
		LabelUpdateDeliveryArea.setBounds(844, 338, 112, 30);
		frame.getContentPane().add(LabelUpdateDeliveryArea);
		
		JLabel LabelUpdatePhone = new JLabel("Phone:");
		LabelUpdatePhone.setFont(new Font("Tahoma", Font.PLAIN, 10));
		LabelUpdatePhone.setBounds(844, 364, 112, 30);
		frame.getContentPane().add(LabelUpdatePhone);
		
		updateIDTxt = new JTextField();
		updateIDTxt.setColumns(10);
		updateIDTxt.setBounds(966, 229, 120, 19);
		frame.getContentPane().add(updateIDTxt);
		
		updateFnameTxt = new JTextField();
		updateFnameTxt.setColumns(10);
		updateFnameTxt.setBounds(966, 262, 120, 19);
		frame.getContentPane().add(updateFnameTxt);
		
		updateLnameTxt = new JTextField();
		updateLnameTxt.setColumns(10);
		updateLnameTxt.setBounds(966, 288, 120, 19);
		frame.getContentPane().add(updateLnameTxt);
		
		updateAddressTxt = new JTextField();
		updateAddressTxt.setColumns(10);
		updateAddressTxt.setBounds(966, 315, 120, 19);
		frame.getContentPane().add(updateAddressTxt);
		
		updateDeliveryTxt = new JTextField();
		updateDeliveryTxt.setColumns(10);
		updateDeliveryTxt.setBounds(966, 344, 120, 19);
		frame.getContentPane().add(updateDeliveryTxt);
		
		updatePhoneTxt = new JTextField();
		updatePhoneTxt.setColumns(10);
		updatePhoneTxt.setBounds(966, 370, 120, 19);
		frame.getContentPane().add(updatePhoneTxt);
		
		JButton btnRefreshCustomers = new JButton("Refresh Customers");
		btnRefreshCustomers.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				customerTable();
			}
		});
		btnRefreshCustomers.setBounds(468, 399, 160, 22);
		frame.getContentPane().add(btnRefreshCustomers);
		
		JButton btnHome = new JButton("Return Main Menu");
		btnHome.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiMain frameMain = new GuiMain();
				frameMain.frameOn();
				frameOff();
			}
		});
		btnHome.setBounds(20, 11, 150, 30);
		frame.getContentPane().add(btnHome);
		
		

}
	
	
	public void customerTable() {
		tableModel.setRowCount(0);
		try {
			MySQL sqlObject = new MySQL();
			ResultSet rs = sqlObject.printAllCustomers();
			while (rs.next()) {
				Object rowData[] = new Object[6];
				rowData[0] = rs.getString(1);
				rowData[1] = rs.getString(2);
				rowData[2] = rs.getString(3);
				rowData[3] = rs.getString(4);
				rowData[4] = rs.getString(5);
				rowData[5] = rs.getString(6);
				tableModel.addRow(rowData);
			}
		} catch (SQLException e1) {
			System.err.println("Error SQLException\n" + e1.toString());
		}
	}
	
	public void frameOn() {
		frame.setVisible(true);
	}

	public void frameOff() {
		frame.setVisible(false);
	}
}
