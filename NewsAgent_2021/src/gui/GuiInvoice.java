package gui;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import run.MySQL;

public class GuiInvoice {
	private JFrame frame;
	private JTable table;
	private DefaultTableModel tableModel;
	private JTextField deleteInvoiceTxt;
	private JTextField readMonthlyTxt;
	private JTextField readDateTxt;

	public GuiInvoice() {

		frame = new JFrame();
		frame.setBounds(100, 100, 1110, 670);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblInvoice = new JLabel("Invoice Menu");
		lblInvoice.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblInvoice.setBounds(441, 11, 211, 40);
		frame.getContentPane().add(lblInvoice);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(284, 130, 525, 273);
		frame.getContentPane().add(scrollPane);

		tableModel = new DefaultTableModel(
				new String[] { "Invoice ID", "Date", "Cust Name", "Cust Surname", "Total Bill" }, 0);
		table = new JTable(tableModel);
		scrollPane.setViewportView(table);

		JButton btnDeleteCInv = new JButton("Delete Invoice");
		btnDeleteCInv.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					MySQL sqlObject = new MySQL();
					int id = Integer.parseInt(deleteInvoiceTxt.getText());
					boolean deleteInvoice = sqlObject.deleteInvoice(id);
					if (deleteInvoice == true) {
						System.out.println("Success: Invoice Delete Attempted\n");
					} else {
						System.out.println("Error: SQLException wrong Invoice delete sql command");
					}
				} catch (NumberFormatException eee) {
					System.out.println("Error: NumberFormatException\n");
				} catch (SQLException e1) {
					System.out.println("Error: SQLException");
				}
			}
		});
		btnDeleteCInv.setBounds(120, 310, 150, 30);
		frame.getContentPane().add(btnDeleteCInv);

		deleteInvoiceTxt = new JTextField();
		deleteInvoiceTxt.setColumns(10);
		deleteInvoiceTxt.setBounds(120, 270, 150, 30);
		frame.getContentPane().add(deleteInvoiceTxt);

		JLabel LabelDeleteInv = new JLabel(">Delete Invoice<");
		LabelDeleteInv.setFont(new Font("Tahoma", Font.BOLD, 18));
		LabelDeleteInv.setBounds(20, 230, 250, 30);
		frame.getContentPane().add(LabelDeleteInv);

		JLabel lblNewLabel_1_3 = new JLabel("Enter Invoice ID:");
		lblNewLabel_1_3.setBounds(20, 270, 100, 30);
		frame.getContentPane().add(lblNewLabel_1_3);

		JButton btnHome = new JButton("Return Main Menu");
		btnHome.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiMain frameMain = new GuiMain();
				frameMain.frameOn();
				frameOff();
			}
		});
		btnHome.setBounds(20, 11, 150, 30);
		frame.getContentPane().add(btnHome);

		JLabel LabelReadInvoice = new JLabel(">Read Invoice<");
		LabelReadInvoice.setFont(new Font("Tahoma", Font.BOLD, 18));
		LabelReadInvoice.setBounds(20, 380, 250, 30);
		frame.getContentPane().add(LabelReadInvoice);

		readMonthlyTxt = new JTextField();
		readMonthlyTxt.setColumns(10);
		readMonthlyTxt.setBounds(120, 120, 150, 30);
		frame.getContentPane().add(readMonthlyTxt);

		readDateTxt = new JTextField();
		readDateTxt.setColumns(10);
		readDateTxt.setBounds(120, 420, 150, 30);
		frame.getContentPane().add(readDateTxt);

		JButton btnCreateMonthly = new JButton("Create Invoice");
		btnCreateMonthly.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					MySQL sqlObject = new MySQL();
					String date = readMonthlyTxt.getText();
					boolean insertResult = sqlObject.invoiceMonthly(date);
					if (insertResult == true) {
						readMonthlyTxt.setText("");
						System.out.println("Success: Monthly invoice insert Attempted\n");
					} else {
						System.out.println(
								"Error: SQLException wrong invoice insert sql command or Customer id doesnt exist");
					}

					sqlObject.closeSQL();
				} catch (SQLException e1) {
					System.out.println("Error: SQLException");
				}

			}
		});
		btnCreateMonthly.setBounds(120, 160, 150, 30);
		frame.getContentPane().add(btnCreateMonthly);

		JButton btnReadDate = new JButton("Read Date");
		btnReadDate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				invoiceTableDate(readDateTxt.getText());
			}
		});
		btnReadDate.setBounds(120, 461, 150, 30);
		frame.getContentPane().add(btnReadDate);

		JLabel lblNewLabel = new JLabel("Enter YYYY-MM:");
		lblNewLabel.setBounds(20, 120, 100, 30);
		frame.getContentPane().add(lblNewLabel);

		JLabel creatmothlyLable = new JLabel(">Create Monthly Invoice<");
		creatmothlyLable.setFont(new Font("Tahoma", Font.BOLD, 18));
		creatmothlyLable.setBounds(20, 80, 250, 30);
		frame.getContentPane().add(creatmothlyLable);
		
		JLabel lblNewLabel_1_2_2_1 = new JLabel(">Read Delivery Driver<");
		lblNewLabel_1_2_2_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_2_2_1.setBounds(436, 80, 222, 30);
		frame.getContentPane().add(lblNewLabel_1_2_2_1);
		
		JLabel lblNewLabel_1 = new JLabel("Enter YYYY-MM:");
		lblNewLabel_1.setBounds(20, 420, 100, 30);
		frame.getContentPane().add(lblNewLabel_1);

	}

	public void invoiceTableDate(String date) {
		tableModel.setRowCount(0);
		try {
			MySQL sqlObject = new MySQL();
			ResultSet rs = sqlObject.invoiceReadDate(date);
			while (rs.next()) {
				Object rowData[] = new Object[5];
				rowData[0] = rs.getString(1);
				rowData[1] = rs.getString(5);
				rowData[2] = rs.getString(10);
				rowData[3] = rs.getString(11);
				rowData[4] = rs.getString(28);

				tableModel.addRow(rowData);
			}
		} catch (SQLException e1) {
			System.err.println("Error SQLException\n" + e1.toString());
		}
	}

	public void frameOn() {
		frame.setVisible(true);
	}

	public void frameOff() {
		frame.setVisible(false);
	}
}
