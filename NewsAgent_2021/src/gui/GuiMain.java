package gui;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class GuiMain {

	private JFrame frame;

	public GuiMain() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1110, 670);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnNewButton = new JButton("Customer");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Gui_Customer cust = new Gui_Customer();
				cust.frameOn();
				frameOff();
			}
		});
		btnNewButton.setBounds(447, 140, 200, 30);
		frame.getContentPane().add(btnNewButton);

		JButton btnPublicationManagment = new JButton("Publication Managment");
		btnPublicationManagment.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiPublication  abc = new GuiPublication();
				 frameOff();
				 abc.frameOn();
			}
		});
		btnPublicationManagment.setBounds(447, 190, 200, 30);
		frame.getContentPane().add(btnPublicationManagment);

		JButton btnOrders = new JButton("Orders");
		btnOrders.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiOrder abc = new GuiOrder();
				 frameOff();
				 abc.frameOn();
			}
		});
		btnOrders.setBounds(447, 290, 200, 30);
		frame.getContentPane().add(btnOrders);

		JButton btnDeliveryArea = new JButton("Delivery Area");
		btnDeliveryArea.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frameOff();
				GuiArea abc = new GuiArea();
				abc.frameOn();
			}
		});
		btnDeliveryArea.setBounds(447, 390, 200, 30);
		frame.getContentPane().add(btnDeliveryArea);

		JButton btnDeliveryDriver = new JButton("Delivery Driver");
		btnDeliveryDriver.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiDriverNew driver = new GuiDriverNew();
				driver.frameOn();
				frameOff();
			}
		});
		btnDeliveryDriver.setBounds(447, 240, 200, 30);
		frame.getContentPane().add(btnDeliveryDriver);

		JButton btnInvoice = new JButton("Invoice");
		btnInvoice.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiInvoice invoice = new GuiInvoice();
				invoice.frameOn();
				frameOff();
			}
		});
		btnInvoice.setBounds(447, 440, 200, 30);
		frame.getContentPane().add(btnInvoice);

		JLabel lblNewLabel = new JLabel("MENU");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblNewLabel.setBounds(497, 57, 100, 30);
		frame.getContentPane().add(lblNewLabel);

		JButton btnDeliveryDriverLog = new JButton("Delivery Driver Log In");
		btnDeliveryDriverLog.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiLogIn logIn = new GuiLogIn();
				logIn.frameOn();
				frameOff();
			}
		});
		btnDeliveryDriverLog.setBounds(447, 540, 200, 30);
		frame.getContentPane().add(btnDeliveryDriverLog);

		JLabel lblNewLabel_1 = new JLabel("Delivery Driver");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_1.setBounds(501, 490, 92, 30);
		frame.getContentPane().add(lblNewLabel_1);

		JButton btnOrderToPublication = new JButton("Order To Publication");
		btnOrderToPublication.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiOrdToPub ordPub = new GuiOrdToPub();
				ordPub.frameOn();
				frameOff();
			}
		});
		btnOrderToPublication.setBounds(447, 340, 200, 30);
		frame.getContentPane().add(btnOrderToPublication);
		frame.setVisible(true);
	}

	public void frameOn() {
		frame.setVisible(true);
	}

	public void frameOff() {
		frame.setVisible(false);
	}
}
