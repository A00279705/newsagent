package gui;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import classes.Driver;
import exceptionHandler.DriversExceptionHandler;
import run.MySQL;

public class GuiDriverNew {

	private JFrame frame;
	private JTextField name1;
	private JTable table;
	private JTextField password1;
	private JTextField id2;
	private JTextField name3;
	private JTextField password3;
	private JTextField id3;
	private DefaultTableModel tableModel;
	private JLabel message1;
	private JLabel message2;
	private JLabel message3;

	public GuiDriverNew() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1110, 670);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("Enter Driver Name:");
		lblNewLabel_1.setBounds(20, 130, 120, 30);
		frame.getContentPane().add(lblNewLabel_1);

		name1 = new JTextField();
		name1.setColumns(10);
		name1.setBounds(140, 130, 150, 30);
		frame.getContentPane().add(name1);

		JButton btnCreateDriver = new JButton("Create Driver");
		btnCreateDriver.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					message1.setText("Warning");
					String name = name1.getText();
					String password = password1.getText();
					MySQL sqlObject = new MySQL();
					Driver driverObj = new Driver(name, password);
					boolean insertResult = sqlObject.insertdelivery_driver(driverObj);
					if (insertResult == true) {
						message1.setText("Driver Created");
						name1.setText("");
						password1.setText("");
						System.out.println("Success: Driver insert Attempted\n");
					} else {
						message1.setText("Order Failed");
						System.out.println("Error: SQLException wrong Driver insert sql command");
					}
					driverTable();
					sqlObject.closeSQL();
				} catch (DriversExceptionHandler e1) {
					message1.setText("DriversExceptionHandler");
					System.out.println("Error: DriversExceptionHandler\n");
				} catch (SQLException e1) {
					message1.setText("SQLException");
					System.out.println("Error: SQLException");
				}
			}
		});
		btnCreateDriver.setBounds(140, 230, 150, 30);
		frame.getContentPane().add(btnCreateDriver);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(326, 130, 440, 225);
		frame.getContentPane().add(scrollPane);

		tableModel = new DefaultTableModel(new String[] { "Driver ID", "Name", "Password" }, 0);
		table = new JTable(tableModel);
		scrollPane.setViewportView(table);

		JLabel lblDeliveryDriver_1 = new JLabel("Delivery Driver");
		lblDeliveryDriver_1.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblDeliveryDriver_1.setBounds(427, 11, 236, 40);
		frame.getContentPane().add(lblDeliveryDriver_1);

		JLabel lblNewLabel_1_1 = new JLabel("Enter Driver Password:");
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblNewLabel_1_1.setBounds(20, 180, 120, 30);
		frame.getContentPane().add(lblNewLabel_1_1);

		password1 = new JTextField();
		password1.setColumns(10);
		password1.setBounds(140, 180, 150, 30);
		frame.getContentPane().add(password1);

		JLabel lblNewLabel_1_2 = new JLabel(">Create Delivery Driver<");
		lblNewLabel_1_2.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_2.setBounds(20, 80, 270, 30);
		frame.getContentPane().add(lblNewLabel_1_2);

		JButton btnCreateDriver_1 = new JButton("Delete Driver");
		btnCreateDriver_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					message2.setText("Warning");
					MySQL sqlObject = new MySQL();
					int id = Integer.parseInt(id2.getText());
					Driver driverObj = new Driver();
					driverObj.validateID_driver(id);
					boolean updateDriver = sqlObject.deleteDriver(driverObj);
					if (updateDriver == true) {
						id2.setText("");
						message2.setText("Driver Deleted");
						System.out.println("Success: Driver Delete Attempted\n");
					} else {
						System.out.println("Error: SQLException wrong Driver delete sql command");
					}
					sqlObject.closeSQL();
					driverTable();
				} catch (NumberFormatException eee) {
					message2.setText("NumberFormatException");
					System.out.println("Error: NumberFormatException\n");
				} catch (DriversExceptionHandler ee) {
					message2.setText("DriversExceptionHandler");
					System.out.println("Error: DriverExceptionHandler\n");
				} catch (SQLException e1) {
					message2.setText("SQLException");
					System.out.println("Error: SQLException");
				}
			}
		});
		btnCreateDriver_1.setBounds(140, 400, 150, 30);
		frame.getContentPane().add(btnCreateDriver_1);

		id2 = new JTextField();
		id2.setColumns(10);
		id2.setBounds(140, 350, 150, 30);
		frame.getContentPane().add(id2);

		JLabel lblNewLabel_1_2_1 = new JLabel(">Delete Delivery Driver<");
		lblNewLabel_1_2_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_2_1.setBounds(20, 300, 270, 30);
		frame.getContentPane().add(lblNewLabel_1_2_1);

		JLabel lblNewLabel_1_3 = new JLabel("Enter Driver ID:");
		lblNewLabel_1_3.setBounds(20, 350, 120, 30);
		frame.getContentPane().add(lblNewLabel_1_3);

		JLabel lblNewLabel_1_2_2 = new JLabel(">Update Delivery Driver<");
		lblNewLabel_1_2_2.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_2_2.setBounds(780, 80, 270, 30);
		frame.getContentPane().add(lblNewLabel_1_2_2);

		name3 = new JTextField();
		name3.setColumns(10);
		name3.setBounds(900, 180, 150, 30);
		frame.getContentPane().add(name3);

		JLabel lblNewLabel_1_4 = new JLabel("Enter Driver Name:");
		lblNewLabel_1_4.setBounds(780, 180, 120, 30);
		frame.getContentPane().add(lblNewLabel_1_4);

		JLabel lblNewLabel_1_1_1 = new JLabel("Enter Driver Password:");
		lblNewLabel_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblNewLabel_1_1_1.setBounds(780, 230, 120, 30);
		frame.getContentPane().add(lblNewLabel_1_1_1);

		password3 = new JTextField();
		password3.setColumns(10);
		password3.setBounds(900, 230, 150, 30);
		frame.getContentPane().add(password3);

		JButton btnUpdateDriver = new JButton("Update Driver");
		btnUpdateDriver.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					message3.setText("Warning");
					MySQL sqlObject = new MySQL();
					int id = Integer.parseInt(id3.getText());
					String name = name3.getText();
					String password = password3.getText();
					Driver updatedDriver = new Driver();

					updatedDriver.ValidateUpdateName_driver(name);
					updatedDriver.ValidateUpdatePassword_driver(password);
					updatedDriver.validateID_driver(id);
					boolean insertResult = sqlObject.updateDriver(updatedDriver);
					if (insertResult == true) {
						name3.setText("");
						id3.setText("");
						password3.setText("");
						message3.setText("Driver Updated");
						System.out.println("Success: Driver Update Attempted\n");
					} else {
						message3.setText("Driver Failed");
						System.out.println("Error: SQLException wrong Driver update sql command\n");
					}
					sqlObject.closeSQL();
					driverTable();
				} catch (NumberFormatException e1) {
					message3.setText("NumberFormatException");
					System.out.println("Error: NumberFormatException\n");
				} catch (DriversExceptionHandler e1) {
					message3.setText("DriversExceptionHandler");
					System.out.println("Error: DriverExceptionHandler\n");
				} catch (SQLException e1) {
					message3.setText("SQLException");
					System.out.println("Error: SQLException");
				}
			}
		});
		btnUpdateDriver.setBounds(901, 280, 150, 30);
		frame.getContentPane().add(btnUpdateDriver);

		JLabel lblNewLabel_1_4_1 = new JLabel("Enter Driver ID:");
		lblNewLabel_1_4_1.setBounds(780, 130, 120, 30);
		frame.getContentPane().add(lblNewLabel_1_4_1);

		id3 = new JTextField();
		id3.setColumns(10);
		id3.setBounds(900, 130, 150, 30);
		frame.getContentPane().add(id3);

		JLabel lblNewLabel_1_2_2_1 = new JLabel(">Read Delivery Driver<");
		lblNewLabel_1_2_2_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_2_2_1.setBounds(436, 80, 222, 30);
		frame.getContentPane().add(lblNewLabel_1_2_2_1);

		JButton btnRefreshDrivers = new JButton("Refresh Drivers");
		btnRefreshDrivers.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				driverTable();
			}
		});
		btnRefreshDrivers.setBounds(472, 366, 150, 30);
		frame.getContentPane().add(btnRefreshDrivers);

		message1 = new JLabel("Warning");
		message1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		message1.setBackground(new Color(220, 20, 60));
		message1.setBounds(20, 230, 120, 30);
		frame.getContentPane().add(message1);

		JButton btnNewButton = new JButton("Return Main Menu");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiMain frameMain = new GuiMain();
				frameMain.frameOn();
				frameOff();
			}
		});
		btnNewButton.setBounds(20, 11, 150, 30);
		frame.getContentPane().add(btnNewButton);

		message2 = new JLabel("Warning");
		message2.setFont(new Font("Tahoma", Font.PLAIN, 10));
		message2.setBackground(new Color(220, 20, 60));
		message2.setBounds(20, 400, 120, 30);
		frame.getContentPane().add(message2);

		message3 = new JLabel("Warning");
		message3.setFont(new Font("Tahoma", Font.PLAIN, 10));
		message3.setBackground(new Color(220, 20, 60));
		message3.setBounds(780, 280, 120, 30);
		frame.getContentPane().add(message3);
		driverTable();
		frame.setVisible(true);
	}

	public void driverTable() {
		tableModel.setRowCount(0);
		try {
			MySQL sqlObject = new MySQL();
			ResultSet rs = sqlObject.printAlldelivery_drivers();
			while (rs.next()) {
				Object rowData[] = new Object[3];
				rowData[0] = rs.getString(1);
				rowData[1] = rs.getString(2);
				rowData[2] = rs.getString(3);
				tableModel.addRow(rowData);
			}
			sqlObject.closeSQL();
		} catch (SQLException e1) {
			System.err.println("Error SQLException\n" + e1.toString());
		}
	}

	public void frameOn() {
		frame.setVisible(true);
	}

	public void frameOff() {
		frame.setVisible(false);
	}
}
