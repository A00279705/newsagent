package gui;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import classes.Publication;
import exceptionHandler.PublicationExceptionHandler;
import run.MySQL;

public class GuiPublication {
	private JFrame frame;
	private JTable table;
	private JTextField deletePubTxt;
	private DefaultTableModel tableModel;
	private JTextField booknameTxt;
	private JTextField bookpriceTxt;
	private JTextField bookamtTxt;
	private JTextField updateIDTxt;
	private JTextField updatebooknameTxt;
	private JTextField updatebookpriceTxt;
	private JTextField updatebookamtTxt;
	private JLabel message1;
	private JLabel message2;
	private JLabel message3;

	public GuiPublication() {

		frame = new JFrame();
		frame.setBounds(100, 100, 1110, 670);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblPublicationMenu = new JLabel("Publication Management");
		lblPublicationMenu.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblPublicationMenu.setBounds(371, 10, 394, 40);
		frame.getContentPane().add(lblPublicationMenu);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(284, 116, 525, 273);
		frame.getContentPane().add(scrollPane);

		tableModel = new DefaultTableModel(new String[] { "ID", "Book Name", "Book Price", "Book Amount" }, 0);
		table = new JTable(tableModel);
		scrollPane.setViewportView(table);

		JLabel LabelBookName = new JLabel("Book Name:");
		LabelBookName.setFont(new Font("Tahoma", Font.PLAIN, 11));
		LabelBookName.setBounds(10, 120, 80, 30);
		frame.getContentPane().add(LabelBookName);

		JLabel LabelCreateCust = new JLabel(">Create Publication<");
		LabelCreateCust.setFont(new Font("Tahoma", Font.BOLD, 18));
		LabelCreateCust.setBounds(20, 80, 270, 30);
		frame.getContentPane().add(LabelCreateCust);

		JButton btnDeletePub = new JButton("Delete Publication");
		btnDeletePub.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					MySQL sqlObject = new MySQL();
					int id = Integer.parseInt(deletePubTxt.getText());
					Publication pubObj = new Publication();
					pubObj.validateId_publication(id);
					boolean deletePublications = sqlObject.deletePublications(id);
					if (deletePublications == true) {
						message2.setText("Publication Deleted");
						System.out.println("Success: Publication Delete Attempted\n");
					} else {
						message2.setText("Publication Not Deleted");
						System.out.println("Error: SQLException wrong Publication delete sql command");
					}
					publicationTable();
				} catch (NumberFormatException eee) {
					message2.setText("NumberFormatException");
					System.out.println("Error: NumberFormatException\n");
				} catch (PublicationExceptionHandler ee) {
					message2.setText("PublicationExceptionHandler");
					System.out.println("Error: PublicationExceptionHandler\n");
				} catch (SQLException e1) {
					message2.setText("SQLException");
					System.out.println("Error: SQLException");
				}
			}

		});
		btnDeletePub.setBounds(47, 456, 150, 30);
		frame.getContentPane().add(btnDeletePub);

		deletePubTxt = new JTextField();
		deletePubTxt.setColumns(10);
		deletePubTxt.setBounds(130, 415, 150, 30);
		frame.getContentPane().add(deletePubTxt);

		JLabel LabelDeleteCust = new JLabel(">Delete Publication<");
		LabelDeleteCust.setFont(new Font("Tahoma", Font.BOLD, 18));
		LabelDeleteCust.setBounds(10, 375, 270, 30);
		frame.getContentPane().add(LabelDeleteCust);

		JLabel lblNewLabel_1_3 = new JLabel("Enter Publication ID:");
		lblNewLabel_1_3.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_3.setBounds(10, 420, 110, 19);
		frame.getContentPane().add(lblNewLabel_1_3);

		JLabel LabelUpdatePublication = new JLabel(">Update Publication<");
		LabelUpdatePublication.setFont(new Font("Tahoma", Font.BOLD, 18));
		LabelUpdatePublication.setBounds(879, 80, 207, 30);
		frame.getContentPane().add(LabelUpdatePublication);

		JLabel labelUpdateFName = new JLabel("Book Name:");
		labelUpdateFName.setFont(new Font("Tahoma", Font.PLAIN, 11));
		labelUpdateFName.setBounds(831, 210, 112, 30);
		frame.getContentPane().add(labelUpdateFName);

		JLabel labelUpdateLName = new JLabel("Book Price:");
		labelUpdateLName.setFont(new Font("Tahoma", Font.PLAIN, 11));
		labelUpdateLName.setBounds(831, 269, 112, 30);
		frame.getContentPane().add(labelUpdateLName);

		JButton btnUpdatePub = new JButton("Update Publication");
		btnUpdatePub.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					MySQL sqlObject = new MySQL();
					int id = Integer.parseInt(updateIDTxt.getText());
					String bookname = updatebooknameTxt.getText();
					int bookprice = Integer.parseInt(updatebookpriceTxt.getText());
					int bookamt = Integer.parseInt(updatebookamtTxt.getText());
					Publication updatePub = new Publication();
					updatePub.validateBook_name(bookname);
					updatePub.validateBook_price(bookprice);
					updatePub.validateBook_amount(bookamt);
					updatePub.validateId_publication(id);

					boolean updatePublication = sqlObject.updatePublication(updatePub);
					if (updatePublication == true) {
						message3.setText("Publication Updated");
						System.out.println("Success: Publication Update Attempted\n");
					} else {
						message3.setText("Publication Not Updated");
						System.out.println("Error: SQLException wrong Publication update sql command\n");
					}
					publicationTable();
				} catch (NumberFormatException e1) {
					message3.setText("NumberFormatException");
					System.out.println("Error: NumberFormatException\n");
				} catch (PublicationExceptionHandler e1) {
					message3.setText("PublicationExceptionHandler");
					System.out.println("Error: PublicationExceptionHandler\n");
				} catch (SQLException e1) {
					message3.setText("SQLException");
					System.out.println("Error: SQLException");
				}
			}

		});
		btnUpdatePub.setBounds(886, 395, 150, 30);
		frame.getContentPane().add(btnUpdatePub);

		JLabel LabelUpdateID = new JLabel("Enter Publication ID:");
		LabelUpdateID.setFont(new Font("Tahoma", Font.PLAIN, 11));
		LabelUpdateID.setBounds(831, 155, 112, 30);
		frame.getContentPane().add(LabelUpdateID);

		JLabel LabelCustomerRead = new JLabel(">Read Publication<");
		LabelCustomerRead.setFont(new Font("Tahoma", Font.BOLD, 18));
		LabelCustomerRead.setBounds(456, 80, 189, 30);
		frame.getContentPane().add(LabelCustomerRead);

		JLabel LabelBookPrice = new JLabel("Book Price:");
		LabelBookPrice.setFont(new Font("Tahoma", Font.PLAIN, 11));
		LabelBookPrice.setBounds(10, 172, 80, 30);
		frame.getContentPane().add(LabelBookPrice);

		JLabel BookAmount = new JLabel("Book Amount:");
		BookAmount.setFont(new Font("Tahoma", Font.PLAIN, 11));
		BookAmount.setBounds(10, 223, 80, 30);
		frame.getContentPane().add(BookAmount);

		booknameTxt = new JTextField();
		booknameTxt.setBounds(100, 121, 150, 30);
		frame.getContentPane().add(booknameTxt);
		booknameTxt.setColumns(10);

		bookpriceTxt = new JTextField();
		bookpriceTxt.setColumns(10);
		bookpriceTxt.setBounds(100, 173, 150, 30);
		frame.getContentPane().add(bookpriceTxt);

		bookamtTxt = new JTextField();
		bookamtTxt.setColumns(10);
		bookamtTxt.setBounds(100, 224, 150, 30);
		frame.getContentPane().add(bookamtTxt);

		JButton btnCreatePub = new JButton("Create Publication");
		btnCreatePub.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String bookname = booknameTxt.getText();
					int bookprice = Integer.parseInt(bookpriceTxt.getText());
					int bookamt = Integer.parseInt(bookamtTxt.getText());
					MySQL sqlObject = new MySQL();
					Publication pubObj = new Publication();
					pubObj.validateBook_name(bookname);
					pubObj.validateBook_price(bookprice);
					pubObj.validateBook_amount(bookamt);
					boolean insertResult = sqlObject.insertPublication(pubObj);
					if (insertResult == true) {
						message1.setText("Publication Created");
						booknameTxt.setText("");
						bookpriceTxt.setText("");
						bookamtTxt.setText("");
						System.out.println("Success: Publication insert Attempted\n");
					} else {
						message1.setText("Publication Failed");
						System.out.println(
								"Error: SQLException wrong Publication insert sql command or Publication id doesnt exist");
					}
					publicationTable();
					sqlObject.closeSQL();
				} catch (SQLException e1) {
					message1.setText("SQLException");
					System.out.println("Error: SQLException");
				} catch (PublicationExceptionHandler e1) {
					message1.setText("PublicationExceptionHandler");
					System.out.println("Error: PublicationExceptionHandler");
				} catch (NumberFormatException e1) {
					message1.setText("NumberFormatException");
					System.out.println("Error: NumberFormatException\n");
				}
			}
		});
		btnCreatePub.setBounds(47, 282, 150, 30);
		frame.getContentPane().add(btnCreatePub);

		JLabel LabelUpdateAddress = new JLabel("Book Amount:");
		LabelUpdateAddress.setFont(new Font("Tahoma", Font.PLAIN, 11));
		LabelUpdateAddress.setBounds(831, 322, 112, 30);
		frame.getContentPane().add(LabelUpdateAddress);

		updateIDTxt = new JTextField();
		updateIDTxt.setColumns(10);
		updateIDTxt.setBounds(936, 156, 150, 30);
		frame.getContentPane().add(updateIDTxt);

		updatebooknameTxt = new JTextField();
		updatebooknameTxt.setColumns(10);
		updatebooknameTxt.setBounds(936, 211, 150, 30);
		frame.getContentPane().add(updatebooknameTxt);

		updatebookpriceTxt = new JTextField();
		updatebookpriceTxt.setColumns(10);
		updatebookpriceTxt.setBounds(936, 270, 150, 30);
		frame.getContentPane().add(updatebookpriceTxt);

		updatebookamtTxt = new JTextField();
		updatebookamtTxt.setColumns(10);
		updatebookamtTxt.setBounds(936, 323, 150, 30);
		frame.getContentPane().add(updatebookamtTxt);

		JButton btnRefreshPub = new JButton("Refresh Publication");
		btnRefreshPub.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				publicationTable();
			}
		});
		btnRefreshPub.setBounds(468, 399, 150, 30);
		frame.getContentPane().add(btnRefreshPub);

		JButton btnHome = new JButton("Return Main Menu");
		btnHome.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiMain frameMain = new GuiMain();
				frameMain.frameOn();
				frameOff();
			}
		});
		btnHome.setBounds(20, 11, 150, 30);
		frame.getContentPane().add(btnHome);

		message1 = new JLabel("Warning");
		message1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		message1.setBounds(60, 337, 120, 30);
		frame.getContentPane().add(message1);

		message2 = new JLabel("Warning");
		message2.setFont(new Font("Tahoma", Font.PLAIN, 10));
		message2.setBounds(60, 520, 120, 30);
		frame.getContentPane().add(message2);

		message3 = new JLabel("Warning");
		message3.setFont(new Font("Tahoma", Font.PLAIN, 10));
		message3.setBounds(896, 466, 120, 30);
		frame.getContentPane().add(message3);

		JButton btnNewButton = new JButton("Generate Report");
		btnNewButton.setBounds(468, 520, 150, 30);
		frame.getContentPane().add(btnNewButton);

		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					System.out.print("");
					MySQL.ExportStats();
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (SQLException e1) {
					e1.printStackTrace();

				}

			}
		});

	}

	public void publicationTable() {
		tableModel.setRowCount(0);
		try {
			MySQL sqlObject = new MySQL();
			ResultSet rs = sqlObject.printAllPublications();
			while (rs.next()) {
				Object rowData[] = new Object[4];
				rowData[0] = rs.getString(1);
				rowData[1] = rs.getString(2);
				rowData[2] = rs.getString(3);
				rowData[3] = rs.getString(4);
				tableModel.addRow(rowData);
			}
		} catch (SQLException e1) {
			System.err.println("Error SQLException\n" + e1.toString());
		}
	}

	public void frameOn() {
		frame.setVisible(true);
	}

	public void frameOff() {
		frame.setVisible(false);
	}
}
