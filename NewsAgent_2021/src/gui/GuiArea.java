package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import classes.Area;
import exceptionHandler.AreaExceptionHandler;
import run.MySQL;

public class GuiArea {

	private JFrame frame;
	private JTextField txtCustomer_ID;
	private JTable table;
	private JTextField txtDriver_ID;
	private JTextField txtDelete;
	private DefaultTableModel tableModel;

	public GuiArea() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1110, 670);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("Customer ID");
		lblNewLabel_1.setBounds(57, 130, 120, 30);
		frame.getContentPane().add(lblNewLabel_1);

		txtCustomer_ID = new JTextField();
		txtCustomer_ID.setColumns(10);
		txtCustomer_ID.setBounds(140, 130, 96, 30);
		frame.getContentPane().add(txtCustomer_ID);

		JButton btnCreateArea = new JButton("Create Area");
		btnCreateArea.setBackground(Color.GREEN);
		btnCreateArea.setFont(new Font("Tahoma", Font.BOLD, 11));

		btnCreateArea.addActionListener(new ActionListener() {
			@Override

			public void actionPerformed(ActionEvent e) {
				try {

					// String name = customer_ID.getText();
					int id_customer = Integer.parseInt(txtCustomer_ID.getText());
					int id_driver = Integer.parseInt(txtDriver_ID.getText());

					MySQL sqlObject = new MySQL();
					Area areaObj = new Area(id_customer, id_driver);

					boolean insertResult = sqlObject.insertArea(areaObj);
					if (insertResult == true) {
						txtCustomer_ID.setText("");
						txtDriver_ID.setText("");

						System.out.println("PASS: Area details Inserted\n");
					} else
						System.out.println("ERROR: Area info Not Inserted\n");

					// sqlObject.closeSQL();

				} catch (NumberFormatException E) {
					System.out.println("The Input has to be a number: NumberFormatException\n");
				} catch (AreaExceptionHandler e1) {
					System.out.println("ERROR: Area Exception Handler\n");
				} catch (NullPointerException e1) {
					System.out.println("ERROR: NullPointerException Server Could Be not Running");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		/*
		 * //message1.setText(""); String name = customer_ID.getText(); String password
		 * = driver_ID.getText();
		 * 
		 * MySQL sqlObject = new MySQL(); Driver driverObj = new Driver(name, password);
		 * 
		 * boolean insertResult = sqlObject.insertdelivery_driver(driverObj); if
		 * (insertResult == true) { customer_ID.setText(""); driver_ID.setText("");
		 * System.out.println("Success: Area insert Attempted\n"); } else {
		 * System.out.println(
		 * "Error: SQLException wrong Order insert sql command or Customer id doesnt exist"
		 * ); } //driverTable(); sqlObject.closeSQL(); } catch (DriversExceptionHandler
		 * e1) {
		 * 
		 * //message1.setText("Incorect Values Entered");
		 * System.out.println("Error: OrderExceptionHandler\n"); } catch (SQLException
		 * e1) { System.out.println("Error: SQLException"); }
		 * 
		 * } });
		 */

		btnCreateArea.setBounds(57, 221, 179, 30);
		frame.getContentPane().add(btnCreateArea);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(325, 150, 444, 277);
		frame.getContentPane().add(scrollPane);

		tableModel = new DefaultTableModel(new String[] { "Area ID", " Customer ID", " Del Area" }, 0);
		table = new JTable(tableModel);
		scrollPane.setViewportView(table);

		JLabel lblDeliveryArea_1 = new JLabel("Delivery Area");
		lblDeliveryArea_1.setForeground(Color.RED);
		lblDeliveryArea_1.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblDeliveryArea_1.setBounds(439, 11, 216, 40);
		frame.getContentPane().add(lblDeliveryArea_1);

		JLabel lblNewLabel_1_1 = new JLabel("Delivery Area");
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblNewLabel_1_1.setBounds(57, 180, 120, 30);
		frame.getContentPane().add(lblNewLabel_1_1);

		txtDriver_ID = new JTextField();
		txtDriver_ID.setColumns(10);
		txtDriver_ID.setBounds(140, 180, 96, 30);
		frame.getContentPane().add(txtDriver_ID);

		JLabel lblNewLabel_1_2 = new JLabel(">Create Delivery Area<");
		lblNewLabel_1_2.setForeground(Color.BLUE);
		lblNewLabel_1_2.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_2.setBounds(20, 80, 270, 30);
		frame.getContentPane().add(lblNewLabel_1_2);

//*********************************************************************************  Delete Area
		JLabel lblNewLabel_1_2_1 = new JLabel(">Delete Delivery Area<");
		lblNewLabel_1_2_1.setForeground(Color.BLUE);
		lblNewLabel_1_2_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_2_1.setBounds(20, 304, 216, 30);
		frame.getContentPane().add(lblNewLabel_1_2_1);

		JLabel lblNewLabel_1_3 = new JLabel("Enter Area ID:");
		lblNewLabel_1_3.setBounds(40, 350, 120, 30);
		frame.getContentPane().add(lblNewLabel_1_3);

		txtDelete = new JTextField();
		txtDelete.setColumns(10);
		txtDelete.setBounds(140, 350, 96, 30);
		frame.getContentPane().add(txtDelete);

		JButton btnDelete = new JButton("Delete Area");
		btnDelete.setBackground(Color.GREEN);
		btnDelete.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnDelete.setBounds(20, 397, 216, 30);
		frame.getContentPane().add(btnDelete);

		btnDelete.addActionListener(new ActionListener() {
			@Override

			public void actionPerformed(ActionEvent e) {

				try {
					MySQL sqlObject = new MySQL();
					int id_area = Integer.parseInt(txtDelete.getText());

					Area areaObj = new Area();
					areaObj.validateId_area(id_area);
					boolean updateArea = sqlObject.deleteArea(areaObj.getId_area());
					if (updateArea == true) {
						System.out.println("Success: Area Delete Attempted\n");
					} else {
						System.out.println("Error: SQLException wrong Area delete sql command");
					}
				} catch (NumberFormatException E) {
					System.out.println("Error: NumberFormatException\n");
				} catch (AreaExceptionHandler e1) {
					System.out.println("Error: AreaExceptionHandler\n");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		JLabel lblNewLabel_12 = new JLabel("> Generate Del Area Report <");
		lblNewLabel_12.setForeground(Color.BLUE);
		lblNewLabel_12.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_12.setBounds(796, 186, 275, 14);
		frame.getContentPane().add(lblNewLabel_12);

		JLabel lblNewLabel_13 = new JLabel("New GUI Window for ");
		lblNewLabel_13.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_13.setBounds(780, 221, 124, 14);
		frame.getContentPane().add(lblNewLabel_13);

		JButton btnGenDelArea = new JButton("Gen Del Area Report");
		btnGenDelArea.setBackground(Color.GREEN);
		btnGenDelArea.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnGenDelArea.setBounds(914, 217, 157, 23);
		frame.getContentPane().add(btnGenDelArea);

		// JButton btnDeliveryArea = new JButton("Delivery Area");
		btnGenDelArea.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frameOff();
				GuiAreaGen abc = new GuiAreaGen();
				abc.frameOn();
			}
		});

		JLabel lblNewLabel_1_2_2_1 = new JLabel(">Read Delivery Area<");
		lblNewLabel_1_2_2_1.setForeground(Color.BLUE);
		lblNewLabel_1_2_2_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_2_2_1.setBounds(439, 109, 216, 30);
		frame.getContentPane().add(lblNewLabel_1_2_2_1);

		// ************************************************************ Refresh Area //
		// Read Area
		JButton btnRefreshArea = new JButton("Read Delivery Area");
		btnRefreshArea.setBackground(Color.GREEN);
		btnRefreshArea.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnRefreshArea.setBounds(445, 448, 204, 30);
		frame.getContentPane().add(btnRefreshArea);
		
		JButton btnNewButton = new JButton("Return Main Menu");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiMain frameMain = new GuiMain();
				frameMain.frameOn();
				frameOff();
			}
		});
		btnNewButton.setBounds(20, 11, 150, 30);
		frame.getContentPane().add(btnNewButton);
		
		btnRefreshArea.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				areaTable();
			}
		});
		areaTable();
		// *********************************************************** Area Table
	}

	public void areaTable() {
		tableModel.setRowCount(0);
		try {
			MySQL sqlObject = new MySQL();
			ResultSet rs = sqlObject.printArea(); // on MyRun.java case 55

			while (rs.next()) {
				Object rowData[] = new Object[3];
				rowData[0] = rs.getString(1);
				rowData[1] = rs.getString(2);
				rowData[2] = rs.getString(3);
				tableModel.addRow(rowData);

				/*
				 * 
				 * MySQL sqlObject = new MySQL(); ResultSet generate =
				 * sqlObject.generateDeliveryAreaReport(ordObj1); //on MyRun.java case 5555
				 * 
				 * while (rs.next()) { Object rowData[] = new Object[3];
				 * 
				 * rowData[0] = rs.getString(1); rowData[0] = rs.getString(11); rowData[0] =
				 * rs.getString(4); rowData[0] = rs.getString(5); rowData[0] = rs.getString(7);
				 * rowData[0] = rs.getString(8); rowData[0] = rs.getString(9); rowData[0] =
				 * rs.getString(14);
				 * 
				 * tableModel.addRow(rowData);
				 */
			}
		} catch (SQLException e1) {
			System.err.println("Error SQLException\n" + e1.toString());
		}
	}

	public void frameOn() {
		frame.setVisible(true);
	}

	public void frameOff() {
		frame.setVisible(false);
	}
}
//*************************************************************************
