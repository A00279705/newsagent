package gui;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import classes.OrdersToPublication;
import exceptionHandler.OrderExceptionHandler;
import run.MySQL;

public class GuiOrdToPub {

	private JFrame frame;
	private JTextField order1;
	private JTable table;
	private JTextField publication1;
	private JTextField ordtopubID;
	private DefaultTableModel tableModel;
	private JLabel message1;
	private JLabel message2;

	public GuiOrdToPub() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1110, 670);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("Enter Order ID:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1.setBounds(20, 130, 120, 30);
		frame.getContentPane().add(lblNewLabel_1);

		order1 = new JTextField();
		order1.setColumns(10);
		order1.setBounds(140, 130, 150, 30);
		frame.getContentPane().add(order1);

		JButton btnCreateDriver = new JButton("Create Ord To Pub");
		btnCreateDriver.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					message1.setText("Warning");
					int orderId = Integer.parseInt(order1.getText());
					int publicationId = Integer.parseInt(publication1.getText());
					MySQL sqlObject = new MySQL();
					OrdersToPublication custObj = new OrdersToPublication(orderId, publicationId);
					boolean insertResult = sqlObject.insertOrdPub(custObj);
					if (insertResult == true) {
						message1.setText("OrdToPub Created");
						System.out.println("Success: Ord To Pub insert Attempted\n");
					} else {
						message1.setText("OrdToPub Failed");
						System.out.println(
								"Error: SQLException wrong Ord To Pub insert sql command or order,publication id doesnt exist");
					}
					order1.setText("");
					publication1.setText("");
					ordTable();
					sqlObject.closeSQL();
				} catch (NumberFormatException E) {
					message1.setText("NumberFormatException");
					System.out.println("Error: NumberFormatException\n");
				} catch (OrderExceptionHandler e1) {
					message1.setText("OrderExceptionHandler");
					System.out.println("Error: OrderExceptionHandlerr\n");
				} catch (SQLException e1) {
					message1.setText("SQLException");
					System.out.println("Error: SQLException");
				}
			}
		});
		btnCreateDriver.setBounds(140, 230, 150, 30);
		frame.getContentPane().add(btnCreateDriver);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(326, 130, 440, 225);
		frame.getContentPane().add(scrollPane);

		tableModel = new DefaultTableModel(new String[] { "OrdToPub ID", "Order ID", "Publication ID" }, 0);
		table = new JTable(tableModel);
		scrollPane.setViewportView(table);

		JLabel lblDeliveryDriver_1 = new JLabel("Order To Publication");
		lblDeliveryDriver_1.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblDeliveryDriver_1.setBounds(387, 11, 319, 40);
		frame.getContentPane().add(lblDeliveryDriver_1);

		JLabel lbl11 = new JLabel("Enter Publication ID:");
		lbl11.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lbl11.setBounds(20, 180, 120, 30);
		frame.getContentPane().add(lbl11);

		publication1 = new JTextField();
		publication1.setColumns(10);
		publication1.setBounds(140, 180, 150, 30);
		frame.getContentPane().add(publication1);

		JLabel lblNewLabel_1_2 = new JLabel(">Create Order To Publication<");
		lblNewLabel_1_2.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_2.setBounds(20, 80, 286, 30);
		frame.getContentPane().add(lblNewLabel_1_2);

		JButton btnCreateDriver_1 = new JButton("Delete Ord To Pub");
		btnCreateDriver_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					message2.setText("Warning");
					MySQL sqlObject = new MySQL();
					int ordtopub = Integer.parseInt(ordtopubID.getText());
					OrdersToPublication ordtopubObj = new OrdersToPublication();
					ordtopubObj.validateOrdProdID_ordprod(ordtopub);
					boolean updateOrder = sqlObject.deleteOrderToPublication(ordtopubObj.getId_ordprod());
					if (updateOrder == true) {
						message2.setText("OrdToPub Deleted");
						System.out.println("Success: Ord To Pub delete Attempted\n");
					} else {
						message2.setText("OrdToPub Failed");
						System.out.println("Error: SQLException wrong Ord To Pub delete sql command");
					}
					ordtopubID.setText("");
					ordTable();
				} catch (NumberFormatException E) {
					message2.setText("NumberFormatException");
					System.out.println("Error: NumberFormatException\n");
				} catch (OrderExceptionHandler e1) {
					message2.setText("NumberFormatException");
					System.out.println("Error: OrderExceptionHandler\n");
				} catch (SQLException e1) {
					message2.setText("SQLException");
					System.out.println("Error: SQLException");
				}
			}
		});
		btnCreateDriver_1.setBounds(140, 400, 150, 30);
		frame.getContentPane().add(btnCreateDriver_1);

		ordtopubID = new JTextField();
		ordtopubID.setColumns(10);
		ordtopubID.setBounds(140, 350, 150, 30);
		frame.getContentPane().add(ordtopubID);

		JLabel lblNewLabel_1_2_1 = new JLabel(">Delete Order To Publication<");
		lblNewLabel_1_2_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_2_1.setBounds(20, 300, 296, 30);
		frame.getContentPane().add(lblNewLabel_1_2_1);

		JLabel lblNewLabel_1_3 = new JLabel("Enter OrdPub ID:");
		lblNewLabel_1_3.setBounds(20, 350, 120, 30);
		frame.getContentPane().add(lblNewLabel_1_3);

		JLabel lblNewLabel_1_2_2_1 = new JLabel(">Read Ord To Pub<");
		lblNewLabel_1_2_2_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_2_2_1.setBounds(448, 80, 197, 30);
		frame.getContentPane().add(lblNewLabel_1_2_2_1);

		JButton btnRefreshDrivers = new JButton("Refresh Ord To Pub");
		btnRefreshDrivers.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ordTable();
			}
		});
		btnRefreshDrivers.setBounds(472, 366, 150, 30);
		frame.getContentPane().add(btnRefreshDrivers);

		message1 = new JLabel("Warning");
		message1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		message1.setBackground(new Color(220, 20, 60));
		message1.setBounds(20, 230, 120, 30);
		frame.getContentPane().add(message1);

		JButton btnNewButton = new JButton("Return Main Menu");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiMain frameMain = new GuiMain();
				frameMain.frameOn();
				frameOff();
			}
		});
		btnNewButton.setBounds(20, 11, 150, 30);
		frame.getContentPane().add(btnNewButton);
		
		message2 = new JLabel("Warning");
		message2.setFont(new Font("Tahoma", Font.PLAIN, 10));
		message2.setBackground(new Color(220, 20, 60));
		message2.setBounds(20, 400, 120, 30);
		frame.getContentPane().add(message2);
		ordTable();
		frame.setVisible(true);
	}

	public void ordTable() {
		tableModel.setRowCount(0);
		try {
			MySQL sqlObject = new MySQL();
			ResultSet rs = sqlObject.printAllOrdersToPublication();
			while (rs.next()) {
				Object rowData[] = new Object[3];
				rowData[0] = rs.getString(1);
				rowData[1] = rs.getString(2);
				rowData[2] = rs.getString(3);
				tableModel.addRow(rowData);
			}
		} catch (SQLException e1) {
			System.err.println("Error SQLException\n" + e1.toString());
		}
	}

	public void frameOn() {
		frame.setVisible(true);
	}

	public void frameOff() {
		frame.setVisible(false);
	}
}
