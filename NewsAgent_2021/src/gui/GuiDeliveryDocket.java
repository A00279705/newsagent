package gui;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import classes.DeliveryDocket;
import exceptionHandler.DeliveryDocketException;
import run.MySQL;

public class GuiDeliveryDocket {

	private JFrame frame;
	private JTextField driver1;
	private JTextField area1;
	private JTextField area2;
	private JTextField type1;
	private JTextField date2;
	private JTable table;
	DefaultTableModel tableModel;
	private JLabel message1;
	private JLabel message2;

	public GuiDeliveryDocket() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1110, 670);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("Enter Driver ID:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1.setBounds(20, 130, 120, 30);
		frame.getContentPane().add(lblNewLabel_1);

		driver1 = new JTextField();
		driver1.setColumns(10);
		driver1.setBounds(140, 130, 150, 30);
		frame.getContentPane().add(driver1);

		JButton btnGenerateDocket = new JButton("Generate Docket");
		btnGenerateDocket.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					message1.setText("");
					int driver11 = Integer.parseInt(driver1.getText());
					String orderType = type1.getText();
					int area11 = Integer.parseInt(area1.getText());
					DeliveryDocket docket = new DeliveryDocket(driver11, area11, orderType);
					MySQL sqlObject = new MySQL();
					boolean insertRes = sqlObject.createDeliveryDocket(docket);
					if (insertRes == true) {
						driver1.setText("");
						type1.setText("");
						area1.setText("");
						message1.setText("Docket Created");
						System.out.println("Success: Order createDeliveryDocket Attempted\n");
					} else {
						message1.setText("Docket Failed");
						System.out.println(
								"Error: SQLException wrong Order createDeliveryDocket sql command or Customer id doesnt exist");
					}
					sqlObject.closeSQL();
				} catch (NumberFormatException E) {
					message1.setText("NumberFormatException");
					System.out.println("Error: NumberFormatException\n");
				} catch (DeliveryDocketException e1) {
					message1.setText("DeliveryDocketException");
					System.out.println("Error: DeliveryDocketException\n");
				} catch (SQLException e1) {
					message1.setText("SQLException");
					System.out.println("Error: SQLException\n");
				}
			}
		});
		btnGenerateDocket.setBounds(140, 280, 150, 30);
		frame.getContentPane().add(btnGenerateDocket);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(330, 135, 743, 334);
		frame.getContentPane().add(scrollPane);

		tableModel = new DefaultTableModel(new String[] { "Docket ID", "Driver Name", "Publication", "Date",
				"Door Number", "Area", "Name", "Surname", "Phone" }, 0);
		table = new JTable(tableModel);
		scrollPane.setViewportView(table);

		JLabel lblDeliveryDriver_1 = new JLabel("Delivery Docket");
		lblDeliveryDriver_1.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblDeliveryDriver_1.setBounds(422, 25, 249, 40);
		frame.getContentPane().add(lblDeliveryDriver_1);

		JLabel lblNewLabel_1_1 = new JLabel("Enter Area ID:");
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_1.setBounds(20, 180, 120, 30);
		frame.getContentPane().add(lblNewLabel_1_1);

		area1 = new JTextField();
		area1.setColumns(10);
		area1.setBounds(140, 180, 150, 30);
		frame.getContentPane().add(area1);

		JLabel lblNewLabel_1_2 = new JLabel(">Generate Delivery Docket<");
		lblNewLabel_1_2.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_2.setBounds(20, 80, 270, 30);
		frame.getContentPane().add(lblNewLabel_1_2);

		JButton btnread = new JButton("Read Docket");
		btnread.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					message2.setText("Warning");
					tableModel.setRowCount(0);
					int area = Integer.parseInt(area2.getText());
					String date = date2.getText();
					MySQL sqlObject = new MySQL();
					DeliveryDocket docket = new DeliveryDocket();
					docket.dvalidateDate(date);
					docket.validate_area(area);
					ResultSet rs = sqlObject.printAlldeliveryDocket(docket);
					if (rs == null) {
						message2.setText("null");
						System.out.println("Error: SQLException wrong Docket read sql command");
					} else {
						message2.setText("Docket printed");
						while (rs.next()) {
							Object rowData[] = new Object[9];
							rowData[0] = rs.getString(1);
							rowData[1] = rs.getString(26);
							rowData[2] = rs.getString(22);
							rowData[3] = rs.getString(5);
							rowData[4] = rs.getString(12);
							rowData[5] = rs.getString(13);
							rowData[6] = rs.getString(10);
							rowData[7] = rs.getString(11);
							rowData[8] = rs.getString(14);
							tableModel.addRow(rowData);
						}
					}
					sqlObject.closeSQL();
				} catch (SQLException e1) {
					message2.setText("SQLException");
					System.out.println("Error: SQLException\n");
				} catch (NumberFormatException E) {
					message2.setText("NumberFormatException");
					System.out.println("Error: NumberFormatException\n");
				} catch (DeliveryDocketException e1) {
					message2.setText("DeliveryDocketException");
					System.out.println("Error: DeliveryDocketException\n");
				}
			}
		});
		btnread.setBounds(140, 480, 150, 30);
		frame.getContentPane().add(btnread);

		area2 = new JTextField();
		area2.setColumns(10);
		area2.setBounds(140, 380, 150, 30);
		frame.getContentPane().add(area2);

		JLabel lblNewLabel_1_2_1 = new JLabel(">Read Delivery Docket<");
		lblNewLabel_1_2_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_2_1.setBounds(20, 330, 270, 30);
		frame.getContentPane().add(lblNewLabel_1_2_1);

		JLabel lblNewLabel_1_3 = new JLabel("Enter Area:");
		lblNewLabel_1_3.setBounds(20, 380, 120, 30);
		frame.getContentPane().add(lblNewLabel_1_3);

		JLabel lblNewLabel_1_2_2_1 = new JLabel(">Read Delivery Docket<");
		lblNewLabel_1_2_2_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_2_2_1.setBounds(429, 94, 236, 30);
		frame.getContentPane().add(lblNewLabel_1_2_2_1);

		JButton btnNewButton = new JButton("Return Main Menu");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiMain frameMain = new GuiMain();
				frameMain.frameOn();
				frameOff();
			}
		});
		btnNewButton.setBounds(34, 25, 150, 30);
		frame.getContentPane().add(btnNewButton);

		type1 = new JTextField();
		type1.setColumns(10);
		type1.setBounds(140, 230, 150, 30);
		frame.getContentPane().add(type1);

		JLabel lblNewLabel_1_1_2 = new JLabel("Enter Order Type:");
		lblNewLabel_1_1_2.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_1_2.setBounds(20, 230, 120, 30);
		frame.getContentPane().add(lblNewLabel_1_1_2);

		JLabel lblNewLabel_1_3_1 = new JLabel("Enter YYYY-MM-DD:");
		lblNewLabel_1_3_1.setBounds(20, 430, 120, 30);
		frame.getContentPane().add(lblNewLabel_1_3_1);

		date2 = new JTextField();
		date2.setToolTipText("");
		date2.setColumns(10);
		date2.setBounds(140, 430, 150, 30);
		frame.getContentPane().add(date2);

		message1 = new JLabel("Warning");
		message1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		message1.setBounds(20, 280, 120, 30);
		frame.getContentPane().add(message1);

		JButton btnViewAll = new JButton("View All");
		btnViewAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					tableModel.setRowCount(0);
					MySQL sqlObject = new MySQL();
					ResultSet rs = sqlObject.printAllDeliveryDockets();
					if (rs == null) {
						System.out.println("Error: SQLException wrong Order read sql command");
					} else {
						while (rs.next()) {
							Object rowData[] = new Object[9];
							rowData[0] = rs.getString(1);
							rowData[1] = rs.getString(26);
							rowData[2] = rs.getString(22);
							rowData[3] = rs.getString(5);
							rowData[4] = rs.getString(12);
							rowData[5] = rs.getString(13);
							rowData[6] = rs.getString(10);
							rowData[7] = rs.getString(11);
							rowData[8] = rs.getString(14);
							tableModel.addRow(rowData);
						}
					}
					sqlObject.closeSQL();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnViewAll.setBounds(624, 480, 150, 30);
		frame.getContentPane().add(btnViewAll);
		
		message2 = new JLabel("Warning");
		message2.setFont(new Font("Tahoma", Font.PLAIN, 11));
		message2.setBounds(20, 480, 120, 30);
		frame.getContentPane().add(message2);
		frame.setVisible(true);
	}

	public void frameOn() {
		frame.setVisible(true);
	}

	public void frameOff() {
		frame.setVisible(false);
	}
}
