package gui;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import classes.Order;
import exceptionHandler.OrderExceptionHandler;
import run.MySQL;

public class GuiOrder {
	private JFrame frame;
	private JTable table;
	private JTextField deleteOrdTxt;
	private DefaultTableModel tableModel;
	private JTextField ordertypeTxt;
	private JTextField custidTxt;
	private JTextField updateIDTxt;
	private JTextField updateordertypeTxt;
	private JTextField updatecustidTxt;
	private JLabel message1;
	private JLabel message2;
	private JLabel message3;

	public GuiOrder() {

		frame = new JFrame();
		frame.setBounds(100, 100, 1110, 670);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblOrdersMenu = new JLabel("Orders");
		lblOrdersMenu.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblOrdersMenu.setBounds(489, 10, 394, 40);
		frame.getContentPane().add(lblOrdersMenu);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(284, 116, 525, 273);
		frame.getContentPane().add(scrollPane);

		tableModel = new DefaultTableModel(new String[] { "ID", "Order Type", "Customer ID"}, 0);
		table = new JTable(tableModel);
		scrollPane.setViewportView(table);

		JLabel LabelOrderType = new JLabel("Order Type:");
		LabelOrderType.setFont(new Font("Tahoma", Font.PLAIN, 11));
		LabelOrderType.setBounds(10, 120, 80, 30);
		frame.getContentPane().add(LabelOrderType);

	

		JLabel LabelCreateOrder = new JLabel(">Create Order<");
		LabelCreateOrder.setFont(new Font("Tahoma", Font.BOLD, 18));
		LabelCreateOrder.setBounds(20, 80, 270, 30);
		frame.getContentPane().add(LabelCreateOrder);

		JButton btnDeleteOrder = new JButton("Delete Order");
		btnDeleteOrder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					MySQL sqlObject = new MySQL();
					int id = Integer.parseInt(deleteOrdTxt.getText());
					Order ordObj = new Order();
					ordObj.validateID_customer(id);
					boolean deleteOrders = sqlObject.deleteOrders(id);
					if (deleteOrders == true) {
						message2.setText("Order Deleted");
						System.out.println("Success: Order Delete Attempted\n");
					} else {
						message2.setText("Publication Not Deleted");
						System.out.println("Error: SQLException wrong Order delete sql command");
					}
					orderTable();
				}catch (NumberFormatException eee) {
					message2.setText("NumberFormatException");
					System.out.println("Error: NumberFormatException\n");
				} catch (OrderExceptionHandler ee) {
					message2.setText("OrderExceptionHandler");
					System.out.println("Error: OrderExceptionHandler\n");
				} catch (SQLException e1) {
					message2.setText("SQLException");
					System.out.println("Error: SQLException");
				}
			}
				
		});
		btnDeleteOrder.setBounds(130, 456, 150, 30);
		frame.getContentPane().add(btnDeleteOrder);

		deleteOrdTxt = new JTextField();
		deleteOrdTxt.setColumns(10);
		deleteOrdTxt.setBounds(130, 415, 150, 30);
		frame.getContentPane().add(deleteOrdTxt);

		JLabel LabelDeleteOrder = new JLabel(">Delete Order<");
		LabelDeleteOrder.setFont(new Font("Tahoma", Font.BOLD, 18));
		LabelDeleteOrder.setBounds(10, 375, 270, 30);
		frame.getContentPane().add(LabelDeleteOrder);

		JLabel lblNewLabel_1_3 = new JLabel("Enter Order ID:");
		lblNewLabel_1_3.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_3.setBounds(10, 420, 110, 19);
		frame.getContentPane().add(lblNewLabel_1_3);

		JLabel LabelUpdateOrder = new JLabel(">Update Order<");
		LabelUpdateOrder.setFont(new Font("Tahoma", Font.BOLD, 18));
		LabelUpdateOrder.setBounds(879, 80, 207, 30);
		frame.getContentPane().add(LabelUpdateOrder);

		JLabel labelOrderType = new JLabel("Order Type:");
		labelOrderType.setFont(new Font("Tahoma", Font.PLAIN, 11));
		labelOrderType.setBounds(831, 210, 112, 30);
		frame.getContentPane().add(labelOrderType);

//		JLabel labelCustomerID = new JLabel("Customer ID:");
//		labelCustomerID.setFont(new Font("Tahoma", Font.PLAIN, 11));
//		labelCustomerID.setBounds(831, 269, 112, 30);
//		frame.getContentPane().add(labelCustomerID);

	

		JButton btnUpdateOrder = new JButton("Update Order");
		btnUpdateOrder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					MySQL sqlObject = new MySQL();
					int id = Integer.parseInt(updateIDTxt.getText());
					String ordertype = updateordertypeTxt.getText();
//					int custid = Integer.parseInt(updatecustidTxt.getText());
					Order updateOrd = new Order();
					updateOrd.validateID_order(id);
					updateOrd.validateOrder_UpdateType(ordertype);
//					updateOrd.validateID_customer(custid);
					
					boolean updateOrders = sqlObject.updateOrders(updateOrd);
					if(updateOrders == true) {
						message3.setText("Order Updated");
						System.out.println("Success: Order Update Attempted\n");
					} else {
						message3.setText("Order Not Updated");
						System.out.println("Error: SQLException wrong Order update sql command\n");
					}
					orderTable();
				} catch (NumberFormatException e1) {
					message3.setText("NumberFormatException");
					System.out.println("Error: NumberFormatException\n");
				} catch (OrderExceptionHandler e1) {
					message3.setText("OrderExceptionHandler");
					System.out.println("Error: OrderExceptionHandler\n");
				} catch (SQLException e1) {
					message3.setText("SQLException");
					System.out.println("Error: SQLException");
				}
				}
			
		});
		btnUpdateOrder.setBounds(936, 251, 150, 30);
		frame.getContentPane().add(btnUpdateOrder);

		JLabel LabelUpdateID = new JLabel("Enter Order ID:");
		LabelUpdateID.setFont(new Font("Tahoma", Font.PLAIN, 11));
		LabelUpdateID.setBounds(831, 155, 112, 30);
		frame.getContentPane().add(LabelUpdateID);

		
		
		JLabel LabelOrderRead = new JLabel(">Read Orders<");
		LabelOrderRead.setFont(new Font("Tahoma", Font.BOLD, 18));
		LabelOrderRead.setBounds(456, 80, 189, 30);
		frame.getContentPane().add(LabelOrderRead);
		
		JLabel LabelBookPrice = new JLabel("Customer ID:");
		LabelBookPrice.setFont(new Font("Tahoma", Font.PLAIN, 11));
		LabelBookPrice.setBounds(10, 172, 80, 30);
		frame.getContentPane().add(LabelBookPrice);
		
		ordertypeTxt = new JTextField();
		ordertypeTxt.setBounds(100, 121, 150, 30);
		frame.getContentPane().add(ordertypeTxt);
		ordertypeTxt.setColumns(10);
		
		custidTxt = new JTextField();
		custidTxt.setColumns(10);
		custidTxt.setBounds(100, 173, 150, 30);
		frame.getContentPane().add(custidTxt);
		
		JButton btnCreateOrd = new JButton("Create Order");
		btnCreateOrd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try{
				String ordertype = ordertypeTxt.getText();
				int custid = Integer.parseInt(custidTxt.getText());
				MySQL sqlObject = new MySQL();
				Order custObj = new Order(ordertype, custid);
				boolean insertResult = sqlObject.insertOrders(custObj);
				if (insertResult == true) {
					message1.setText("Order Created");
					ordertypeTxt.setText("");
					custidTxt.setText("");
					System.out.println("Success: Order insert Attempted\n");
				}
				else {
					message1.setText("Order Failed");
					System.out.println(
							"Error: SQLException wrong Order insert sql command or Order id doesnt exist");
				}
				orderTable();
				sqlObject.closeSQL();
				} 
				catch (SQLException e1) {
					message1.setText("SQLException");
					System.out.println("Error: SQLException");
				} catch (OrderExceptionHandler e1) {
					message1.setText("OrderExceptionHandler");
					System.out.println("Error: OrderExceptionHandler");
				}
				catch (NumberFormatException e1) {
					message1.setText("NumberFormatException");
					System.out.println("Error: NumberFormatException\n");
				}
			}
		});
		btnCreateOrd.setBounds(100, 210, 150, 30);
		frame.getContentPane().add(btnCreateOrd);
		
		updateIDTxt = new JTextField();
		updateIDTxt.setColumns(10);
		updateIDTxt.setBounds(936, 156, 150, 30);
		frame.getContentPane().add(updateIDTxt);
		
		updateordertypeTxt = new JTextField();
		updateordertypeTxt.setColumns(10);
		updateordertypeTxt.setBounds(936, 211, 150, 30);
		frame.getContentPane().add(updateordertypeTxt);
		
//		updatecustidTxt = new JTextField();
//		updatecustidTxt.setColumns(10);
//		updatecustidTxt.setBounds(936, 270, 150, 30);
//		frame.getContentPane().add(updatecustidTxt);
		
		JButton btnRefreshPub = new JButton("Refresh Orders");
		btnRefreshPub.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				orderTable();
			}
		});
		btnRefreshPub.setBounds(468, 399, 150, 30);
		frame.getContentPane().add(btnRefreshPub);
		
		JButton btnHome = new JButton("Return Main Menu");
		btnHome.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiMain frameMain = new GuiMain();
				frameMain.frameOn();
				frameOff();
			}
		});
		btnHome.setBounds(20, 11, 150, 30);
		frame.getContentPane().add(btnHome);
		
		message1 = new JLabel("Warning");
		message1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		message1.setBounds(10, 238, 120, 30);
		frame.getContentPane().add(message1);
		
		message2 = new JLabel("Warning");
		message2.setFont(new Font("Tahoma", Font.PLAIN, 10));
		message2.setBounds(10, 456, 120, 30);
		frame.getContentPane().add(message2);
		
		message3 = new JLabel("Warning");
		message3.setFont(new Font("Tahoma", Font.PLAIN, 10));
		message3.setBounds(819, 251, 120, 30);
		frame.getContentPane().add(message3);
		
		

}
	
	
	public void orderTable() {
		tableModel.setRowCount(0);
		try {
			MySQL sqlObject = new MySQL();
			ResultSet rs = sqlObject.printAllOrders();
			while (rs.next()) {
				Object rowData[] = new Object[3];
				rowData[0] = rs.getString(1);
				rowData[1] = rs.getString(2);
				rowData[2] = rs.getString(3);
				tableModel.addRow(rowData);
			}
		} catch (SQLException e1) {
			System.err.println("Error SQLException\n" + e1.toString());
		}
	}
	
	public void frameOn() {
		frame.setVisible(true);
	}

	public void frameOff() {
		frame.setVisible(false);
	}
}
