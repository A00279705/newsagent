package gui;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;

import classes.Driver;
import exceptionHandler.DriversExceptionHandler;
import run.MySQL;

import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class Guidriver {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JButton btnNewButton_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Guidriver window = new Guidriver();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Guidriver() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 648, 519);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					String name = textField.getText();
					String password = textField_1.getText();
					MySQL sqlObject = new MySQL();
					Driver driverObj = new Driver(name, password);
					boolean insertResult = sqlObject.insertdelivery_driver(driverObj);
					if (insertResult == true) {
						textField.setText("");
						textField_1.setText("");
						System.out.println("Success: Order insert Attempted\n");
					} else {
						System.out.println(
								"Error: SQLException wrong Order insert sql command or Customer id doesnt exist");
					}
				
				
				} catch (DriversExceptionHandler e1) {

				
					System.out.println("Error: OrderExceptionHandler\n");
				} catch (SQLException e1) {
					System.out.println("Error: SQLException");
				}

			}
		});
		btnNewButton.setBounds(10, 35, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		textField = new JTextField();
		textField.setBounds(217, 36, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(217, 90, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(217, 149, 86, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		btnNewButton_1 = new JButton("New button");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					MySQL sqlObject = new MySQL();
					int id = Integer.parseInt(textField_2.getText());
					Driver driverObj = new Driver();
					driverObj.validateID_driver(id);
					boolean updateDriver = sqlObject.deleteDriver(driverObj);
					if (updateDriver == true) {
						System.out.println("Success: Driver Delete Attempted\n");
					} else {
						System.out.println("Error: SQLException wrong Driver delete sql command");
					}
									} catch (NumberFormatException eee) {
					System.out.println("Error: NumberFormatException\n");
				} catch (DriversExceptionHandler ee) {
					System.out.println("Error: DriverExceptionHandler\n");
				} catch (SQLException e1) {
					System.out.println("Error: SQLException");
				}
	
				
			}
		});
		btnNewButton_1.setBounds(10, 89, 89, 23);
		frame.getContentPane().add(btnNewButton_1);
	}
}
