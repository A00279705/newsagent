package tests;
import classes.Driver;
import exceptionHandler.DriversExceptionHandler;
import junit.framework.TestCase;

public class DriverTest extends TestCase {

	// +++++++++++++++++++++++++++TEST Driver Name+++++++++++++++++++++++++++++++++
	// Test #: 1 null value name
	// Test Objective:To catch an empty - driver name
	// Inputs: name = ""
	// Expected Output: Error: Your name must contain characters"
	public void testValidateName_driver01() {
		try {
			Driver driverObj = new Driver();
			driverObj.ValidateName_driver("");
			fail("Exception expected");
		} catch (DriversExceptionHandler e) {
			assertEquals("Error: You name must contain characters", e.getMessage());
		}
	}

	// Test #: 2 name length greater than 45
	// Test Objective:To catch an a driver name length greater than 45
	// Inputs: name = "hello my length is greater than 45 so there must be an
	// exception"
	// Expected Output: Error: Your name length must be less than 45"
	public void testValidateName_driver02() {
		try {
			Driver driverObj = new Driver();
			driverObj.ValidateName_driver("hello my length is greater than 45 so there must be an exception");
			fail("Exception expected");
		} catch (DriversExceptionHandler e) {
			assertEquals("Error: driver name length not greater then 45", e.getMessage());
		}
	}

	// Test #: 3 name length within range
	// Test Objective:To input a valid driver name
	// Inputs: name = "john Paul"
	// Expected Output: No Exception"
	public void testValidateName_driver03() {
		try {
			Driver driverObj = new Driver();
			driverObj.ValidateName_driver("john Paul");
		} catch (DriversExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++ Boundary values test
	// Test #: 8 name length = 45
	// Test Objective:Accept driver name of length 45
	// Inputs: name = "john Paul qwertyuioplkjhgfdsazxcvbnmqwertyuiop"
	// Expected Output: No Exception"

	public void testValidateName_driver04() {
		try {
			Driver driverObj = new Driver();
			driverObj.ValidateName_driver("johnPaulqwertyuioplkjhgfdsuiop");
		} catch (DriversExceptionHandler e) {
			fail("Exception not expected");
		}
	}

//++++++++++++++++++++++++++++++++++++++++ Test driver password  ++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// Test #: 4 null value password
	// Test Objective:To catch an empty - driver password
	// Inputs: password = ""
	// Expected Output: Error: "You can't enter an empty password"

	public void testValidatePassword_driver01() {
		try {
			Driver driverObj = new Driver();
			driverObj.ValidatePassword_driver("");
			fail("Exception expected");
		} catch (DriversExceptionHandler e) {
			assertEquals("Error: You password must contain characters", e.getMessage());
		}
	}

	// Test #: 5 password length less than 8 characters
	// Test Objective:To catch a password less than 8 characters - driver password
	// Inputs: password = "1234567"
	// Expected Output: Error: "Your password must contain at least 8 characters"

	public void testValidatePassword_driver02() {
		try {
			Driver driverObj = new Driver();
			driverObj.ValidatePassword_driver("1234567");
			fail("Exception expected");
		} catch (DriversExceptionHandler e) {
			assertEquals("Error: Your password must contains at least 8 Characters", e.getMessage());
		}
	}

	// Test #: 6 password length greater than 45 characters
	// Test Objective:To catch a password greater than 45 characters - driver
	// password
	// Inputs: password = "123456789sdfghsdhsgyhsdfgestyergdfhaearghaerhahayj"
	// Expected Output: Error: "Your password must contain at least 8 characters"

	public void testValidatePassword_driver03() {
		try {
			Driver driverObj = new Driver();
			driverObj.ValidatePassword_driver("sssssssssssssssssssssgestyergdfhaearghaerhahayj");
			fail("Exception expected");
		} catch (DriversExceptionHandler e) {
			assertEquals("Error: driver password length not greater then 45", e.getMessage());
		}
	}

	// Test #: 7 password length greater than 8
	// Test Objective: To input a valid driver password
	// Inputs: name = "1password"
	// Expected Output: No Exception"

	public void testValidatePassword_driver04() {
		try {
			Driver driverObj = new Driver();
			driverObj.ValidatePassword_driver("password");
		} catch (DriversExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 9 password length = 45 characters
	// Test Objective:Accept a password = 45 characters - driver password
	// Inputs: password = "1234567890123456789012345678901234567890qwert"
	// Expected Output: Error: "No exception"

	public void testValidatePassword_driver05() {
		try {
			Driver driverObj = new Driver();
			driverObj.ValidatePassword_driver("12345678");
		} catch (DriversExceptionHandler e) {
			fail("Exception not expected");
		}
	}
	// Test #: 10 password length = 8
	// Test Objective: Accept a valid driver password of length=8
	// Inputs: name = "password"
	// Expected Output: No Exception"

	public void testValidatePassword_driver06() {
		try {
			Driver driverObj = new Driver();
			driverObj.ValidatePassword_driver("password");
		} catch (DriversExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// ++++++++++++++++++++++++++ Testing Constructor++++++++++++++++++++++++++++
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Test #: 11
	// Test Objective: To create a valid driver by providing the value of name and
	// password
	// Inputs: name = "john paul", password = "password"
	// Expected Output:Driver created with name = "john paul", password = "password"

	public void testDriver001() {
		try {
			Driver driverObj = new Driver("john paul", "password");
			assertEquals("john paul", driverObj.getName());
			assertEquals("password", driverObj.getPassword());
		} catch (DriversExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 12
	// Test Objective: try to create a invalid driver by providing the value of name
	// and password
	// Inputs: name = "", password = "password"
	// Expected Output:Error: "Your name must not be empty contain characters"

	public void testDriver002() {
		try {
			Driver driverObj = new Driver("", "password");
			assertEquals("", driverObj.getName());
			assertEquals("password", driverObj.getPassword());
			fail("Exception expected");
		} catch (DriversExceptionHandler e) {
			assertEquals("Error : Fialed to set Driver", e.getMessage());
		}
	}

	// Test #: 13
	// Test Objective: try to create a invalid driver by providing the value of name
	// and password
	// Inputs: name = "john paul", password = ""
	// Expected Output:"Error: Your password must not be empty"

	public void testDriver003() {
		try {
			Driver driverObj = new Driver("john paul", "");
			assertEquals("john paul", driverObj.getName());
			assertEquals("", driverObj.getPassword());
			fail("Exception expected");
		} catch (DriversExceptionHandler e) {
			assertEquals("Error : Fialed to set Driver", e.getMessage());
		}
	}
	// Test #: 14
	// Test Objective: try to create a invalid driver by providing the value of name
	// and password
	// Inputs: name = "john paul", password = "1234567"
	// Expected Output:"Error: Your password length must be at least 8"

	public void testDriver004() {
		try {
			Driver driverObj = new Driver("john paul", "1234567");
			assertEquals("john paul", driverObj.getName());
			assertEquals("1234567", driverObj.getPassword());
			fail("Exception expected");
		} catch (DriversExceptionHandler e) {
			assertEquals("Error : Fialed to set Driver", e.getMessage());
		}
	}
	// Test #: 15
	// Test Objective: try to create a invalid driver by providing the value of name
	// and password
	// Inputs: name = "thrthhfjhtrwtwywtyyyyeyeyeyeyeryreyryyeryreeyerydfj",
	// password = "password"
	// Expected Output:Error: "Your name length must be less than 45"

	public void testDriver005() {
		try {
			Driver driverObj = new Driver("thrthhfjhtrwtwywtyyyyeyeyeyeyeryreyryyeryreeyerydfj", "password");
			assertEquals("thrthhfjhtrwtwywtyyyyeyeyeyeyeryreyryyeryreeyerydfj", driverObj.getName());
			assertEquals("password", driverObj.getPassword());
			fail("Exception expected");
		} catch (DriversExceptionHandler e) {
			assertEquals("Error : Fialed to set Driver", e.getMessage());
		}
	}
	// Test #: 16
	// Test Objective: try to create a invalid driver by providing the value of name
	// and password
	// Inputs: name = "john paul", password =
	// "1234567fghfgjfjfjrrjrjrjrtjtujuujtuetueueeryeryeye"
	// Expected Output:"Error: Your password length must be at most 45"

	public void testDriver006() {
		try {
			Driver driverObj = new Driver("john paul", "1234567fghfgjfjfjrrjrjrjrtjtujuujtuetueueeryeryeye");
			assertEquals("john paul", driverObj.getName());
			assertEquals("1234567fghfgjfjfjrrjrjrjrtjtujuujtuetueueeryeryeye", driverObj.getPassword());
			fail("Exception expected");
		} catch (DriversExceptionHandler e) {
			assertEquals("Error : Fialed to set Driver", e.getMessage());
		}
	}

}
