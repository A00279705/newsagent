package tests;
import classes.OrdersToPublication;
import exceptionHandler.OrderExceptionHandler;
import junit.framework.TestCase;

public class OrdersToProductsTest extends TestCase {

	// Test #: 1
	// Test Objective: To create Order To Publication
	// Inputs: id_order = 1, id_publication = 1
	// Expected Output: id_order = 1, id_publication = 1
	public void testOrdProdContructor001() {
		try {
			OrdersToPublication ordObj = new OrdersToPublication(1, 1);
			assertEquals(1, ordObj.getId_order());
			assertEquals(1, ordObj.getId_publication());
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 1.5
	// Test Objective: To create Order To Publication
	// Inputs: id_order = -1, id_publication = 1
	// Expected Output: id_order = 1, id_publication = 1
	public void testOrdProdContructor002() {
		try {
			OrdersToPublication ordObj = new OrdersToPublication(-1, 1);
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: Failed to set Orders To Publication", e.getMessage());
		}
	}

	// Test #: 2
	// Test Objective: To input a valid Order ID
	// Inputs: id_order = Integer.MAX_VALUE
	// Expected Output: No Exception"
	public void testValidateOrdProdID_order01() {
		try {
			OrdersToPublication ordObj = new OrdersToPublication();
			ordObj.validateOrdProdID_order(Integer.MAX_VALUE);
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 3
	// Test Objective: To input a valid Order ID
	// Inputs: id_order = 1
	// Expected Output: No Exception"
	public void testValidateOrdProdID_order02() {
		try {
			OrdersToPublication ordObj = new OrdersToPublication();
			ordObj.validateOrdProdID_order(1);
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 4
	// Test Objective:To catch an invalid Order ID
	// Inputs: id_order = 0
	// Expected Output: Error: You cant enter a number bellow 1"
	public void testValidateOrdProdID_order03() {
		try {
			OrdersToPublication ordObj = new OrdersToPublication();
			ordObj.validateOrdProdID_order(0);
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You cant enter a number bellow 1", e.getMessage());
		}
	}

	// Test #: 5
	// Test Objective:To catch an invalid Order ID
	// Inputs: id_order = Integer.MIN_VALUE
	// Expected Output: Error: You cant enter a number bellow 1"
	public void testValidateOrdProdID_order04() {
		try {
			OrdersToPublication ordObj = new OrdersToPublication();
			ordObj.validateOrdProdID_order(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You cant enter a number bellow 1", e.getMessage());
		}
	}

	// Test #: 6
	// Test Objective: To input a valid Publication ID
	// Inputs: id_publication = Integer.MAX_VALUE
	// Expected Output: No Exception"
	public void testValidateOrdProdID_publication01() {
		try {
			OrdersToPublication ordObj = new OrdersToPublication();
			ordObj.validateOrdProdID_publication(Integer.MAX_VALUE);
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 7
	// Test Objective: To input a valid Publication ID
	// Inputs: id_publication = 1
	// Expected Output: No Exception"
	public void testValidateOrdProdID_publication02() {
		try {
			OrdersToPublication ordObj = new OrdersToPublication();
			ordObj.validateOrdProdID_publication(1);
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 8
	// Test Objective:To catch an invalid Publication ID
	// Inputs: id_publication = 0
	// Expected Output: Error: You cant enter a number bellow 1"
	public void testValidateOrdProdID_publication03() {
		try {
			OrdersToPublication ordObj = new OrdersToPublication();
			ordObj.validateOrdProdID_publication(0);
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You cant enter a number bellow 1", e.getMessage());
		}
	}

	// Test #: 9
	// Test Objective:To catch an invalid Publication ID
	// Inputs: id_publication = Integer.MIN_VALUE
	// Expected Output: Error: You cant enter a number bellow 1"
	public void testValidateOrdProdID_publication04() {
		try {
			OrdersToPublication ordObj = new OrdersToPublication();
			ordObj.validateOrdProdID_publication(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You cant enter a number bellow 1", e.getMessage());
		}
	}

	// Test #: 10
	// Test Objective: To input a valid Order To Publication ID
	// Inputs: id_ordprod = Integer.MAX_VALUE
	// Expected Output: id_ordprod = Integer.MAX_VALUE
	public void testValidateOrdProdID_ordprod01() {
		try {
			OrdersToPublication ordObj = new OrdersToPublication();
			ordObj.validateOrdProdID_ordprod(Integer.MAX_VALUE);
			assertEquals(Integer.MAX_VALUE, ordObj.getId_ordprod());
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 11
	// Test Objective: To input a valid Order To Publication ID
	// Inputs: id_ordprod = 1
	// Expected Output: id_ordprod = 1
	public void testValidateOrdProdID_ordprod02() {
		try {
			OrdersToPublication ordObj = new OrdersToPublication();
			ordObj.validateOrdProdID_ordprod(1);
			assertEquals(1, ordObj.getId_ordprod());
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 12
	// Test Objective:To catch an invalid Order To Publication ID
	// Inputs: id_ordprod = 0
	// Expected Output: Error: You cant enter a number bellow 1"
	public void testValidateOrdProdID_ordprod03() {
		try {
			OrdersToPublication ordObj = new OrdersToPublication();
			ordObj.validateOrdProdID_ordprod(0);
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You cant enter a number bellow 1", e.getMessage());
		}
	}

	// Test #: 13
	// Test Objective:To catch an invalid Order To Publication ID
	// Inputs: id_ordprod = Integer.MIN_VALUE
	// Expected Output: Error: You cant enter a number bellow 1"
	public void testValidateOrdProdID_ordprod04() {
		try {
			OrdersToPublication ordObj = new OrdersToPublication();
			ordObj.validateOrdProdID_ordprod(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You cant enter a number bellow 1", e.getMessage());
		}
	}

}
