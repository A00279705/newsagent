package tests;

import classes.Area;
import exceptionHandler.AreaExceptionHandler;
import junit.framework.TestCase;

public class AreaTest extends TestCase {

	// +++++++++++++++++++++++++++TEST customer_id +++++++++++++++++++++++++++++++++

// Test #: 1	min value
	// Test Objective:To catch an invalid - area Customer ID
	// Inputs: id_customer = Integer.MIN_VALUE
	// Expected Output: Error: You can't enter a number below 1"

	public void testValidateId_customer001() {
		try {
			Area areaObj = new Area();
			areaObj.validateId_customer(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (AreaExceptionHandler e) {
			assertEquals("Error: You cant enter a number below 1", e.getMessage());
		}
	}

	// Test #: 4 ACCEPT create a customer ID
	// Test Objective: To input a valid Customer ID
	// Inputs: id_customer = MAX>VALUE
	// Expected Output: No Exception"

	public void testValidateId_customer002() {
		try {
			Area areaObj = new Area();
			areaObj.validateId_customer(Integer.MAX_VALUE);
		} catch (AreaExceptionHandler e) {
			fail("Exception not expected");
		}
	}

// Test #: 3  invalid nr	0
	// Test Objective:To catch an invalid area Customer ID
	// Inputs: id_customer = 0
	// Expected Output: Error: You can't enter a number 0 as an ID customer"
	public void testValidateId_customer003() {
		try {
			Area areaObj = new Area();
			areaObj.validateId_customer(0);

			fail("Exception expected");
		} catch (AreaExceptionHandler e) {
			// assertEquals("Error: You can't enter a number 0 as an ID customer",
			// e.getMessage());
			assertEquals("Error: You cant enter a number below 1", e.getMessage());
		}
	}

// Test #: 4 	ACCEPT create a customer ID
	// Test Objective: To input a valid Customer ID
	// Inputs: id_customer = 1
	// Expected Output: No Exception"

	public void testValidateId_customer004() {
		try {
			Area areaObj = new Area();
			areaObj.validateId_customer(1);
		} catch (AreaExceptionHandler e) {
			fail("Exception not expected");

		}
	}
	/*
	 * // Test #: 5 // Test Objective: To input a valid Customer ID // Inputs:
	 * id_customer = Integer.MAX_VALUE // Expected Output: No Exception" public void
	 * testValidateCustomerID005() { try { Area areaObj = new Area();
	 * areaObj.validateId_customer(Integer.MAX_VALUE); } catch (AreaExceptionHandler
	 * e) { fail("Exception not expected"); } }
	 */

//+++++++++++++++++++++++++++ Test driver ID  ++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// Test #: 1 min value
	// Test Objective:To catch an invalid - area driver ID
	// Inputs: id_customer = Integer.MIN_VALUE
	// Expected Output: Error: You can't enter a number below 1"

	public void testValidateId_driver001() {
		try {
			Area areaObj = new Area();
			areaObj.validateId_driver(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (AreaExceptionHandler e) {
			assertEquals("Error: You cant enter a number below 1", e.getMessage());
		}
	}

	// Test #: 2 max value
	// Test Objective:To catch an invalid - area driver ID
	// Inputs: id_driver = Integer.MAX_VALUE
	// Expected Output: Error: Invalid number, out of range

	public void testValidateId_driver002() {
		try {
			Area areaObj = new Area();
			areaObj.validateId_driver(Integer.MAX_VALUE);
			fail("Exception expected");
		} catch (AreaExceptionHandler e) {
			assertEquals("Error: The number is out of range", e.getMessage());
		}
	}

	// Test #: 3 invalid nr 0
	// Test Objective:To catch an invalid area driver ID
	// Inputs: id_driver = 0
	// Expected Output: Error: You can't enter a number 0 as an ID driver"
	public void testValidateId_driver003() {
		try {
			Area areaObj = new Area();
			areaObj.validateId_driver(0);

			fail("Exception expected");
		} catch (AreaExceptionHandler e) {
			assertEquals("Error: You cant enter a number below 1", e.getMessage());
		}
	}

	// Test #: 4 ACCEPT ID driver 1 to 24
	// Test Objective: To input a valid Driver ID
	// Inputs: id_driver = 1 to 24
	// Expected Output: No Exception"

	public void testValidateId_driver004() {
		try {
			Area areaObj = new Area();
			areaObj.validateId_driver(1);
		} catch (AreaExceptionHandler e) {

			fail("Exception not expected");
		}
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++ Boundary values test
	// Test #: 5 ACCEPT create a driver ID 1 - 24
	// Test Objective: To input a valid Driver ID
	// Inputs: id_driver = 1
	// Expected Output: No Exception"

	public void testValidateId_driver005() {
		try {
			Area areaObj = new Area();
			areaObj.validateId_driver(24);
		} catch (AreaExceptionHandler e) {

			fail("Exception not expected");
		}
	}

	// Test #: 6 Test 25
	// Test Objective:To catch an invalid area driver ID
	// Inputs: id_driver = 25
	// Expected Output: Error: You can't enter a number 25 as an ID driver"
	public void testValidateId_driver006() {
		try {
			Area areaObj = new Area();
			areaObj.validateId_driver(25);

			fail("Exception expected");
		} catch (AreaExceptionHandler e) {
			assertEquals("Error: The number is out of range", e.getMessage());
		}
	}

	// ++++++++++++++ Testing Constructor
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Test #: 1
	// Test Objective: To create a customer and driver id for the area
	// Inputs: id_customer = "1", id_driver = 1,
	// Expected Output:delivery Area Object created with
	// id_customer = "1", id_driver = 1,

	public void testaArea001() {
		try {
			Area areaObj = new Area(1, 1);
			assertEquals(1, areaObj.getId_customer());
			assertEquals(1, areaObj.getId_driver());
		} catch (AreaExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 2
	// Test Objective: To create a customer and driver id for the area
	// Inputs: id_customer = "-1", id_driver = -1,
	// Expected Output:delivery Area Object created with
	// id_customer = "1", id_driver = 1,

	public void testaArea002() {
		try {
			Area areaObj = new Area(-1, -1);
			assertEquals(-1, areaObj.getId_customer());
			assertEquals(-1, areaObj.getId_driver());
		} catch (AreaExceptionHandler e) {
			assertEquals("Error: You cant enter a number below 1", e.getMessage());
		}
	}

	// Test #: 3
	// Test Objective: To create a customer and driver id for the area
	// Inputs: id_customer = "-1", id_driver = -1,
	// Expected Output:delivery Area Object created with
	// id_customer = "1", id_driver = 1,

	public void testArea003() {
		try {
			Area areaObj = new Area(1, 25);
			assertEquals(1, areaObj.getId_customer());
			assertEquals(25, areaObj.getId_driver());
		} catch (AreaExceptionHandler e) {
			assertEquals("Error: The number is out of range", e.getMessage());

		}
	}

	// +++++++++++++++++++++ ID DRIVER TEST

	// Test #:
	// Test Objective: To catch an invalid id driver
	// Input areaName = "-1"
	// Expected Output: Exception Message: "Area Name does not meet minimum length
	// requirements"

//		public void testId_driver002() {
//			try {
//				Area areaObj2 = new Area("M", 2);
//				assertEquals("M", areaObj2.getName_deliveryArea());
//				assertEquals(2, areaObj2.getId_area());
//				fail("Exception expected");
//			} catch (AreaExceptionHandler e) {
//				assertEquals("Name of Delivery Area does not meet minimum length requirements", e.getMessage());

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// --------------------------- Area ------------------------------//

	// Test #: 1 Max value
	// Test Objective: Test Delivery Area
	// Input: id_area = Integer.MAX_VALUE
	// Expected Output: No Exception

	public void testDelete1() {
		try {
			Area areaObj = new Area();
			areaObj.validateId_driver(Integer.MAX_VALUE);
			fail("Exception expected");
		} catch (AreaExceptionHandler e) {
			assertEquals("Error: The number is out of range", e.getMessage());
		}
	}

	// Test #: 2 min value
	// Test Objective: Test Invalid Area ID
	// Input: id_area = Integer.MIN_VALUE
	// Expected Output: Exception Message - "Error - id cannot be below 1"

	public void testDelete002() {
		try {
			Area areaObj = new Area();
			areaObj.validateId_area(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (AreaExceptionHandler e) {
			assertEquals("Error: You cant enter a number below 1", e.getMessage());
		}
	}

	// Test #: 3 input value 0
	// Test Objective: Test Invalid Publication ID 0
	// Input: id_Area = 0
	// Expected Output: Exception Message - "Error - id area cannot be below 1"

	public void testDelete003() {
		try {
			Area areaObj = new Area();
			areaObj.validateId_area(0);
			fail("Exception expected");
		} catch (AreaExceptionHandler e) {
			assertEquals("Error: You cant enter a number below 1", e.getMessage());
		}
	}

	// Test #: 4 ACCEPT delete an area ID
	// Test Objective: To input a valid area ID
	// Inputs: id_area = 1
	// Expected Output: No Exception"

	public void testDelete4() {
		try {
			Area areaObj = new Area();
			areaObj.validateId_area(1);
		} catch (AreaExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 5 input value over 24
	// Test Objective: Test Invalid Area ID 25
	// Input: id_Area = 25
	// Expected Output: Exception Message - "Error - id area cannot be over25"

	public void testDelete5() {
		try {
			Area areaObj = new Area();
			areaObj.validateId_area(25);
			fail("Exception expected");
		} catch (AreaExceptionHandler e) {
			assertEquals("Error: The  area number is out of accepted range", e.getMessage());
		}
	}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

}
