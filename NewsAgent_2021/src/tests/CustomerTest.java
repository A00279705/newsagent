package tests;


import classes.Customer;
import exceptionHandler.CustomerExceptionHandler;
import junit.framework.TestCase;

public class CustomerTest extends TestCase {

	// Test #: 1
	// Test Objective: To create a Customer Account
	// Inputs: custID = 1, custFirstName = "John", custLastName = "Smith",
	// custAddress = "William street", custDeliveryArea = "2", custPhone
	// ="087-123123123"
	// Expected Output: Customer Object created with custFirstName = "John",
	// custLastName = "Smith", custStreet = "William street", custDeliveryArea =
	// "2",
	// custPhone = "087-123123123"

	public void testCustomer001() {

		Customer custObj = new Customer("John", "Smith", "William Street", "2", "087-123123123");

		// assertEquals(1, custObj.getId());
		assertEquals("John", custObj.getFirstName());
		assertEquals("Smith", custObj.getLastName());
		assertEquals("William Street", custObj.getAddress());
		assertEquals("2", custObj.getDeliveryArea());
		assertEquals("087-123123123", custObj.getPhone());
	}

	// Test #: 2
	// Test Objective:To catch an invalid customer first name
	// Inputs: custFirstName = "K"
	// Expected Output: Exception Message: "Customer First Name does not meet
	// minimum length requirements"

	public void testValidateFirstName001() {
		try {
			Customer newCust = new Customer();
			newCust.validateFirstName("K");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer First Name does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test #: 3
	// Test Objective:To catch an invalid customer first name
	// Inputs: custFirstName = "KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK"
	// Expected Output: Exception Message: "Customer First Name exceeds maximum
	// length requirements"

	public void testValidateFirstName002() {
		try {
			Customer newCust = new Customer();
			newCust.validateFirstName("KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer First Name exceeds maximum length requirements", e.getMessage());
		}
	}

	// Test #: 4
	// Test Objective:To catch an invalid customer first name
	// Inputs: custFirstName = ""
	// Expected Output: Exception Message: "Customer First Name NOT specified"

	public void testValidateFirstName003() {
		try {
			Customer newCust = new Customer();
			newCust.validateFirstName("");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer First Name NOT specified", e.getMessage());
		}
	}

	// Test #: 5
	// Test Objective:To catch an invalid customer last name
	// Inputs: custFirstName = "K"
	// Expected Output: Exception Message: "Customer Last Name does not meet minimum
	// length requirements"

	public void testValidateLastName001() {
		try {
			Customer newCust = new Customer();
			newCust.validateLastName("K");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer Last Name does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test #: 6
	// Test Objective:To catch an invalid customer last name
	// Inputs: custFirstName = "KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK"
	// Expected Output: Exception Message: "Customer Last Name exceeds maximum
	// length requirements"

	public void testValidateLastName002() {
		try {
			Customer newCust = new Customer();
			newCust.validateLastName("KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer Last Name exceeds maximum length requirements", e.getMessage());
		}
	}

	// Test #: 7
	// Test Objective:To catch an invalid customer last name
	// Inputs: custFirstName = ""
	// Expected Output: Exception Message: "Customer Last Name NOT specified"

	public void testValidateLastName003() {
		try {
			Customer newCust = new Customer();
			newCust.validateLastName("");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer Last Name NOT specified", e.getMessage());
		}
	}

	// Test #: 8
	// Test Objective:To catch an invalid Address
	// Inputs: custAddress = ""
	// Expected Output: Exception Message: "Address NOT specified"

	public void testvalidateAddress001() {
		try {
			Customer newCust = new Customer();
			newCust.validateAddress("");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Address NOT specified", e.getMessage());
		}
	}

	// Test #: 9
	// Test Objective:To catch an invalid Address
	// Inputs: custAddress = "k"
	// Expected Output: Exception Message: "Address does not meet minimum length
	// requirements"

	public void testvalidateAddress002() {
		try {
			Customer newCust = new Customer();
			newCust.validateAddress("k");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Address does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test #: 10
	// Test Objective:To catch an invalid customer last name
	// Inputs: custAddress = "KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK"
	// Expected Output: Exception Message: "Address exceeds maximum length
	// requirements"

	public void testvalidateAddress003() {
		try {
			Customer newCust = new Customer();
			newCust.validateAddress("KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Address exceeds maximum length requirements", e.getMessage());
		}
	}

	// Test #: 11
	// Test Objective:To catch an invalid customer phone number
	// Inputs: custFirstName = "087 123456789011111111"
	// Expected Output: Exception Message: "Address exceeds maximum length
	// requirements"

	public void testValidatePhoneNumber001() {
		try {
			Customer newCust = new Customer();
			newCust.validatePhone("087 123456789011111111");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Phone number exceeds maximum length requirements", e.getMessage());
		}
	}

	// Test #: 12
	// Test Objective:To catch an invalid customer phone number
	// Inputs: custFirstName = "087"
	// Expected Output: Exception Message: "Phone number does not meet minimum
	// length requirements"

	public void testValidatePhoneNumber002() {
		try {
			Customer newCust = new Customer();
			newCust.validatePhone("087");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Phone number does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test #: 13
	// Test Objective:To catch an invalid customer phone number
	// Inputs: custFirstName = ""
	// Expected Output: Exception Message: "Phone number NOT specified"

	public void testValidatePhoneNumber003() {
		try {
			Customer newCust = new Customer();
			newCust.validatePhone("");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Phone number NOT specified", e.getMessage());
		}
	}

	// Test #: 14
	// Test Objective:To catch an invalid customer Delivery Area
	// Inputs: custFirstName = ""
	// Expected Output: Exception Message: "Delivery Area NOT specified"

	public void testValidateDeliveryArea001() {
		try {

			Customer newCust = new Customer();
			newCust.validateDeliveryArea("");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Delivery Area NOT specified", e.getMessage());

		}
	}

	// Test #: 15
	// Test Objective:To catch an invalid customer Delivery Area
	// Inputs: custFirstName = "0"
	// Expected Output: Exception Message: "Delivery Area below range"

	public void testValidateDeliveryArea002() {
		try {
			Customer newCust = new Customer();
			newCust.validateDeliveryArea("0");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Delivery Area below range", e.getMessage());

		}
	}
	// Test #: 15
	// Test Objective:To catch an invalid customer Delivery Area
	// Inputs: custFirstName = "25"
	// Expected Output: Exception Message: "Delivery Area below range"

	public void testValidateDeliveryArea003() {
		try {
			Customer newCust = new Customer();
			newCust.validateDeliveryArea("25");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Delivery Area above range", e.getMessage());

		}
	}

}
