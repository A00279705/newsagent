package tests;

import classes.Order;
import exceptionHandler.OrderExceptionHandler;
import junit.framework.TestCase;

public class OrderTest extends TestCase {

	// Test #: 1
	// Test Objective: To create Order
	// Inputs: order_type = "daily", id_customer = 1
	// Expected Output: order_type = "daily", id_customer = 1
	public void testAOrder001() {
		try {
			Order ordObj = new Order("daily", 1);
			assertEquals("daily", ordObj.getOrder_type());
			assertEquals(1, ordObj.getId_customer());
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 1.5
	// Test Objective: To create Order
	// Inputs: order_type = "yearly", id_customer = 1
	// Expected Output: Exception Message: "Error: Failed to set Order"
	public void testAOrder002() {
		try {
			new Order("yearly", 1);
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: Failed to set Order", e.getMessage());
		}
	}

	// Test #: 2
	// Test Objective: To catch an invalid Order Type
	// Inputs: order_type = "yearly"
	// Expected Output: Exception Message: "Error: You can only Enter daily, weekly,
	// monthly"
	public void testValidateOrderType001() {
		try {
			Order ordObj = new Order();
			ordObj.validateOrder_type("yearly");
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You can only Enter daily, weekly, monthly", e.getMessage());
		}
	}

	// Test #: 3
	// Test Objective: To input a valid Order Type
	// Inputs: order_type = "daily"
	// Expected Output: No Exception"
	public void testValidateOrderType002() {
		try {
			Order ordObj = new Order();
			ordObj.validateOrder_type("daily");
		} catch (OrderExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 4
	// Test Objective: To input a valid Order Type
	// Inputs: order_type = "weekly"
	// Expected Output: No Exception"
	public void testValidateOrderType003() {
		try {
			Order ordObj = new Order();
			ordObj.validateOrder_type("weekly");
		} catch (OrderExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 5
	// Test Objective: To input a valid Order Type
	// Inputs: order_type = "monthly"
	// Expected Output: No Exception"
	public void testValidateOrderType004() {
		try {
			Order ordObj = new Order();
			ordObj.validateOrder_type("monthly");
		} catch (OrderExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 5
	// Test Objective: To input a valid Order Type
	// Inputs: order_type = "monthly"
	// Expected Output: No Exception"
	public void testValidateOrderType005() {
		try {
			Order ordObj = new Order();
			ordObj.validateOrder_type("");
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You can only Enter daily, weekly, monthly", e.getMessage());
		}
	}

	public void testValidateOrderType006() {
		try {
			Order ordObj = new Order();
			ordObj.validateOrder_type(" ");
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You can only Enter daily, weekly, monthly", e.getMessage());
		}
	}

	// Test #: 6
	// Test Objective:To catch an invalid Order Customer ID
	// Inputs: id_customer = Integer.MIN_VALUE
	// Expected Output: Error: You cant enter a number bellow 1"
	public void testValidateCustomerID001() {
		try {
			Order ordObj = new Order();
			ordObj.validateID_customer(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You cant enter a number bellow 1", e.getMessage());
		}
	}

	// Test #: 7
	// Test Objective:To catch an invalid Order Customer ID
	// Inputs: id_customer = 0
	// Expected Output: Error: You cant enter a number bellow 1"
	public void testValidateCustomerID002() {
		try {
			Order ordObj = new Order();
			ordObj.validateID_customer(0);
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You cant enter a number bellow 1", e.getMessage());
		}
	}

	// Test #: 8
	// Test Objective: To input a valid Customer ID
	// Inputs: id_customer = 1
	// Expected Output: No Exception"
	public void testValidateCustomerID003() {
		try {
			Order ordObj = new Order();
			ordObj.validateID_customer(1);
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 9
	// Test Objective: To input a valid Customer ID
	// Inputs: id_customer = Integer.MAX_VALUE
	// Expected Output: No Exception"
	public void testValidateCustomerID004() {
		try {
			Order ordObj = new Order();
			ordObj.validateID_customer(Integer.MAX_VALUE);
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 10
	// Test Objective:To catch an invalid Order Order ID For Delete
	// Inputs: id_customer = 0
	// Expected Output: Error: You cant enter a number bellow 1"
	public void testValidateOrderID001() {
		try {
			Order ordObj = new Order();
			ordObj.validateID_order(0);
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You cant enter a number bellow 1", e.getMessage());
		}
	}

	// Test #: 11
	// Test Objective:To catch an invalid Order Order ID For Delete
	// Inputs: id_customer = Integer.MIN_VALUE
	// Expected Output: Error: You cant enter a number bellow 1"
	public void testValidateOrderID002() {
		try {
			Order ordObj = new Order();
			ordObj.validateID_order(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You cant enter a number bellow 1", e.getMessage());
		}
	}

	// Test #: 12
	// Test Objective: To input a valid Customer ID
	// Inputs: id_customer = 1
	// Expected Output: id_customer = 1
	public void testValidateOrderID003() {
		try {
			Order ordObj = new Order();
			ordObj.validateID_order(1);
			assertEquals(1, ordObj.getId_order());
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 13
	// Test Objective: To input a valid Customer ID
	// Inputs: id_customer = Integer.MAX_VALUE
	// Expected Output: id_customer = Integer.MAX_VALUE
	public void testValidateOrderID004() {
		try {
			Order ordObj = new Order();
			ordObj.validateID_order(Integer.MAX_VALUE);
			assertEquals(Integer.MAX_VALUE, ordObj.getId_order());
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 14
	// Test Objective: To catch an invalid Order Type
	// Inputs: order_type = "yearly"
	// Expected Output: Exception Message: "Error: You can only Enter daily, weekly,
	// monthly"
	public void testValidateOrderTypeUpdate001() {
		try {
			Order ordObj = new Order();
			ordObj.validateOrder_UpdateType("yearly");
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You can only Enter daily, weekly, monthly", e.getMessage());
		}
	}

	// Test #: 15
	// Test Objective: To input a valid Order Type for an update
	// Inputs: order_type = "daily"
	// Expected Output: order_type = daily
	public void testValidateOrderTypeUpdate002() {
		try {
			Order ordObj = new Order();
			ordObj.validateOrder_UpdateType("daily");
			assertEquals("daily", ordObj.getOrder_type());
		} catch (OrderExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 16
	// Test Objective: To input a valid Order Type for an update
	// Inputs: order_type = "weekly"
	// Expected Output: order_type = weekly
	public void testValidateOrderTypeUpdate003() {
		try {
			Order ordObj = new Order();
			ordObj.validateOrder_UpdateType("weekly");
			assertEquals("weekly", ordObj.getOrder_type());
		} catch (OrderExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 17
	// Test Objective: To input a valid Order Type for an update
	// Inputs: order_type = "monthly"
	// Expected Output: order_type = monthly
	public void testValidateOrderTypeUpdate004() {
		try {
			Order ordObj = new Order();
			ordObj.validateOrder_UpdateType("monthly");
			assertEquals("monthly", ordObj.getOrder_type());
		} catch (OrderExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 1
	// Test Objective: To input a valid Order Type for an update
	// Inputs: order_type = "monthly"
	// Expected Output: order_type = monthly
	public void testValidateOrderTypeUpdate005() {
		try {
			Order ordObj = new Order();
			ordObj.validateOrder_UpdateType("");
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You can only Enter daily, weekly, monthly", e.getMessage());
		}
	}

	public void testValidateOrderTypeUpdate006() {
		try {
			Order ordObj = new Order();
			ordObj.validateOrder_UpdateType(" ");
			fail("Exception expected");
		} catch (OrderExceptionHandler e) {
			assertEquals("Error: You can only Enter daily, weekly, monthly", e.getMessage());
		}
	}

}
