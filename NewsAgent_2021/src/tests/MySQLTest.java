package tests;

import java.sql.SQLException;

import classes.Area;
import classes.Customer;
import classes.Driver;
import classes.Order;
import classes.OrdersToPublication;
import classes.Publication;
import exceptionHandler.AreaExceptionHandler;
import exceptionHandler.CustomerExceptionHandler;
import exceptionHandler.DriversExceptionHandler;
import exceptionHandler.OrderExceptionHandler;
import exceptionHandler.PublicationExceptionHandler;
import junit.framework.TestCase;
import run.MySQL;

public class MySQLTest extends TestCase {

//<-------------------------CREATE------------------------->	
	
	// Test #: 1 Area
	// Test Objective: To initialise the jdbc
	// Inputs: MySQL sqlObj = new MySQL();
	// Expected Output: created sqlObj object
	public void testValidateConstructor01() {
		try {
			MySQL sqlObj = new MySQL();
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 2 Area
	// Test Objective: To execute a SQL Update on Insert Area
	// Inputs: new Area(1, 1);
	// Expected Output: true
	public void testValidateInsert01() {
		try {
			MySQL sqlObj = new MySQL();
			Area area = new Area(1, 1);
			assertEquals(true, sqlObj.insertArea(area));
		} catch (SQLException e) {
			fail("Exception not expected");
		} catch (AreaExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 3 Area
	// Test Objective: To execute a SQL generate a Delivery Area
	// Expected Output: true

	public void testValidateGenerateDeliveryAreaReport() {
		try {
			MySQL sqlObj = new MySQL();
			Order orderObj = new Order();
			sqlObj.generateDeliveryAreaReport(orderObj);
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 3
	// Test Objective: To execute a SQL Update on Insert Orders
	// Inputs: new Order("daily", 1);
	// Expected Output: true
	public void testValidateInsert02() {
		try {
			MySQL sqlObj = new MySQL();
			Order order = new Order("daily", 1);
			assertEquals(true, sqlObj.insertOrders(order));
		} catch (SQLException e) {
			fail("Exception not expected");
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 3.5
	// Test Objective: To execute a SQL Update on Insert Orders
	// Inputs: new Order("daily", 1);
	// Expected Output: true
	public void testValidateInsert023() {
		try {
			MySQL sqlObj = new MySQL();
			Order order = new Order("daily", 1);
			OrdersToPublication ordtopub = new OrdersToPublication(1, 1);
			assertEquals(true, sqlObj.insertOrders1(order, ordtopub));
		} catch (SQLException e) {
			fail("Exception not expected");
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 4
	// Test Objective: To execute a SQL Update on Insert Ord To Pub
	// Inputs: new OrdersToPublication(1, 1);
	// Expected Output: true
	public void testValidateInsert03() {
		try {
			MySQL sqlObj = new MySQL();
			OrdersToPublication ordtopub = new OrdersToPublication(1, 1);
			assertEquals(true, sqlObj.insertOrdPub(ordtopub));
		} catch (SQLException e) {
			fail("Exception not expected");
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 5
	// Test Objective: To execute a SQL Update on Customer
	// Inputs: new Customer(1, 1);
	// Expected Output: true
	public void testValidateInsert04() {
		try {
			MySQL sqlObj = new MySQL();
			Customer ordtopub = new Customer("aa", "aa", "aa", "aa", "aa");
			assertEquals(true, sqlObj.insertCustomer(ordtopub));
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 6
	// Test Objective: To execute a SQL Update on Insert Publication
	// Inputs: new Publication("book B", 1,1);
	// Expected Output: true
	public void testValidateInsert05() {
		try {
			MySQL sqlObj = new MySQL();
			Publication ordtopub = new Publication("book B", 1, 1);
			assertEquals(true, sqlObj.insertPublication(ordtopub));
		} catch (SQLException e) {
			fail("Exception not expected");
		} catch (PublicationExceptionHandler e) {
			fail("Exception not expected");
		}

	}

//<-------------------------READ------------------------->
	// Test #: 1
	// Test Objective: To execute a SQL read on Aread
	// Inputs: sqlObj.printArea();
	// Expected Output: true
	public void testValidateRead01() {
		try {
			MySQL sqlObj = new MySQL();
			sqlObj.printArea();
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 2
	// Test Objective: To execute a SQL read on Ord To PUb
	// Inputs: sqlObj.printAllOrdersToPublication();
	// Expected Output: true
	public void testValidateRead02() {
		try {
			MySQL sqlObj = new MySQL();
			sqlObj.printAllOrdersToPublication();
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 3
	// Test Objective: To execute a SQL read on orders
	// Inputs: sqlObj.printAllOrders();
	// Expected Output: true
	public void testValidateRead03() {
		try {
			MySQL sqlObj = new MySQL();
			sqlObj.printAllOrders();
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 4
	// Test Objective: To execute a SQL read on Customer
	// Inputs: sqlObj.printAllCustomers();
	// Expected Output: true
	public void testValidateRead04() {
		try {
			MySQL sqlObj = new MySQL();
			sqlObj.printAllCustomers();
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 5
	// Test Objective: To execute a SQL read on Publication
	// Inputs: sqlObj.printAllPublications();
	// Expected Output: true
	public void testValidateRead05() {
		try {
			MySQL sqlObj = new MySQL();
			sqlObj.printAllPublications();
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

//<-------------------------UPDATE------------------------->

	// Test #: 1
	// Test Objective: To execute a SQL update on Order
	// Inputs: sqlObj.updateOrders(order)
	// Expected Output: true
	public void testValidateUpdate01() {
		try {
			MySQL sqlObj = new MySQL();
			Order order = new Order();
			order.validateID_order(1);
			order.validateOrder_UpdateType("daily");
			assertEquals(true, sqlObj.updateOrders(order));
		} catch (SQLException e) {
			fail("Exception not expected");
		} catch (OrderExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 2
	// Test Objective: To execute a SQL update on Publication
	// Inputs: sqlObj.updatePublication(pubObj)
	// Expected Output: true
	public void testValidateUpdate02() {
		try {
			MySQL sqlObj = new MySQL();
			Publication pubObj = new Publication();
			pubObj.validateId_publication(1);
			pubObj.validateBook_name("ffff");
			pubObj.validateBook_price(100);
			pubObj.validateBook_amount(100);
			assertEquals(true, sqlObj.updatePublication(pubObj));
		} catch (SQLException e) {
			fail("Exception not expected");
		} catch (PublicationExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 3
	// Test Objective: To execute a SQL update on Customer
	// Inputs: sqlObj.updateCustomer(updateCust)
	// Expected Output: true
	public void testValidateUpdate03() {
		try {
			MySQL sqlObj = new MySQL();
			Customer updateCust = new Customer();
			updateCust.validateCustomer_UpdateFirstName("aaa");
			updateCust.validateCustomer_UpdateLastName("aaa");
			updateCust.validateCustomer_UpdateAddress("aaa");
			updateCust.validateCustomer_UpdateDeliveryArea("22");
			updateCust.validateCustomer_UpdatePhone("0855555550");
			updateCust.validateID_customer(5);
			assertEquals(true, sqlObj.updateCustomer(updateCust));
		} catch (SQLException e) {
			fail("Exception not expected");
		} catch (CustomerExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 4
	// Test Objective: To execute a SQL update on Driver
	// Inputs: sqlObj.updateOrders(order)
	// Expected Output: true
	public void testValidateUpdate04() {
		try {
			MySQL sqlObj = new MySQL();
			Driver driver = new Driver();
			driver.setName("DriverA");
			driver.setPassword("password");
			driver.setId_driver(1);
			assertEquals(true, sqlObj.updateDriver(driver));
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

//<-------------------------DELETE------------------------->
	// Test #: 1
	// Test Objective: To execute a SQL delete on Orders
	// Inputs: sqlObj.deleteOrders(10)
	// Expected Output: true
	public void testValidateDelete01() {
		try {
			MySQL sqlObj = new MySQL();
			assertEquals(true, sqlObj.deleteOrders(10));
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 2
	// Test Objective: To execute a SQL delete on Ord To Pub
	// Inputs: sqlObj.deleteOrderToPublication(10)
	// Expected Output: true
	public void testValidateDelete02() {
		try {
			MySQL sqlObj = new MySQL();
			assertEquals(true, sqlObj.deleteOrderToPublication(10));
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 3
	// Test Objective: To execute a SQL delete on Customer
	// Inputs: sqlObj.deleteCustomer(10)
	// Expected Output: true
	public void testValidateDelete03() {
		try {
			MySQL sqlObj = new MySQL();
			assertEquals(true, sqlObj.deleteCustomer(7));
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 4
	// Test Objective: To execute a SQL delete on Publication
	// Inputs: sqlObj.deletePublications(10)
	// Expected Output: true
	public void testValidateDelete04() {
		try {
			MySQL sqlObj = new MySQL();
			assertEquals(true, sqlObj.deletePublications(10));
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 5
	// Test Objective: To execute a SQL delete on Driver
	// Inputs: sqlObj.deleteDriver(3)
	// Expected Output: true
	public void testValidateDelete05() {
		try {
			MySQL sqlObj = new MySQL();
			Driver driverObj = new Driver();
			driverObj.setId_driver(3);
			assertEquals(true, sqlObj.deleteDriver(driverObj));
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 6
	// Test Objective: To execute a SQL delete on Invoice
	// Inputs: sqlObj.deleteInvoice(1)
	// Expected Output: true
	public void testValidateDelete06() {
		try {
			MySQL sqlObj = new MySQL();
			assertEquals(true, sqlObj.deleteInvoice(9));
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 7
	// Test Objective: To execute a SQL delete on Area
	// Inputs: sqlObj.deleteInvoice(1)
	// Expected Output: true
	public void testValidateDelete07() {
		try {
			MySQL sqlObj = new MySQL();
			assertEquals(true, sqlObj.deleteArea(9));
		} catch (SQLException e) {
			fail("Exception not expected");
		}
	}

//<-------------------------  ------------------------->

	// Test #: 1
	// Test Objective: To execute a SQL increment on Publication
	// Inputs: sqlObj.validateId_publication(10)
	// Expected Output: true
	public void testValidatePubInc01() {
		try {
			MySQL sqlObj = new MySQL();
			Publication ordtopub = new Publication();
			ordtopub.validateId_publication(10);
			assertEquals(true, sqlObj.incrementPublication(ordtopub));
		} catch (SQLException e) {
			fail("Exception not expected");
		} catch (PublicationExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test #: 2
	// Test Objective: To execute a SQL decrement on Publication
	// Inputs: sqlObj.validateId_publication(10)
	// Expected Output: true
	public void testValidatePubInc02() {
		try {
			MySQL sqlObj = new MySQL();
			Publication ordtopub = new Publication();
			ordtopub.validateId_publication(10);
			assertEquals(true, sqlObj.decrementPublication(ordtopub));
		} catch (SQLException e) {
			fail("Exception not expected");
		} catch (PublicationExceptionHandler e) {
			fail("Exception not expected");
		}
	}

//<-------------------------LOG IN ------------------------->
	// Test #: 1
	// Test Objective: To successfully log in a driver
	// Inputs: Driver("Driver2","password");
	// Expected Output: true
	public void testDriverLogin01() {
		try {
			MySQL sqlObj = new MySQL();
			Driver driverObj = new Driver("Driver2", "password");
			assertEquals(true, sqlObj.DriverLogin(driverObj));
		} catch (SQLException e) {
			fail("Exception not expected");
		} catch (DriversExceptionHandler e) {
			fail("Exception not expected");
		}

	}

	// Test #: 2
	// Test Objective: To attempt to log in a driver with an invalid name
	// Inputs: Driver("SFGDF","12345678");
	// Expected Output: false
	public void testDriverLogin02() {
		try {
			MySQL sqlObj = new MySQL();
			Driver driverObj = new Driver("SFGDF", "12345678");
			assertEquals(false, sqlObj.DriverLogin(driverObj));
		} catch (SQLException e) {
			fail("Exception not expected");
		} catch (DriversExceptionHandler e) {
			fail("Exception not expected");
		}

	}

}
