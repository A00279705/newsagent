package tests;
import classes.DeliveryDocket;
import exceptionHandler.DeliveryDocketException;
import junit.framework.TestCase;

public class DeliveryDocketTest extends TestCase {

	// Test #: 0
	// Test Objective: To create Docket
	// Inputs: driverID = 1, type = daily, area = 1
	// Expected Output: driverID = 1, type = daily, area = 1
	public void testValidateDocket01() {
		try {
			DeliveryDocket dd = new DeliveryDocket(1, 1, "daily");
			assertEquals("daily", dd.getType());
			assertEquals(1, dd.getDriverID());
			assertEquals(1, dd.getArea());
		} catch (DeliveryDocketException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 1
	// Test Objective: To create Docket
	// Inputs: driverID = 0, type = daily, area = 1
	// Expected Output: Error: Failed to set DeliveryDocket
	public void testValidateDocket02() {
		try {
			DeliveryDocket dd = new DeliveryDocket(0, 1, "daily");
		} catch (DeliveryDocketException e) {
			assertEquals("Error: Failed to set DeliveryDocket", e.getMessage());
		}
	}

//<------------------------DATE------------------------>

	// Test #: 1
	// Test Objective: To input a valid date
	// Inputs: date = 2021-02-27
	// Expected Output: 2021-02-27 = 2021-02-27
	public void testValidateDate01() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.dvalidateDate("2021-02-27");
			assertEquals("2021-02-27", dd.getDate());
		} catch (DeliveryDocketException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 2
	// Test Objective: To input a invalid day
	// Inputs: date = 2021-02-40
	// Expected Output: Error: date is out of range
	public void testValidateDate02() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.dvalidateDate("2021-02-40");
			fail("Exception expected");
		} catch (DeliveryDocketException e) {
			assertEquals("Error: date is out of range", e.getMessage());
		}
	}

	// Test #: 3
	// Test Objective: To input a invalid month
	// Inputs: date = 2021-13-16
	// Expected Output: Error: date is out of range
	public void testValidateDate03() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.dvalidateDate("2021-13-16");
			fail("Exception expected");
		} catch (DeliveryDocketException e) {
			assertEquals("Error: date is out of range", e.getMessage());
		}
	}

	// Test #: 4
	// Test Objective: To input a invalid year
	// Inputs: date = A-13-16
	// Expected Output: Error: date is out of range
	public void testValidateDate04() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.dvalidateDate("A-12-16");
			fail("Exception expected");
		} catch (DeliveryDocketException e) {
			assertEquals("Error: date is out of range", e.getMessage());
		}
	}

//<------------------------DRIVER ID------------------------>

	// Test #: 1
	// Test Objective: To input a invalid DRIVER ID
	// Inputs: driverID = Integer.MIN_VALUE
	// Expected Output: Error: You cant enter a number greater than 0
	public void testValidateDriverID01() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.validateID_driver(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (DeliveryDocketException e) {
			assertEquals("Error: You cant enter a number greater than 0", e.getMessage());
		}
	}

	// Test #: 2
	// Test Objective: To input a invalid DRIVER ID
	// Inputs: driverID = 0
	// Expected Output: Error: You cant enter a number greater than 0
	public void testValidateDriverID02() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.validateID_driver(0);
			fail("Exception expected");
		} catch (DeliveryDocketException e) {
			assertEquals("Error: You cant enter a number greater than 0", e.getMessage());
		}
	}

	// Test #: 3
	// Test Objective: To input a valid DRIVER ID
	// Inputs: driverID = 1
	// Expected Output: No Exception
	public void testValidateDriverID03() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.validateID_driver(1);
		} catch (DeliveryDocketException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 4
	// Test Objective: To input a valid DRIVER ID
	// Inputs: driverID = Integer.MAX_VALUE
	// Expected Output: No Exception
	public void testValidateDriverID04() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.validateID_driver(Integer.MAX_VALUE);
		} catch (DeliveryDocketException e) {
			fail("Exception not expected");
		}
	}

//<------------------------TYPE------------------------>

	// Test #: 1
	// Test Objective: To catch an invalid Order Type
	// Inputs: order_type = "yearly"
	// Expected Output: Exception Message: "Error: You can only Enter daily, weekly,
	// monthly"
	public void testValidateOrderType001() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.validateOrder_type("yearly");
			fail("Exception expected");
		} catch (DeliveryDocketException e) {
			assertEquals("Error: You can only Enter daily, weekly, monthly", e.getMessage());
		}
	}

	// Test #: 2
	// Test Objective: To input a valid Order Type
	// Inputs: order_type = "daily"
	// Expected Output: No Exception"
	public void testValidateOrderType002() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.validateOrder_type("daily");
		} catch (DeliveryDocketException e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 3
	// Test Objective: To input a valid Order Type
	// Inputs: order_type = "weekly"
	// Expected Output: No Exception"
	public void testValidateOrderType003() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.validateOrder_type("weekly");
		} catch (DeliveryDocketException e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 4
	// Test Objective: To input a valid Order Type
	// Inputs: order_type = "monthly"
	// Expected Output: No Exception"
	public void testValidateOrderType004() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.validateOrder_type("monthly");
		} catch (DeliveryDocketException e) {
			fail("Exception Not expected");
		}
	}

//<------------------------AREA------------------------>

	// Test #: 1
	// Test Objective: Test Invalid Area ID
	// Input: area = Integer.MIN_VALUE
	// Expected Output: Exception Message - "Error - id cannot be below 1"
	public void testValidateArea001() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.validate_area(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (DeliveryDocketException e) {
			assertEquals("Error: You cant enter a number below 1", e.getMessage());
		}
	}

	// Test #: 2
	// Test Objective: Test Invalid Publication ID 0
	// Input: area = 0
	// Expected Output: Error: You cant enter a number below 1
	public void testValidateArea002() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.validate_area(0);
			fail("Exception expected");
		} catch (DeliveryDocketException e) {
			assertEquals("Error: You cant enter a number below 1", e.getMessage());
		}
	}

	// Test #: 3
	// Test Objective: To input a valid area ID
	// Inputs: area = 1
	// Expected Output: No Exception
	public void testValidateArea003() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.validate_area(1);
		} catch (DeliveryDocketException e) {
			fail("Exception not expected");
		}
	}

	// Test #: 4
	// Test Objective: Test Invalid Area ID 25
	// Input: area = 25
	// Expected Output: Error: The number is out of range
	public void testValidateArea004() {
		try {
			DeliveryDocket dd = new DeliveryDocket();
			dd.validate_area(25);
			fail("Exception expected");
		} catch (DeliveryDocketException e) {
			assertEquals("Error: The number is out of range", e.getMessage());
		}
	}
}
