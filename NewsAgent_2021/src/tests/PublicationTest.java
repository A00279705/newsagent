package tests;
import classes.Publication;
import exceptionHandler.PublicationExceptionHandler;
import junit.framework.TestCase;

public class PublicationTest extends TestCase {

	// ---------------------------CREATE PUBLICATION------------------------------//

	// Test #: 1
	// Test Objective: Create Publication
	// Input: book_name = "Book7" , book_price = 300 , book_amount = 500
	// Expected Output: Publication created

	public void testPublication001() {
		try {
			Publication pubObj = new Publication("Book7", 300, 500);
			assertEquals("Book7", pubObj.getBook_name());
			assertEquals(300, pubObj.getBook_price());
			assertEquals(500, pubObj.getBook_amount());
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 2
	// Test Objective: Test Valid Book Name with special character
	// Input: book name = Harry Potter & the Order of the Phoenix
	// Expected Output: No Exception

	public void testCreate001() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_name("Harry Potter & the Order of the Phoenix");
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 3
	// Test Objective: Test Book Name with 50 characters
	// Input: book name = My Grandmother Asked Me to Tell You She�s Sorry 22
	// Expected Output: No Exception"

	public void testCreate002() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_name("My Grandmother Asked Me to Tell You She�s Sorry 22");
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 4
	// Test Objective: Test Book Name with 1 characters
	// Input: book name = a
	// Expected Output: No Exception"

	public void testCreate003() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_name("a");
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 5
	// Test Objective: Test Book Name with no input
	// Input: book name =
	// Expected Output: Exception Message - "Error - name cannot be empty"

	public void testCreate004() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_name("");
			fail("Exception expected");
		} catch (PublicationExceptionHandler e) {
			assertEquals("Error - name cannot be empty", e.getMessage());
		}
	}

	// Test #: 6
	// Test Objective: Test Invalid Book Price
	// Input: book_price = Integer.MIN_VALUE
	// Expected Output: Exception Message - "Error - book price cannot be below 0"

	public void testCreate005() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_price(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (PublicationExceptionHandler e) {
			assertEquals("Error - book price cannot be below 0", e.getMessage());
		}
	}

	// Test #: 7
	// Test Objective: Test Valid Book Price
	// Input: book_price = 0
	// Expected Output: No Exception

	public void testCreate006() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_price(0);
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 8
	// Test Objective: Test Valid Book Price
	// Input: book_price = Integer.MAX_VALUE
	// Expected Output: No Exception

	public void testCreate007() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_price(Integer.MAX_VALUE);
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 9
	// Test Objective: Test Invalid Book Amount
	// Input: book_amount = Integer.MIN_VALUE
	// Expected Output: Exception Message - "Error - book amount cannot be below 0"

	public void testCreate008() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_amount(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (PublicationExceptionHandler e) {
			assertEquals("Error - book amount cannot be below 0", e.getMessage());
		}
	}

	// Test #: 10
	// Test Objective: Test Valid Book Amount
	// Input: book_amount = Integer.MAX_VALUE
	// Expected Output: No Exception

	public void testCreate009() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_amount(Integer.MAX_VALUE);
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 11
	// Test Objective: Test Valid Book Price
	// Input: book_amount = 0
	// Expected Output: No Exception

	public void testCreate010() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_price(0);
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// ---------------------------UPDATE PUBLICATION------------------------------//

	// Test #: 12
	// Test Objective: Test Valid Publication ID
	// Input: id_publication = Integer.MAX_VALUE
	// Expected Output: No Exception

	public void testUpdate001() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateId_publication(Integer.MAX_VALUE);
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 13
	// Test Objective: Test Invalid Publication ID
	// Input: id_publication = Integer.MIN_VALUE
	// Expected Output: Exception Message - "Error - id cannot be below 1"

	public void testUpdate002() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateId_publication(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (PublicationExceptionHandler e) {
			assertEquals("Error - id cannot be below 1", e.getMessage());
		}
	}

	// Test #: 14
	// Test Objective: Test Invalid Publication ID
	// Input: id_publication = 0
	// Expected Output: Exception Message - "Error - id cannot be below 1"

	public void testUpdate003() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateId_publication(0);
			fail("Exception expected");
		} catch (PublicationExceptionHandler e) {
			assertEquals("Error - id cannot be below 1", e.getMessage());
		}
	}

	// Test #: 15
	// Test Objective: Test Valid Book Name with special character
	// Input: book name = Harry Potter & the Order of the Phoenix
	// Expected Output: No Exception

	public void testUpdate004() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_name("Harry Potter & the Order of the Phoenix");
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 16
	// Test Objective: Test Book Name with 50 characters
	// Input: book name = My Grandmother Asked Me to Tell You She�s Sorry 22
	// Expected Output: Exception Message - "Error - Book length cannot be more than
	// 50 characters"

	public void testUpdate005() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_name("My Grandmother Asked Me to Tell You She�s Sorry 22");
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 17
	// Test Objective: Test Book Name with 1 characters
	// Input: book name = a
	// Expected Output: No Exception"

	public void testUpdate006() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_name("a");
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 18
	// Test Objective: Test Book Name with no input
	// Input: book name =
	// Expected Output: Exception Message - "Error - name cannot be empty"

	public void testUpdate007() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_name("");
			fail("Exception expected");
		} catch (PublicationExceptionHandler e) {
			assertEquals("Error - name cannot be empty", e.getMessage());
		}
	}

	// Test #: 19
	// Test Objective: Test Invalid Book Price
	// Input: book_price = Integer.MIN_VALUE
	// Expected Output: Exception Message - "Error - book price cannot be below 0"

	public void testUpdate008() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_price(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (PublicationExceptionHandler e) {
			assertEquals("Error - book price cannot be below 0", e.getMessage());
		}
	}

	// Test #: 20
	// Test Objective: Test Valid Book Price
	// Input: book_price = 0
	// Expected Output: No Exception

	public void testUpdate009() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_price(0);
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 21
	// Test Objective: Test Valid Book Price
	// Input: book_price = Integer.MAX_VALUE
	// Expected Output: No Exception

	public void testUpdate010() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_price(Integer.MAX_VALUE);
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 22
	// Test Objective: Test Invalid Book Amount
	// Input: book_amount = Integer.MIN_VALUE
	// Expected Output: Exception Message - "Error - book amount cannot be below 0"

	public void testUpdate0011() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_amount(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (PublicationExceptionHandler e) {
			assertEquals("Error - book amount cannot be below 0", e.getMessage());
		}
	}

	// Test #: 23
	// Test Objective: Test Valid Book Amount
	// Input: book_amount = Integer.MAX_VALUE
	// Expected Output: No Exception

	public void testUpdate012() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_amount(Integer.MAX_VALUE);
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 24
	// Test Objective: Test Valid Book Price
	// Input: book_amount = 0
	// Expected Output: No Exception

	public void testUpdate013() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateBook_price(0);
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// ---------------------------DELETE PUBLICATION------------------------------//

	// Test #: 25
	// Test Objective: Test Valid Publication ID
	// Input: id_publication = Integer.MAX_VALUE
	// Expected Output: No Exception

	public void testDelete001() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateId_publication(Integer.MAX_VALUE);
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 26
	// Test Objective: Test Invalid Publication ID
	// Input: id_publication = Integer.MIN_VALUE
	// Expected Output: Exception Message - "Error - id cannot be below 1"

	public void testDelete002() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateId_publication(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (PublicationExceptionHandler e) {
			assertEquals("Error - id cannot be below 1", e.getMessage());
		}
	}

	// Test #: 27
	// Test Objective: Test Invalid Publication ID
	// Input: id_publication = 0
	// Expected Output: Exception Message - "Error - id cannot be below 1"

	public void testDelete003() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateId_publication(0);
			fail("Exception expected");
		} catch (PublicationExceptionHandler e) {
			assertEquals("Error - id cannot be below 1", e.getMessage());
		}
	}

	// ---------------------------INCREMENT PUBLICATION
	// STOCK------------------------------//

	// Test #: 28
	// Test Objective: Test valid increment operation
	// Input: id_publication = Integer.MAX_VALUE
	// Expected Output: No Exception

	public void testIncrement001() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateId_publication(Integer.MAX_VALUE);
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 29
	// Test Objective: Test Invalid increment operation
	// Input: id_publication = 0
	// Expected Output: Exception Message - "Error - id cannot be below 1"

	public void testIncrement002() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateId_publication(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (PublicationExceptionHandler e) {
			assertEquals("Error - id cannot be below 1", e.getMessage());
		}
	}

	// Test #: 30
	// Test Objective: Test Invalid increment operation
	// Input: id_publication = 0
	// Expected Output: Exception Message - "Error - id cannot be below 1"

	public void testIncrement003() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateId_publication(0);
			fail("Exception expected");
		} catch (PublicationExceptionHandler e) {
			assertEquals("Error - id cannot be below 1", e.getMessage());
		}
	}

	// ---------------------------DECREMENT PUBLICATION
	// STOCK------------------------------//

	// Test #: 31
	// Test Objective: Test valid decrement operation
	// Input: id_publication = Integer.MAX_VALUE
	// Expected Output: No Exception

	public void testDecrement001() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateId_publication(Integer.MAX_VALUE);
		} catch (PublicationExceptionHandler e) {
			fail("Exception Not expected");
		}
	}

	// Test #: 32
	// Test Objective: Test Invalid decrement operation
	// Input: id_publication = Integer.MIN_VALUE
	// Expected Output: Exception Message - "Error - id cannot be below 1"

	public void testDecrement002() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateId_publication(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (PublicationExceptionHandler e) {
			assertEquals("Error - id cannot be below 1", e.getMessage());
		}
	}

	// Test #: 33
	// Test Objective: Test Invalid decrement operation
	// Input: id_publication = 0
	// Expected Output: Exception Message - "Error - id cannot be below 1"

	public void testDecrement003() {
		try {
			Publication pubObj = new Publication();
			pubObj.validateId_publication(0);
			fail("Exception expected");
		} catch (PublicationExceptionHandler e) {
			assertEquals("Error - id cannot be below 1", e.getMessage());
		}
	}
}
