package exceptionHandler;
public class AreaExceptionHandler extends Exception {

	String message;

	public AreaExceptionHandler(String reply) {
		message = reply;
	}

	public String getMessage() {
		return message;
	}
}
