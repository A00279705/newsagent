package exceptionHandler;

public class InvoiceExceptionHandler extends Exception {

	String message;

	public InvoiceExceptionHandler(String reply) {
		message = reply;
	}

	public String getMessage() {
		return message;
	}
}
