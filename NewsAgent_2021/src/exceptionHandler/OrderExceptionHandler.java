package exceptionHandler;
public class OrderExceptionHandler extends Exception {

	String message;

	public OrderExceptionHandler(String reply) {
		message = reply;
	}

	public String getMessage() {
		return message;
	}
}
