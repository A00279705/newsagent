package exceptionHandler;


public class DeliveryDocketException extends Exception {

	String message;

	public DeliveryDocketException(String errMessage) {
		message = errMessage;
	}

	@Override
	public String getMessage() {
		return message;
	}

}
