package classes;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import exceptionHandler.DeliveryDocketException;

public class DeliveryDocket {

	private int driverID;
	private int area;
	private String date;
	private String type;

	public DeliveryDocket() {

	}

	public DeliveryDocket(int driverID, int area, String type) throws DeliveryDocketException {
		try {
			validateID_driver(driverID);
			validate_area(area);
			validateOrder_type(type);
		} catch (DeliveryDocketException e) {
			throw new DeliveryDocketException("Error: Failed to set DeliveryDocket");
		}
		setDriverID(driverID);
		setArea(area);
		setType(type);
	}

	public int getDriverID() {
		return driverID;
	}

	public void setDriverID(int driverID) {
		this.driverID = driverID;
	}

	public int getArea() {
		return area;
	}

	public void setArea(int area) {
		this.area = area;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	// validate input driver ID
	public void validateID_driver(int driverID) throws DeliveryDocketException {
		if (driverID < 1)
			throw new DeliveryDocketException("Error: You cant enter a number greater than 0");
		// this.driverID = driverID;
	}

	// validate input TYPE for CREATE ORDER
	public void validateOrder_type(String order_type) throws DeliveryDocketException {
		if (!order_type.equals("daily") && !order_type.equals("weekly") && !order_type.equals("monthly"))
			throw new DeliveryDocketException("Error: You can only Enter daily, weekly, monthly");
	}

	public void validate_area(int area) throws DeliveryDocketException {
		if (area <= 0)
			throw new DeliveryDocketException("Error: You cant enter a number below 1");
		else if (area > 24)
			throw new DeliveryDocketException("Error: The number is out of range");
		setArea(area);
	}

	public void dvalidateDate(String date) throws DeliveryDocketException {
		SimpleDateFormat sdfrmt = new SimpleDateFormat("yyyy-MM-dd");
		sdfrmt.setLenient(false);
		try {
			Date javaDate = sdfrmt.parse(date);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String strDate = dateFormat.format(javaDate);
			setDate(strDate);
		} catch (ParseException e) {
			throw new DeliveryDocketException("Error: date is out of range");
		}

	}
}
