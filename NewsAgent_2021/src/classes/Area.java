package classes;

import exceptionHandler.AreaExceptionHandler;

public class Area {
	private int id_area;
	private int id_customer;
	private int id_driver;

	public Area() {

	}

	public Area(int id_customer1, int id_driver1) throws AreaExceptionHandler {
		try {
			validateId_customer(id_customer1);
			validateId_driver(id_driver1);
		} catch (AreaExceptionHandler e) {
			throw e;
		}
		this.id_customer = id_customer1;
		this.id_driver = id_driver1;
	}

	// ++++++++++++++++++++++++++++++++++++++
	public int getId_area() {
		return id_area;
	}

	public void setId_area(int id_area) {
		this.id_area = id_area;
	}

	public int getId_customer() {
		return id_customer;
	}

	public void setId_customer(int id_customer) {
		this.id_customer = id_customer;
	}

	public int getId_driver() {
		return id_driver;
	}

	public void setId_driver(int id_driver) {
		this.id_driver = id_driver;
	}

	// +++++++++++++++++++++++++++++ id_customer
	// nr of customers is unlimited so we catch an exception for negative numbers
	// and let the table open for any nr of customers

	public void validateId_customer(int id_customer1) throws AreaExceptionHandler {
		if (id_customer1 <= 0)
			throw new AreaExceptionHandler("Error: You cant enter a number below 1");
	}

	// +++++++++++++++++++++++++++ id_driver

	// we have 24 areas so we sign 1 driver for each area
	// and so we have 24 drivers (counting from 1 to 24)

	public void validateId_driver(int id_driver1) throws AreaExceptionHandler {
		if (id_driver1 <= 0)
			throw new AreaExceptionHandler("Error: You cant enter a number below 1");
		else if (id_driver1 > 24)
			throw new AreaExceptionHandler("Error: The number is out of range");
	}

	// public void validateArea( int id_customer1, int id_driver1) throws
	// AreaExceptionHandler {

	// if ( id_customer1 < 1 && id_driver1 > 24)
	// throw new AreaExceptionHandler("Error: The numbers are out of range");

	// ++++++++++++++++++++++++++++++++++++++++ id_area
	public void validateId_area(int id_area1) throws AreaExceptionHandler {
		if (id_area1 <= 0)
			throw new AreaExceptionHandler("Error: You cant enter a number below 1");
		else
			this.id_area = id_area1;
	}

}
