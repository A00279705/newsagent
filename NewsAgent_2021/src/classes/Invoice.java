package classes;
import exceptionHandler.InvoiceExceptionHandler;

public class Invoice {
	private int ID;

	public Invoice() {

	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public void validateId_invoice(int id_invoice) throws InvoiceExceptionHandler {
		if (id_invoice <= 0)
			throw new InvoiceExceptionHandler("Error: Invoice ID below range");
		else
			this.ID = id_invoice;
	}
}
