package classes;
import exceptionHandler.CustomerExceptionHandler;

public class Customer {
	private int id_cust;
	private String first_name;
	private String last_name;
	private String address;
	private String delivery_area;
	private String phoneNumber;

	public Customer(String custFirstName, String custLastName, String custAddress, String custDeliveryArea,
			String custPhone) {

		// id_cust = custID;
		first_name = custFirstName;
		last_name = custLastName;
		address = custAddress;
		delivery_area = custDeliveryArea;
		phoneNumber = custPhone;

	}

	public Customer() {

	}

	public void setId(int custId) {
		id_cust = custId;
	}

	public void setFirstName(String custFirstName) {
		first_name = custFirstName;
	}

	public void setLastName(String custLastName) {
		last_name = custLastName;
	}

	public void setStreet(String custAddress) {
		address = custAddress;
	}

	public void setPhone(String custPhone) {
		phoneNumber = custPhone;
	}

	public void setDeliveryArea(String custDeliveryArea) {
		delivery_area = custDeliveryArea;
	}

	public int getId() {
		return id_cust;
	}

	public  String getFirstName() {
		return first_name;
	}

	public String getLastName() {
		return last_name;
	}

	public String getAddress() {
		return address;
	}

	public String getDeliveryArea() {
		return delivery_area;
	}

	public String getPhone() {
		return phoneNumber;
	}

	public void validateFirstName(String custFirstName) throws CustomerExceptionHandler {

		if (custFirstName.isBlank() || custFirstName.isEmpty())
			throw new CustomerExceptionHandler("Customer First Name NOT specified");
		else if (custFirstName.length() < 2)
			throw new CustomerExceptionHandler("Customer First Name does not meet minimum length requirements");
		else if (custFirstName.length() > 30)
			throw new CustomerExceptionHandler("Customer First Name exceeds maximum length requirements");
		else
			this.first_name = custFirstName;
	}

	public void validateLastName(String custLastName) throws CustomerExceptionHandler {

		if (custLastName.isBlank() || custLastName.isEmpty())
			throw new CustomerExceptionHandler("Customer Last Name NOT specified");
		else if (custLastName.length() < 2)
			throw new CustomerExceptionHandler("Customer Last Name does not meet minimum length requirements");
		else if (custLastName.length() > 30)
			throw new CustomerExceptionHandler("Customer Last Name exceeds maximum length requirements");
		else
			this.last_name = custLastName;
	}

	public void validateAddress(String custAddress) throws CustomerExceptionHandler {

		if (custAddress.isBlank() || custAddress.isEmpty())
			throw new CustomerExceptionHandler("Address NOT specified");
		else if (custAddress.length() < 1)
			throw new CustomerExceptionHandler("Address does not meet minimum length requirements");
		else if (custAddress.length() > 30)
			throw new CustomerExceptionHandler("Address exceeds maximum length requirements");
		else
			this.address = custAddress;
	}

	public void validatePhone(String custPhone) throws CustomerExceptionHandler {

		if (custPhone.isBlank() || custPhone.isEmpty())
			throw new CustomerExceptionHandler("Phone number NOT specified");
		else if (custPhone.length() < 10)
			throw new CustomerExceptionHandler("Phone number does not meet minimum length requirements");
		else if (custPhone.length() > 15)
			throw new CustomerExceptionHandler("Phone number exceeds maximum length requirements");
		else
			this.phoneNumber = custPhone;
	}

	public void validateDeliveryArea(String custDeliveryArea) throws CustomerExceptionHandler {

		if (custDeliveryArea.isBlank() || custDeliveryArea.isEmpty())
			throw new CustomerExceptionHandler("Delivery Area NOT specified");
		else if (custDeliveryArea.length() < 1)
			throw new CustomerExceptionHandler("Delivery Area below range");
		else if (custDeliveryArea.length()> 24)
			throw new CustomerExceptionHandler("Delivery Area above range");

	}

	public void validateID_customer(int custId) throws CustomerExceptionHandler {

		if (custId < 0)
			throw new CustomerExceptionHandler("Customer ID below range");
		else
			this.id_cust = custId;
	}

	public void validateCustomer_UpdateFirstName(String Fname) throws CustomerExceptionHandler {

		if (Fname.isBlank() || Fname.isEmpty())
			throw new CustomerExceptionHandler("Customer First Name NOT specified");
		else if (Fname.length() < 2)
			throw new CustomerExceptionHandler("Customer First Name does not meet minimum length requirements");
		else if (Fname.length() > 30)
			throw new CustomerExceptionHandler("Customer First Name exceeds maximum length requirements");
		else
			this.first_name = Fname;
	}

	public void validateCustomer_UpdateLastName(String Lname) throws CustomerExceptionHandler {

		if (Lname.isBlank() || Lname.isEmpty())
			throw new CustomerExceptionHandler("Customer Last Name NOT specified");
		else if (Lname.length() < 2)
			throw new CustomerExceptionHandler("Customer Last Name does not meet minimum length requirements");
		else if (Lname.length() > 30)
			throw new CustomerExceptionHandler("Customer Last Name exceeds maximum length requirements");
		else
			this.last_name = Lname;
	}

	public void validateCustomer_UpdateAddress(String address) throws CustomerExceptionHandler {

		if (address.isBlank() || address.isEmpty())
			throw new CustomerExceptionHandler("Customer Address NOT specified");
		else if (address.length() < 2)
			throw new CustomerExceptionHandler("Customer Address does not meet minimum length requirements");
		else if (address.length() > 30)
			throw new CustomerExceptionHandler("Customer Address exceeds maximum length requirements");
		else
			this.address = address;
	}

	public void validateCustomer_UpdateDeliveryArea(String DA) throws CustomerExceptionHandler {

		if (DA.isBlank() || DA.isEmpty())
			throw new CustomerExceptionHandler("Customer Delivery Area NOT specified");
		else if ((DA.length()) < 1)
			throw new CustomerExceptionHandler("Delivery Area below range");
		else if ((DA.length()) > 24)
			throw new CustomerExceptionHandler("Delivery Area above range");
		else
			this.delivery_area = DA;
	}

	public void validateCustomer_UpdatePhone(String phone) throws CustomerExceptionHandler {

		if (phone.isBlank() || phone.isEmpty())
			throw new CustomerExceptionHandler("Customer Phone NOT specified");
		else if (phone.length() < 10)
			throw new CustomerExceptionHandler("Customer Phone does not meet minimum length requirements");
		else if (phone.length() > 15)
			throw new CustomerExceptionHandler("Customer Phone exceeds maximum length requirements");
		else
			this.phoneNumber = phone;
	}
}
