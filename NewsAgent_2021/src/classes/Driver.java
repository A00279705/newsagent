package classes;
import exceptionHandler.DriversExceptionHandler;

public class Driver {
	private int id_driver;
	private String name;
	private String password;

	public Driver() {

	}

	public Driver(String name, String password) throws DriversExceptionHandler {
		try {
			ValidateName_driver(name);
			ValidatePassword_driver(password);
		} catch (DriversExceptionHandler e) {
			throw new DriversExceptionHandler("Error : Fialed to set Driver");
		}
		this.name = name;
		this.password = password;
	}

	public int getId_driver() {
		return id_driver;
	}

	public void setId_driver(int id_driver) {
		this.id_driver = id_driver;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void ValidateName_driver(String name) throws DriversExceptionHandler {
		if (name.isEmpty() || name.isBlank())
			throw new DriversExceptionHandler("Error: You name must contain characters");
		else if (name.length() > 45)
			throw new DriversExceptionHandler("Error: driver name length not greater then 45");
	}

	public void ValidatePassword_driver(String password) throws DriversExceptionHandler {
		if (password.isEmpty() || password.isBlank())
			throw new DriversExceptionHandler("Error: You password must contain characters");
		else if (password.length() < 8)
			throw new DriversExceptionHandler("Error: Your password must contains at least 8 Characters");
		else if (password.length() > 45)
			throw new DriversExceptionHandler("Error: driver password length not greater then 45");

	}

	
	public void ValidateUpdateName_driver(String name) throws DriversExceptionHandler {
		if (name.isEmpty() || name.isBlank())
			throw new DriversExceptionHandler("Error: You name must contain characters");
		else if (name.length() > 45)
			throw new DriversExceptionHandler("Error: driver name length not greater then 45");
		this.name = name;
	}

	public void ValidateUpdatePassword_driver(String password) throws DriversExceptionHandler {
		if (password.isEmpty() || password.isBlank())
			throw new DriversExceptionHandler("Error: You password must contain characters");
		else if (password.length() < 8)
			throw new DriversExceptionHandler("Error: Your password must contains at least 8 Characters");
		else if (password.length() > 45)
			throw new DriversExceptionHandler("Error: driver password length not greater then 45");
		this.password = password;

	}
	
	
	// validate input driver ID
	public void validateID_driver(int id_driver) throws DriversExceptionHandler {

		if (id_driver < 1 || id_driver > 24)
			throw new DriversExceptionHandler("Error: You cant enter a number below 1 or greater than 24");
		this.id_driver = id_driver;
	}

}
