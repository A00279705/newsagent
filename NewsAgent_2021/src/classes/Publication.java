package classes;
import exceptionHandler.PublicationExceptionHandler;

public class Publication {
	private int id_publication;
	private String book_name;
	private int book_price;
	private int book_amount;

	public Publication(String book_name, int book_price, int book_amount) throws PublicationExceptionHandler {

		// if book_name, book_price and book_amount is successful then those values will
		// be set
		try {

			validateBook_name(book_name);
			validateBook_price(book_price);
			validateBook_amount(book_amount);
		} catch (PublicationExceptionHandler e) {
			throw new PublicationExceptionHandler("Error - Could not set publication");
		}

		this.book_price = book_price;
		this.book_amount = book_amount;
	}

	public Publication() {
		// TODO Auto-generated constructor stub
	}

	public int getId_publication() {
		return id_publication;
	}

	public void setId_publication(int id_publication) {
		this.id_publication = id_publication;
	}

	public String getBook_name() {
		return book_name;
	}

	public void setBook_name(String book_name) {
		this.book_name = book_name;
	}

	public int getBook_price() {
		return book_price;
	}

	public void setBook_price(int book_price) {
		this.book_price = book_price;
	}

	public int getBook_amount() {
		return book_amount;
	}

	public void setBook_amount(int book_amount) {
		this.book_amount = book_amount;
	}

	// validate input for id_publication
	public void validateId_publication(int id_publication) throws PublicationExceptionHandler {

		if (id_publication <= 0)
			throw new PublicationExceptionHandler("Error - id cannot be below 1");
		else
			this.id_publication = id_publication;
	}

	// validate input for book_name
	public void validateBook_name(String book_name) throws PublicationExceptionHandler {

		if (book_name.isBlank() || book_name.isEmpty())
			throw new PublicationExceptionHandler("Error - name cannot be empty");
		else if (book_name.length() < 1)
			throw new PublicationExceptionHandler("Error - Book name should be at least 1 character");
		else if (book_name.length() > 50)
			throw new PublicationExceptionHandler("Error - Book length cannot be more than 50 characters");
		else
			this.book_name = book_name;
	}

	// validate input for book_price
	public void validateBook_price(int book_price) throws PublicationExceptionHandler {

		if (book_price < 0)
			throw new PublicationExceptionHandler("Error - book price cannot be below 0");
		else
			this.book_price = book_price;
	}

	// validate input for book_amount
	public void validateBook_amount(int book_amount) throws PublicationExceptionHandler {

		if (book_amount < 0)
			throw new PublicationExceptionHandler("Error - book amount cannot be below 0");
		else
			this.book_amount = book_amount;
	}

}
