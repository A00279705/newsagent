package classes;
import exceptionHandler.OrderExceptionHandler;

public class OrdersToPublication {
	private int id_ordprod;
	private int id_order;
	private int id_publication;

	public OrdersToPublication() {

	}

	public OrdersToPublication(int id_order, int id_publication) throws OrderExceptionHandler {
		// if id_order and id_publication is successful then set those values.
		try {
			validateOrdProdID_order(id_order);
			validateOrdProdID_publication(id_publication);
		} catch (OrderExceptionHandler e) {
			throw new OrderExceptionHandler("Error: Failed to set Orders To Publication");
		}
		setId_ordprod(0);
		setId_order(id_order);
		setId_publication(id_publication);
	}

	public int getId_ordprod() {
		return id_ordprod;
	}

	public void setId_ordprod(int id_ordprod) {
		this.id_ordprod = id_ordprod;
	}

	public int getId_order() {
		return id_order;
	}

	public void setId_order(int id_order) {
		this.id_order = id_order;
	}

	public int getId_publication() {
		return id_publication;
	}

	public void setId_publication(int id_publication) {
		this.id_publication = id_publication;
	}

	// validate input id_order
	public void validateOrdProdID_order(int id_order) throws OrderExceptionHandler {
		// Agree Formating Rules on "Order To Publication id_order"
		// int id_order must be 1 to Integer.MAX_VALUE
		if (id_order <= 0)
			throw new OrderExceptionHandler("Error: You cant enter a number bellow 1");
	}

	// validate input id_publication
	public void validateOrdProdID_publication(int id_publication) throws OrderExceptionHandler {
		// Agree Formating Rules on "Order To Publication id_publication"
		// int id_publication must be 1 to Integer.MAX_VALUE
		if (id_publication <= 0)
			throw new OrderExceptionHandler("Error: You cant enter a number bellow 1");
	}

	// validate input set id_ordprod
	public void validateOrdProdID_ordprod(int id_ordprod) throws OrderExceptionHandler {
		// Agree Formating Rules on "Order To Publication id_ordprod"
		// int id_ordprod must be 1 to Integer.MAX_VALUE
		if (id_ordprod <= 0)
			throw new OrderExceptionHandler("Error: You cant enter a number bellow 1");
		else
			setId_ordprod(id_ordprod);
	}
}
