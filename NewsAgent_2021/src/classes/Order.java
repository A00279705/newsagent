package classes;
import exceptionHandler.OrderExceptionHandler;

public class Order {
	private int id_order;
	private String order_type;
	private int id_customer;

	public Order() {

	}

	public Order(String order_type, int id_customer) throws OrderExceptionHandler {
		// if order_type and id_customer is successful then set those values.
		try {
			validateOrder_type(order_type);
			validateID_customer(id_customer);
		} catch (OrderExceptionHandler e) {
			throw new OrderExceptionHandler("Error: Failed to set Order");
		}
		setId_order(0);
		setOrder_type(order_type);
		setId_customer(id_customer);
	}

	public int getId_order() {
		return id_order;
	}

	public void setId_order(int id_order) {
		this.id_order = id_order;
	}

	public String getOrder_type() {
		return order_type;
	}

	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}

	public int getId_customer() {
		return id_customer;
	}

	public void setId_customer(int id_customer) {
		this.id_customer = id_customer;
	}

	// validate input TYPE for CREATE ORDER
	public void validateOrder_type(String order_type) throws OrderExceptionHandler {
		// Agree Formating Rules on "Order order_type"
		// String order_type must be "daily", "weekly", "monthly"
		if (order_type.isEmpty() || order_type.isBlank())
			throw new OrderExceptionHandler("Error: You can only Enter daily, weekly, monthly");
		else if (!order_type.equals("daily") && !order_type.equals("weekly") && !order_type.equals("monthly"))
			throw new OrderExceptionHandler("Error: You can only Enter daily, weekly, monthly");
	}

	// validate input CUSTOMER ID for CREATE ORDER
	public void validateID_customer(int id_customer) throws OrderExceptionHandler {
		// Agree Formating Rules on "Order id_customer"
		// int id_customer must be 1 to Integer.MAX_VALUE
		if (id_customer <= 0)
			throw new OrderExceptionHandler("Error: You cant enter a number bellow 1");
	}

	// validate input ORDER ID
	public void validateID_order(int id_order) throws OrderExceptionHandler {
		// Agree Formating Rules on "Order id_order"
		// int id_order must be 1 to Integer.MAX_VALUE then it can set id_order=id_order
		if (id_order <= 0)
			throw new OrderExceptionHandler("Error: You cant enter a number bellow 1");
		else
			this.id_order = id_order;
	}

	// validate ORDER input ORDER ID for UPDATE ORDER
	public void validateOrder_UpdateType(String order_type) throws OrderExceptionHandler {
		// Agree Formating Rules on "input Order order_type"
		// int id_order must be 1 to Integer.MAX_VALUE then it can set id_order=id_order
		if (order_type.isEmpty() || order_type.isBlank())
			throw new OrderExceptionHandler("Error: You can only Enter daily, weekly, monthly");
		else if (!order_type.equals("daily") && !order_type.equals("weekly") && !order_type.equals("monthly"))
			throw new OrderExceptionHandler("Error: You can only Enter daily, weekly, monthly");
		else
			this.order_type = order_type;
	}

}
